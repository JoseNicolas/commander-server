/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commander.server;

/**
 *
 * @author josenicolas
 */
public class ResponseMessage {
    // ========================================================================
    //                              SUCCESS MESSAGES
    // ========================================================================
    public static final String SUCCESS_OPERATION = "La operación se realizó correctamente";
    // ========================================================================
    //                              ERROR MESSAGES
    // ========================================================================
    public static final String NO_ACTION_FOUND = "No se pudo encontrar la acción a realizar";
    public static final String INTERNAL_SERVER_ERROR = "Se produjo un error interno de servidor";
    public static final String ERROR_UPLOADING_FILE = "Se produjo un error realizando la carga del fichero al servidor";
}
