package com.commander.server;

import com.commander.server.controller.LoginFormController;
import com.commander.server.utils.Config;
import com.commander.server.controller.MainFormController;
import com.commander.server.controller.SplashFormController;
import com.commander.server.controller.SplashPreloader;
import com.commander.server.dao.DaoFactory;
import com.commander.server.dao.UsuarioDao;
import com.commander.server.model.*;
import com.sun.javafx.application.LauncherImpl;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.application.Preloader;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.commons.lang3.exception.ExceptionUtils;


public class Main extends Application implements EventHandler {

    public static final String TITLE = "Commander";
    public static final String VERSION = "BETA";
    private Stage primary_stage;
    private static final int COUNT_LIMIT = 10;
    
    @Override
    public void start(Stage stage) throws Exception {
        try{
//            initMainForm(stage);
            this.primary_stage = stage;
            initLoginForm();

        }catch(Exception ex){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(ex), Level.SEVERE);
        }
    }

    @Override
    public void init() throws Exception {
        // Perform some heavy lifting (i.e. database start, check for application updates, etc. )
        for (int i = 1; i <= COUNT_LIMIT; i++) {
            double progress =(double) i/10;
//            System.out.println("progress: " +  progress);            
            LauncherImpl.notifyPreloader(this, new Preloader.ProgressNotification(progress));
            Thread.sleep(400);
        }
    }

    public static void main(String[] args) {
//        launch(args);
        LauncherImpl.launchApplication(Main.class, SplashPreloader.class, args);
    }

    public void initSplash(){
        try{
            // Layout
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLSplashForm.fxml"));
            SplashFormController controller = new SplashFormController();
            loader.setController(controller);

            // Show scene
            Scene scene = new Scene((Parent) loader.load());
            scene.getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");
            this.primary_stage.setScene(scene);
            this.primary_stage.getIcons().add(new Image(Config.getProperty("icon")));
            this.primary_stage.sizeToScene();
            this.primary_stage.setOnCloseRequest(this);
            this.primary_stage.show();
        }catch (Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    public void initLoginForm(){
        try{
            // Layout
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLLoginForm.fxml"));
            LoginFormController controller = new LoginFormController(primary_stage);
            loader.setController(controller);
            // Show scene
            Scene scene = new Scene((Parent) loader.load());
            scene.getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");
            this.primary_stage.setScene(scene);
            this.primary_stage.setTitle("Autentificación");
            this.primary_stage.getIcons().add(new Image(Config.getProperty("icon")));
            this.primary_stage.sizeToScene();
            this.primary_stage.setResizable(false);
            this.primary_stage.setOnCloseRequest(this);
            this.primary_stage.show();
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }

    @Override
    public void handle(Event event) {
        Object target = event.getTarget();
        
        // Evento cierre de ventana principal de la aplicacion
        if (target instanceof Stage){
            Platform.exit();
            System.exit(0);    
        }
    }
}
