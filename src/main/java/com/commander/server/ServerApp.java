package com.commander.server;

import com.commander.server.controller.MainFormController;
import com.commander.server.dao.DaoFactory;
import com.commander.server.model.*;
import com.commander.server.utils.Config;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.kryonet.util.InputStreamSender;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.exception.ExceptionUtils;

/*
 * 2019/05/11
 * @author JosÃ© Francisco NicolÃ¡s Bautista
 * @version 1.0
 */
public class ServerApp extends Listener{
    
     // PROPIEDADES
    public Server server;
       
    // CONSTRUCTORES
    public ServerApp() throws Exception {
        
        int tcp_port = Integer.parseInt(Config.getProperty("server_tcp_port"));
        int udp_port = Integer.parseInt(Config.getProperty("server_udp_port"));

        this.server = new Server((3 * 1024), (3 * 1024));
        
        this.server.start();
        this.server.bind(tcp_port);
        this.server.addListener(this);
        this.register(this.server);
        
        initServerOutputFiles();

        MainFormController.Logger(this.getClass(), "[SERVER] Escuchando TCP en puerto:" + tcp_port, Level.INFO);
    }
    
    // METODOS
    /**
     * Registramos las clases a utilizar para su serializaciÃ³n 
     * @param server 
     */
    private void register(Server server){
        
        server.getKryo().register(Date.class);
        server.getKryo().register(Request.class);
        server.getKryo().register(Response.class);
        server.getKryo().register(ArrayList.class);
        server.getKryo().register(Usuario.class);
        server.getKryo().register(Mesa.class);
        server.getKryo().register(Rol.class);
        server.getKryo().register(ProductoTipo.class);
        server.getKryo().register(Producto.class);
        server.getKryo().register(Menu.class);
        server.getKryo().register(Factura.class);
        server.getKryo().register(DetalleMenu.class);
        server.getKryo().register(DetalleComanda.class);
        server.getKryo().register(Comanda.class);
        server.getKryo().register(byte[].class);
    }
    
    /**
     * Manejador de peticiones TCP del servidor
     * @param Request request 
     */
    private Response handleServerRequest(Request request) {
        Response response = new Response();
        String[] action;
        
        try{
            action = request.getSplitedAction();
            
            switch (action [1]){
                case "base_data":
                    switch (action [0]){
                        case "client":
                            response = ServerAPI.getClientAppBaseData(request);
                            break;
                    }
                    break;
                case "login":
                    switch (action [0]){
                        case "validar":
                            response = ServerAPI.validarLogin(request);
                            break;
                    }
                    break;
                case "usuario":
                    switch (action [0]){
                        case "create":
                            response = ServerAPI.createUsuario(request);
                            break;
                        case "read":
                            response = ServerAPI.readUsuario(request);
                            break;
                        case "readAll":
                            response = ServerAPI.readUsuarios(request);
                            break;
                        case "update":
                            response = ServerAPI.updateUsuario(request);
                            break;
                        case "delete":
                            response = ServerAPI.deleteUsuario(request);
                            break;
                        default:
                            response.setStatus(true);
                            response.setError(true);
                            response.setMessage(ResponseMessage.NO_ACTION_FOUND);
                            break;
                    }
                    break;
                case "comanda":
                    switch (action [0]){
                        case "create":
                            response = ServerAPI.createComanda(request);
                            break;
                        case "create_detalle":
                            response = ServerAPI.createComandaDetalleComanda(request);
                            break;
                        case "read":
                            response = ServerAPI.readComanda(request);
                            break;
                        case "readAll":
                            response = ServerAPI.readComandas(request);
                            break;
                        case "update":
                            response = ServerAPI.updateComanda(request);
                            break;
                        case "delete":
                            response = ServerAPI.deleteComanda(request);
                            break;
                        default:
                            response.setStatus(true);
                            response.setError(true);
                            response.setMessage(ResponseMessage.NO_ACTION_FOUND);
                            break;
                    }
                    break;
                case "factura":
                    switch (action [0]){
                        case "create":
                            response = ServerAPI.createFactura(request);
                            break;
                        case "read":
                            response = ServerAPI.readFactura(request);
                            break;
                        case "update":
                            response = ServerAPI.updateFactura(request);
                            break;
                        case "delete":
                            response = ServerAPI.deleteFactura(request);
                            break;
                        default:
                            response.setStatus(true);
                            response.setError(true);
                            response.setMessage(ResponseMessage.NO_ACTION_FOUND);
                            break;
                    }
                    break;
                case "detalle_comanda":
                    switch (action [0]){
                        case "create":
                            response = ServerAPI.createDetalleComandas(request);
                            break;
                        case "createWithComanda":
                            response = ServerAPI.createDetalleComandasWithComanda(request);
                            break;
                        case "read":
                            response = ServerAPI.readDetalleComanda(request);
                            break;
                        case "readAll":
                            response = ServerAPI.readDetalleComandas(request);
                            break;
                        case "update":
                            response = ServerAPI.updateDetalleComandas(request);
                            break;
                        case "delete":
                            response = ServerAPI.deleteDetalleComanda(request);
                            break;
                        default:
                            response.setStatus(true);
                            response.setError(true);
                            response.setMessage(ResponseMessage.NO_ACTION_FOUND);
                            break;
                    }
                    break;
                case "producto":
                    switch (action [0]){
                        case "create":
                            response = ServerAPI.createProducto(request);
                            break;
                        case "read":
                            response = ServerAPI.readProducto(request);
                            break;
                        case "readAll":
                            response = ServerAPI.readProductos(request);
                            break;
                        case "readAllByTipo":
                            response = ServerAPI.readProductosByTipo(request);
                            break;
                        case "update":
                            response = ServerAPI.updateProducto(request);
                            break;
                        case "delete":
                            response = ServerAPI.deleteProducto(request);
                            break;
                        default:
                            response.setStatus(true);
                            response.setError(true);
                            response.setMessage(ResponseMessage.NO_ACTION_FOUND);
                            break;
                    }
                    break;
                case "mesa":
                    switch (action [0]){
                        case "create":
                            response = ServerAPI.createMesa(request);
                            break;
                        case "read":
                            response = ServerAPI.readMesa(request);
                            break;
                        case "readAll":
                            response = ServerAPI.readMesas(request);
                            break;
                        case "update":
                            response = ServerAPI.updateMesa(request);
                            break;
                        case "delete":
                            response = ServerAPI.deleteMesa(request);
                            break;
                        default:
                            response.setStatus(true);
                            response.setError(true);
                            response.setMessage(ResponseMessage.NO_ACTION_FOUND);
                            break;
                    }
                    break;
                case "productoTipo":
                    switch (action [0]){
                        case "create":
                            response = ServerAPI.createProductoTipo(request);
                            break;
                        case "read":
                            response = ServerAPI.readProductoTipo(request);
                            break;
                        case "readAll":
                            response = ServerAPI.readProductoTipos(request);
                            break;
                        case "update":
                            response = ServerAPI.updateProductoTipo(request);
                            break;
                        case "delete":
                            response = ServerAPI.deleteProductoTipo(request);
                            break;
                        default:
                            response.setStatus(true);
                            response.setError(true);
                            response.setMessage(ResponseMessage.NO_ACTION_FOUND);
                            break;
                    }
                    break;
                case "detalle_menu":
                    switch (action [0]){
                        case "create":
                            response = ServerAPI.createDetalleMenus(request);
                            break;
                        case "read":
                            response = ServerAPI.readDetalleMenu(request);
                            break;
                        case "readAll":
                            response = ServerAPI.readDetalleMenus(request);
                            break;
                        case "update":
                            response = ServerAPI.updateDetalleMenus(request);
                            break;
                        case "delete":
                            response = ServerAPI.deleteDetalleMenu(request);
                            break;
                        default:
                            response.setStatus(true);
                            response.setError(true);
                            response.setMessage(ResponseMessage.NO_ACTION_FOUND);
                            break;
                    }
                    break;
                case "menu":
                    switch (action [0]){
                        case "create":
                            response = ServerAPI.createMenu(request);
                            break;
                        case "read":
                            response = ServerAPI.readMenu(request);
                            break;
                        case "readAll":
                            response = ServerAPI.readMenus(request);
                            break;
                        case "update":
                            response = ServerAPI.updateMenu(request);
                            break;
                        case "delete":
                            response = ServerAPI.deleteComanda(request);
                            break;
                        default:
                            response.setStatus(true);
                            response.setError(true);
                            response.setMessage(ResponseMessage.NO_ACTION_FOUND);
                            break;
                    }
                    break;
                default:
                    response.setStatus(true);
                    response.setError(true);
                    response.setMessage(ResponseMessage.NO_ACTION_FOUND);
                    break;
            }
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
            response.setStatus(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Inicializa un nuevo socket servidor a la espera de recibir un fichero.
     * Una vez recibido el fichero el socket es cerrado.
     * @return 
     */
    public static int initServerInputFiles(final Object obj) throws Exception{
        
        int server_port = 0;
        
        try{
            
            final ServerSocket serverSocket = new ServerSocket(0);
            final ExecutorService clientProcessingPool = Executors.newFixedThreadPool(10);
            server_port = serverSocket.getLocalPort();
            
            Runnable serverTask = new Runnable() {
                @Override
                public void run() {
                    try {
                        int server_port = Integer.parseInt(Config.getProperty("server_files_tcp_port"));

//                        ServerSocket serverSocket = new ServerSocket(server_port);
                        MainFormController.Logger(this.getClass(), "[SERVER INPUT FILES] Escuchando en TCP en puerto " + server_port, Level.INFO);
                        while (true) {
                            Socket clientSocket = serverSocket.accept();
                            if (obj instanceof Producto){
                                Producto producto = (Producto) obj;
                                clientProcessingPool.submit(taskSetProductoImage(clientSocket, producto));    
                            }
                            
                        }
                    } catch (Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            };
            
            new Thread(serverTask).start();
            
        }catch(Exception e){
            MainFormController.Logger(ServerApp.class, ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }

        return server_port;
    }
    
    /**
     * Inicializa y pone a la escucha un socket TCP para servir fichero de imagen de producto.
     * @param id_producto 
     */
    public void initServerOutputFiles(){
        final ExecutorService clientProcessingPool = Executors.newFixedThreadPool(10);
        
        Runnable serverTask = new Runnable() {
            @Override
            public void run() {
                boolean is_send = false;
                ServerSocket server_socket;
                Socket client_socket;
                InputStream is;
                InputStreamReader isr;
                BufferedReader br;
                FileInputStream fis;
                BufferedInputStream bis;
                String id_producto;
                int id_producto_parsed;
                OutputStream os;
                int port = 13085;
                byte[] mybytearray;
                Producto producto;
                    
                try {

                    server_socket = new ServerSocket(port);
                    MainFormController.Logger(this.getClass(), "[SERVER OUTPUT FILE] Escuchando TCP en puerto:" + port, Level.INFO);

                    //Server is running always. This is done using this while(true) loop
                    while(true)
                    {
                        //Reading the message from the client
                        client_socket = server_socket.accept();
                        is = client_socket.getInputStream();
                        isr = new InputStreamReader(is);
                        br = new BufferedReader(isr);
                        id_producto = br.readLine();

                        try{
                            id_producto_parsed = Integer.parseInt(id_producto); 
                        }catch (NumberFormatException ex){
                            id_producto_parsed = 0;
                        }

                        producto = DaoFactory.getProductoDao().load(id_producto_parsed);

                        File image;
                        if (producto != null){
                            image = new File(Config.getProperty("directory_images") + "/" + producto.getImagen());
                            if (image != null){
                                mybytearray = new byte[(int) image.length()];
                            }else{
                                mybytearray = new byte[(int) 0];
                            }
                        }else{
                            image = null;
                            mybytearray = new byte[(int) 0];
                        }

                        fis = new FileInputStream(image);
                        bis = new BufferedInputStream(fis);

                        bis.read(mybytearray, 0, mybytearray.length);
                        os = client_socket.getOutputStream();
                        os.write(mybytearray, 0, mybytearray.length);

                        os.flush();
                        os.close();
                        client_socket.close(); 

                    }
                }catch(Exception e){
                    e.printStackTrace();
                    MainFormController.Logger(this.getClass(), "[SERVER OUTPUT FILE] Se produjo un error!", Level.SEVERE);
                    MainFormController.Logger(this.getClass(), ExceptionUtils.getMessage(e) + port, Level.SEVERE);
                }
            }
        };
        Thread serverThread = new Thread(serverTask);
        serverThread.start();
    }
    
    /**
     * Tarea a realizar por el servidor de ficheros.
     * @return 
     */
    private static Runnable taskSetProductoImage(final Socket socket, final Producto producto){
       
        return new Runnable(){
            @Override
                public void run() {                    
                    try{
                        int bytesRead;
                        String file_full_path = Config.getProperty("directory_images") + "/" + producto.getImagen();
                        InputStream is = socket.getInputStream();    
                        OutputStream os = new FileOutputStream(file_full_path);
                        byte[] buffer = new byte[1024];
                        
                        // Escribimos la imagen en disco
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        
                        // Closing the FileOutputStream handle and socket
                        os.flush();
                        os.close();
                        socket.close();
                    }catch(Exception e){
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                    
                }
        };
    }
    
    // OVERRIDE METODOS
    @Override
    public void idle(Connection cnctn) {
        super.idle(cnctn); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void received(Connection cnctn, Object o) {
        super.received(cnctn, o); //To change body of generated methods, choose Tools | Templates.
        
        Request request;
        Response response;
        
        // Tomamos unicamente las peticiones de instancias de Request
        if (o instanceof Request){
            request = (Request) o;

            // Procesamos la peticion y obtenemos una respuesta
            response = handleServerRequest(request);
            // Devolvemos la respuesta al cliente
            cnctn.sendTCP(response);
            
            MainFormController.Logger(this.getClass(), "[SERVER] Recibida peticion desde " + cnctn.getRemoteAddressTCP().getAddress().getHostAddress() + ":" + cnctn.getRemoteAddressTCP().getPort(), Level.INFO);

        }
    }

    @Override
    public void disconnected(Connection cnctn) {
        super.disconnected(cnctn); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void connected(Connection cnctn) {
        super.connected(cnctn); //To change body of generated methods, choose Tools | Templates.
    }
}