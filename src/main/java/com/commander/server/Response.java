package com.commander.server;

/*
 * 2019/04/29
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class Response {
    private boolean status;// Emula el codigo HTTP
    private boolean error;
    private String message;
    private Object data;

    public Response() {
    }

    public Response(boolean status, boolean error, String message) {
        this.status = status;
        this.error = error;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public Object getData(){
        return this.data;
    }
   
    public void setData(Object data){
        this.data = data;
    }
}
