package com.commander.server.dao;

import com.commander.server.model.Database;
import com.commander.server.model.Usuario;
import com.commander.server.model.interfaces.IUsuarioDao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class UsuarioDao extends Usuario implements IUsuarioDao{

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public Usuario load(int id) throws Exception{
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Usuario response = null;
        Database db=null;

        String sql="SELECT * FROM usuarios WHERE id = " + id;
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            
            if (rs.first()){
                response = new Usuario();
                
                response.setId( rs.getInt(1) );
                response.setUsuario(rs.getString(2) );
                response.setPassword(rs.getString(3) );
                response.setNombre( rs.getString(4) );
                response.setApellido1(rs.getString(5) );
                response.setApellido2( rs.getString(6) );
                response.setSexo( rs.getString(7) );
                response.setCreated( sdf.parse(rs.getString(8)) );
                response.setCreator(rs.getInt(9) );
                response.setUpdated( sdf.parse(rs.getString(10)) );
                response.setUpdater( rs.getInt(11) );
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;       
    }

    @Override
    public Usuario insert(Usuario usuario) throws Exception{
        Usuario response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        ResultSet rs=null;
        
        try{
            String sql="INSERT INTO usuarios "
                + "(`usuario`, `password`, `nombre`, `apellido1`, `apellido2`, "
                + "`sexo`, `created`, `creator`, `updated`, `updater`) "
                + "VALUES ('"+usuario.getUsuario()+"','"
                + usuario.getPassword() + "','"
                + usuario.getNombre()+ "','"
                + usuario.getApellido1()+ "','"
                + usuario.getApellido2()+ "','"
                + usuario.getSexo()+ "','"
                + sdf.format(usuario.getCreated())+ "',"
                + usuario.getCreator()+ ",'"
                + sdf.format(usuario.getUpdated())+ "',"
                + usuario.getUpdater()+ ")";
        
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);
            rs=stm.getGeneratedKeys();
            if (rs.first()){
                usuario.setId(rs.getInt(1));
                response=usuario;
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;     
    }

    @Override
    public Usuario update(int id, Usuario usuario) throws Exception {
        Usuario response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        
        try{
            String sql="UPDATE usuarios SET "
                + "`usuario`='"+usuario.getUsuario()+"', "
                + "`password`='"+usuario.getPassword()+"', "
                + "`nombre`='"+usuario.getNombre()+"', "
                + "`apellido1`='"+usuario.getApellido1()+"', "
                + "`apellido2`='"+usuario.getApellido2()+"', "
                + "`sexo`='"+usuario.getSexo()+"', "
//                + "`created`='"+sdf.format(usuario.getCreated())+"', "
//                + "`creator`="+usuario.getCreator()+", "
                + "`updated`='"+sdf.format(usuario.getUpdated())+"', "
                + "`updater`="+usuario.getUpdater()+" WHERE id="+id;

            response = this.load(id);
            
            if (response!=null){
                db=new Database();
                conn = db.open();
                stm = conn.createStatement();
                stm.execute(sql);
                
                stm.close();
                conn.close();
                db.close();
                
                response = usuario;
            }
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;   
    }

    @Override
    public boolean delete(int id) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        		
        String sql="DELETE FROM usuarios WHERE id="+id;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response; 
    }

    @Override
    public List<Usuario> queryAll() throws Exception {
        List<Usuario> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Usuario usuario = null;
        Database db=null;

        String sql="SELECT * FROM usuarios";
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            
            while (rs.next()) {
                usuario = new Usuario();
                
                usuario.setId( rs.getInt(1) );
                usuario.setUsuario(rs.getString(2) );
                usuario.setPassword(rs.getString(3) );
                usuario.setNombre( rs.getString(4) );
                usuario.setApellido1(rs.getString(5) );
                usuario.setApellido2( rs.getString(6) );
                usuario.setSexo( rs.getString(7) );
                usuario.setCreated( sdf.parse(rs.getString(8)) );
                usuario.setCreator(rs.getInt(9) );
                usuario.setUpdated( sdf.parse(rs.getString(10)) );
                usuario.setUpdater( rs.getInt(11) );
                
                response.add(usuario);
            }
            
            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
    public Usuario queryByUsuario(String usuario) throws Exception{
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Usuario response = null;
        Database db=null;

        String sql="SELECT * FROM usuarios WHERE usuario = '" + usuario + "'";
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            
            if (rs.first()){
                response = new Usuario();
                
                response.setId( rs.getInt(1) );
                response.setUsuario(rs.getString(2) );
                response.setPassword(rs.getString(3) );
                response.setNombre( rs.getString(4) );
                response.setApellido1(rs.getString(5) );
                response.setApellido2( rs.getString(6) );
                response.setSexo( rs.getString(7) );
                response.setCreated( sdf.parse(rs.getString(8)) );
                response.setCreator(rs.getInt(9) );
                response.setUpdated( sdf.parse(rs.getString(10)) );
                response.setUpdater( rs.getInt(11) );
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;       
    }
}