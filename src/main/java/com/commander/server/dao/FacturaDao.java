package com.commander.server.dao;

import com.commander.server.model.Database;
import com.commander.server.model.Factura;
import com.commander.server.model.interfaces.IFacturaDao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class FacturaDao extends Factura implements IFacturaDao{
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public Factura load(int id) throws Exception{
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Factura response = null;
        Database db=null;

        String sql="SELECT * FROM facturas WHERE id = " + id;
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            if (rs.first()){
                response = new Factura();
                
                response.setId( rs.getInt(1) );
                response.setCodigo(rs.getString(2) );
                response.setFecha( sdf.parse(rs.getString(3)) );
                response.setComanda( rs.getInt(4) );
                response.setImporte(rs.getFloat(5) );
                response.setPagada( rs.getBoolean(6) );
                response.setObservaciones(rs.getString(7) );
                response.setCreated( sdf.parse(rs.getString(8)) );
                response.setCreator(rs.getInt(9) );
                response.setUpdated( sdf.parse(rs.getString(10)) );
                response.setUpdater( rs.getInt(11) );
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;       
    }

    @Override
    public Factura insert(Factura factura) throws Exception{
        Factura response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        ResultSet rs=null;
        int pagada = factura.isPagada() ? 1 : 0;
        	
        try{
            String sql="INSERT INTO facturas "
                + "(`codigo`, `fecha`, `comanda`, `importe`, `pagada`, "
                + "`observaciones`, `created`, `creator`, `updated`, `updater`) "
                + "VALUES ('"+factura.getCodigo()+"','"
                + sdf.format(factura.getFecha()) + "',"
                + factura.getComanda()+ ","
                + factura.getImporte()+ ","
                + pagada+ ",'"
                + factura.getObservaciones()+ "','"
                + sdf.format(factura.getCreated())+ "',"
                + factura.getCreator()+ ",'"
                + sdf.format(factura.getUpdated())+ "',"
                + factura.getUpdater()+ ")";
       		
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);
            rs=stm.getGeneratedKeys();
            if (rs.first()){
                factura.setId(rs.getInt(1));
                response=factura;
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;     
    }

    @Override
    public Factura update(int id, Factura factura) throws Exception {
        Factura response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        
        try{
            int pagada = factura.isPagada() ? 1 : 0;
            
            String sql="UPDATE facturas SET "
                + "`codigo`='"+factura.getCodigo()+"', "
                + "`fecha`='"+sdf.format(factura.getFecha())+"', "
                + "`comanda`="+factura.getComanda()+", "
                + "`importe`="+factura.getImporte()+", "
                + "`pagada`="+pagada+", "
                + "`observaciones`='"+factura.getObservaciones()+"', "
//                + "`created`='"+sdf.format(factura.getCreated())+"', "
//                + "`creator`="+factura.getCreator()+", "
                + "`updated`='"+sdf.format(factura.getUpdated())+"', "
                + "`updater`="+factura.getUpdater()+" WHERE id="+id;

            response = this.load(id);
            
            if (response!=null){
                db=new Database();
                conn = db.open();
                stm = conn.createStatement();
                stm.execute(sql);
                
                stm.close();
                conn.close();
                db.close();
                
                response = factura;
            }
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;   
    }

    @Override
    public boolean delete(int id) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        		
        String sql="DELETE FROM facturas WHERE id="+id;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response; 
    }

    @Override
    public List<Factura> queryAll() throws Exception {
        List<Factura> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Factura factura = null;
        Database db=null;

        String sql="SELECT * FROM facturas";
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            
            while (rs.next()) {
                factura = new Factura();
                
                factura.setId( rs.getInt(1) );
                factura.setCodigo(rs.getString(2) );
                factura.setFecha(sdf.parse(rs.getString(3)));
                factura.setComanda(rs.getInt(4) );
                factura.setImporte(rs.getFloat(5) );
                factura.setPagada( rs.getBoolean(6) );
                factura.setObservaciones( rs.getString(7) );
                factura.setCreated( sdf.parse(rs.getString(8)) );
                factura.setCreator(rs.getInt(9) );
                factura.setUpdated( sdf.parse(rs.getString(10)) );
                factura.setUpdater( rs.getInt(11) );
                
                response.add(factura);
            }
            
            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
}