package com.commander.server.dao;

import com.commander.server.model.Database;
import com.commander.server.model.Comanda;
import com.commander.server.model.Usuario;
import com.commander.server.model.interfaces.IComandaDao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class ComandaDao extends Comanda implements IComandaDao{
    
    @Override
    public Comanda load(int id) throws Exception{
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Comanda response = null;
        Database db=null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sql="SELECT * FROM comandas WHERE id = " + id;
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            
            if (rs.first()){
                response = new Comanda();
                
                response.setId( rs.getInt(1) );
                response.setMesa( rs.getInt(2) );
                response.setCamarero( rs.getInt(3) );
                response.setEstado( rs.getString(4) );
                response.setFactura( rs.getInt(5) );
                response.setCreated( sdf.parse( rs.getString(6)) );
                response.setCreator( rs.getInt(7) );
                response.setUpdated( sdf.parse( rs.getString(8)) );
                response.setUpdater( rs.getInt(9) );
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;       
    }

    @Override
    public Comanda insert(Comanda comanda) throws Exception{
        Comanda response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        ResultSet rs=null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        try{
            String sql="INSERT INTO comandas "
                + "(`mesa`, `camarero`, `estado`, `factura`, "
                + "`created`, `creator`, `updated`, `updater`) "
                + "VALUES ("
                + comanda.getMesa()+","
                + comanda.getCamarero() + ",'"
                + comanda.getEstado()+ "',"
                + comanda.getFactura()+ ",'"
                + sdf.format(comanda.getCreated())+ "',"
                + comanda.getCreator()+ ",'"
                + sdf.format(comanda.getUpdated())+ "',"
                + comanda.getUpdater()+ ")";
       		
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);
            rs=stm.getGeneratedKeys();
            
            if (rs.first()){
                comanda.setId(rs.getInt(1));
                response=comanda;
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;     
    }

    @Override
    public Comanda update(int id, Comanda comanda) throws Exception {
        Comanda response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            String sql="UPDATE comandas SET "
                + "`mesa`="+comanda.getMesa()+", "
                + "`camarero`="+comanda.getCamarero()+", "
                + "`estado`='"+comanda.getEstado()+"', "
                + "`factura`="+comanda.getFactura()+", "
//                + "`created`='"+sdf.format(comanda.getCreated())+"', "
//                + "`creator`="+comanda.getCreator()+", "
                + "`updated`='"+sdf.format(comanda.getUpdated())+"', "
                + "`updater`="+comanda.getUpdater()+" WHERE id="+id;
		
            response = this.load(id);
            
            if (response!=null){
                db=new Database();
                conn = db.open();
                stm = conn.createStatement();
                stm.execute(sql);
                
                stm.close();
                conn.close();
                db.close();
                
                response = comanda;
            }
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;   
    }

    @Override
    public boolean delete(int id) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;	
        String sql="DELETE FROM comandas WHERE id="+id;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response; 
    }

    @Override
    public List<Comanda> queryAll() throws Exception {
        List<Comanda> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Comanda comanda = null;
        Database db=null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sql="SELECT * FROM comandas";
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            
            while (rs.next()) {
                comanda=  new Comanda();
                
                comanda.setId( rs.getInt(1) );
                comanda.setMesa( rs.getInt(2) );
                comanda.setCamarero( rs.getInt(3) );
                comanda.setEstado( rs.getString(4) );
                comanda.setFactura( rs.getInt(5) );
                Date created = sdf.parse( rs.getString(6));
                comanda.setCreated( created );
                comanda.setCreator( rs.getInt(7) );
                Date updated = sdf.parse( rs.getString(8));
                comanda.setUpdated( updated );
                comanda.setUpdater( rs.getInt(9) );
                
                response.add(comanda);
            }
            
            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
    public List<Comanda> queryByCamarero(int id_camarero) throws Exception {
        List<Comanda> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Comanda comanda = null;
        Database db=null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sql="SELECT * FROM comandas WHERE camarero = " + id_camarero;
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            
            while (rs.next()) {
                comanda=  new Comanda();
                
                comanda.setId( rs.getInt(1) );
                comanda.setMesa( rs.getInt(2) );
                comanda.setCamarero( rs.getInt(3) );
                comanda.setEstado( rs.getString(4) );
                comanda.setFactura( rs.getInt(5) );
                Date created = sdf.parse( rs.getString(6));
                comanda.setCreated( created );
                comanda.setCreator( rs.getInt(7) );
                Date updated = sdf.parse( rs.getString(8));
                comanda.setUpdated( updated );
                comanda.setUpdater( rs.getInt(9) );
                
                response.add(comanda);
            }
            
            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
}