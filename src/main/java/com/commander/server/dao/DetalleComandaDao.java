package com.commander.server.dao;

import com.commander.server.model.Comanda;
import com.commander.server.model.Database;
import com.commander.server.model.DetalleComanda;
import com.commander.server.model.interfaces.IDetalleComandaDao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class DetalleComandaDao extends DetalleComanda implements IDetalleComandaDao{
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    
    @Override
    public DetalleComanda load(int id) throws Exception{
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        DetalleComanda response = null;
        Database db=null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sql="SELECT * FROM detalle_comandas WHERE id = " + id;
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            
            if (rs.first()){
                response = new DetalleComanda();
                
                response.setId( rs.getInt(1) );
                response.setComanda(rs.getInt(2) );
                response.setProducto(rs.getInt(3) );
                response.setCantidad( rs.getFloat(4) );
                response.setCreated( sdf.parse(rs.getString(5)) );
                response.setCreator(rs.getInt(6) );
                response.setUpdated( sdf.parse(rs.getString(7)) );
                response.setUpdater( rs.getInt(8) );
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;       
    }

    @Override
    public DetalleComanda insert(DetalleComanda detalle_comanda) throws Exception{
        DetalleComanda response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        ResultSet rs=null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
        
        try{
            String sql="INSERT INTO detalle_comandas "
                + "(`comanda`, `producto`, `cantidad`,`created`,`creator`, "
                + "`updated`, `updater`) "
                + "VALUES ("+detalle_comanda.getComanda()+", "
                + detalle_comanda.getProducto() + ","
                + detalle_comanda.getCantidad()+ ",'"
                + sdf.format(detalle_comanda.getCreated())+ "',"
                + detalle_comanda.getCreator()+ ",'"
                + sdf.format(detalle_comanda.getUpdated())+ "',"
                + detalle_comanda.getUpdater()+ ")";
        
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);
            rs=stm.getGeneratedKeys();
            
            if (rs.first()){
                detalle_comanda.setId(rs.getInt(1));
                response=detalle_comanda;
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;     
    }
    
    public List<DetalleComanda> inserts(List<DetalleComanda> detalle_comandas) throws Exception{
        List<DetalleComanda> response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        ResultSet rs=null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
        
        try{
            if (detalle_comandas.size() > 0){
                response = new ArrayList();
                db=new Database();
                conn = db.open();
                stm = conn.createStatement();
                String sql_header = "INSERT INTO detalle_comandas "
                + "(`comanda`, `producto`, `cantidad`, `created`, `creator`, "
                + "`updated`, `updater`) "
                + "VALUES ";
                
                for (int i=0; i<detalle_comandas.size(); i++){
                    DetalleComanda detalle_comanda = detalle_comandas.get(i);
                    
                    String sql_detalle_comanda = sql_header + "(" 
                        + detalle_comanda.getComanda()+", "
                        + detalle_comanda.getProducto() + ","
                        + detalle_comanda.getCantidad()+ ",'"
                        + sdf.format(detalle_comanda.getCreated())+ "',"
                        + detalle_comanda.getCreator()+ ",'"
                        + sdf.format(detalle_comanda.getUpdated())+ "',"
                        + detalle_comanda.getUpdater()+ ")";
                    
                    stm.execute(sql_detalle_comanda);
                    rs=stm.getGeneratedKeys();

                    if (rs.first()){
                        detalle_comanda.setId(rs.getInt(1));
                        response.add(detalle_comanda);
                    }
                }
            }

            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;     
    }

    @Override
    public DetalleComanda update(int id, DetalleComanda detalle_comanda) throws Exception {
        DetalleComanda response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        try{
            String sql="UPDATE detalle_comandas SET "
                + "`comanda`="+detalle_comanda.getComanda()+", "
                + "`producto`="+detalle_comanda.getProducto()+", "
                + "`cantidad`="+detalle_comanda.getCantidad()+", "
//                + "`created`='"+sdf.format(detalle_comanda.getCreated())+"', "
//                + "`creator`="+detalle_comanda.getCreator()+", "
                + "`updated`='"+sdf.format(detalle_comanda.getUpdated())+"', "
                + "`updater`="+detalle_comanda.getUpdater()+" WHERE id="+id;

            response = this.load(id);
            
            if (response!=null){
                db=new Database();
                conn = db.open();
                stm = conn.createStatement();
                stm.execute(sql);
                
                stm.close();
                conn.close();
                db.close();
                
                response = detalle_comanda;
            }
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;   
    }

    @Override
    public boolean delete(int id) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;		
        String sql="DELETE FROM detalle_comandas WHERE id="+id;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response; 
    }
    
    public boolean deleteByComanda(int id_comanda) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;		
        String sql="DELETE FROM detalle_comandas WHERE comanda="+id_comanda;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response; 
    }

    @Override
    public List<DetalleComanda> queryAll() throws Exception {
        List<DetalleComanda> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        DetalleComanda detalle_comanda = null;
        Database db=null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sql="SELECT * FROM detalle_comandas";
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            
            
            while (rs.next()) {
                detalle_comanda = new DetalleComanda();
                
                detalle_comanda.setId( rs.getInt(1) );
                detalle_comanda.setComanda(rs.getInt(2) );
                detalle_comanda.setProducto(rs.getInt(3) );
                detalle_comanda.setCantidad( rs.getFloat(4) );
                detalle_comanda.setCreated( sdf.parse(rs.getString(5)) );
                detalle_comanda.setCreator(rs.getInt(6) );
                detalle_comanda.setUpdated( sdf.parse(rs.getString(7)) );
                detalle_comanda.setUpdater( rs.getInt(8) );
                
                response.add(detalle_comanda);
            }
            
            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
    public List<DetalleComanda> queryByComanda(Comanda comanda) throws Exception{
        List<DetalleComanda> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        DetalleComanda detalle_comanda = null;
        Database db=null;
        String sql="SELECT * FROM detalle_comandas WHERE comanda = " + comanda.getId();

        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();

            while (rs.next()) {
                detalle_comanda = new DetalleComanda();

                detalle_comanda.setId( rs.getInt(1) );
                detalle_comanda.setComanda(rs.getInt(2) );
                detalle_comanda.setProducto(rs.getInt(3) );
                detalle_comanda.setCantidad( rs.getFloat(4) );
                detalle_comanda.setCreated( sdf.parse(rs.getString(5)) );
                detalle_comanda.setCreator(rs.getInt(6) );
                detalle_comanda.setUpdated( sdf.parse(rs.getString(7)) );
                detalle_comanda.setUpdater( rs.getInt(8) );

                response.add(detalle_comanda);
            }

            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;    
    }
    
    public DetalleComanda queryByIdProductoIdComanda(int id_producto, int id_comanda) throws Exception{
            DetalleComanda response=null;
            Connection conn=null;
            Statement stm=null;
            ResultSet rs=null;
            DetalleComanda detalle_comanda = null;
            Database db=null;
            String sql="SELECT * FROM detalle_comandas WHERE producto = "+id_producto+" AND comanda = " + id_comanda;

            try {		
                db = new Database();
                conn= db.open();
                stm=conn.createStatement();
                rs=stm.executeQuery(sql);

                if (rs.first()){
                    response = new DetalleComanda();

                    response.setId( rs.getInt(1) );
                    response.setComanda(rs.getInt(2) );
                    response.setProducto(rs.getInt(3) );
                    response.setCantidad( rs.getFloat(4) );
                    response.setCreated( sdf.parse(rs.getString(5)) );
                    response.setCreator(rs.getInt(6) );
                    response.setUpdated( sdf.parse(rs.getString(7)) );
                    response.setUpdater( rs.getInt(8) );
                }

                rs.close();
                stm.close();
                rs.close();
                conn.close();
                db.close();
            } catch (Exception e) {
                response = null;
                throw new Exception(e.getMessage());
            }

            return response;    
    }
}