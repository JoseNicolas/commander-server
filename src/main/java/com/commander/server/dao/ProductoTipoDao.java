package com.commander.server.dao;

import com.commander.server.model.Database;
import com.commander.server.model.ProductoTipo;
import com.commander.server.model.interfaces.IProductoTipoDao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class ProductoTipoDao extends ProductoTipo implements IProductoTipoDao{

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public ProductoTipo load(int id) throws Exception{
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        ProductoTipo response = null;
        Database db=null;

        String sql="SELECT * FROM producto_tipos WHERE id = " + id;
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            
            if (rs.first()){
                response = new ProductoTipo();
                
                response.setId( rs.getInt(1) );
                response.setNombre(rs.getString(2) );
                response.setPadre(rs.getInt(3) );
                response.setCreated( sdf.parse(rs.getString(4)) );
                response.setCreator(rs.getInt(5) );
                response.setUpdated( sdf.parse(rs.getString(6)) );
                response.setUpdater( rs.getInt(7) );
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;       
    }

    @Override
    public ProductoTipo insert(ProductoTipo producto_tipo) throws Exception{
        ProductoTipo response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        ResultSet rs=null;
        
        try{
            String sql="INSERT INTO producto_tipos "
                + "(`nombre`, `padre`, `created`, `creator`, `updated`, `updater`) "
                + "VALUES ('"+producto_tipo.getNombre()+"',"
                + producto_tipo.getPadre() + ",'"
                + sdf.format(producto_tipo.getCreated())+ "',"
                + producto_tipo.getCreator()+ ",'"
                + sdf.format(producto_tipo.getUpdated())+ "',"
                + producto_tipo.getUpdater()+ ")";
        
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);
            rs=stm.getGeneratedKeys();
            
            if (rs.first()){
                producto_tipo.setId(rs.getInt(1));
                response=producto_tipo;
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;     
    }

    @Override
    public ProductoTipo update(int id, ProductoTipo producto_tipo) throws Exception {
        ProductoTipo response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        
        try{
            
            String sql="UPDATE producto_tipos SET "
                + "`nombre`='"+producto_tipo.getNombre()+"', "
                + "`padre`="+producto_tipo.getPadre()+", "
//                + "`created`='"+sdf.format(usuario.getCreated())+"', "
//                + "`creator`="+usuario.getCreator()+", "
                + "`updated`='"+sdf.format(producto_tipo.getUpdated())+"', "
                + "`updater`="+producto_tipo.getUpdater()+" WHERE id="+id;

            response = this.load(id);
            
            if (response!=null){
                db=new Database();
                conn = db.open();
                stm = conn.createStatement();
                stm.execute(sql);
                
                stm.close();
                conn.close();
                db.close();
                
                response = producto_tipo;
            }
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;   
    }

    @Override
    public boolean delete(int id) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        		
        String sql="DELETE FROM producto_tipos WHERE id="+id;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response; 
    }

    @Override
    public List<ProductoTipo> queryAll() throws Exception {
        List<ProductoTipo> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        ProductoTipo producto_tipo = null;
        Database db=null;

        String sql="SELECT * FROM producto_tipos";
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            
            while (rs.next()) {
                producto_tipo = new ProductoTipo();
                
                producto_tipo.setId( rs.getInt(1) );
                producto_tipo.setNombre(rs.getString(2) );
                producto_tipo.setPadre(rs.getInt(3) );
                producto_tipo.setCreated( sdf.parse(rs.getString(4)) );
                producto_tipo.setCreator(rs.getInt(5) );
                producto_tipo.setUpdated( sdf.parse(rs.getString(6)) );
                producto_tipo.setUpdater( rs.getInt(7) );
                
                response.add(producto_tipo);
            }
            
            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
}