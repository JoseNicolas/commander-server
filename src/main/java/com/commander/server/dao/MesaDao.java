package com.commander.server.dao;

import com.commander.server.model.Database;
import com.commander.server.model.Mesa;
import com.commander.server.model.interfaces.IMesaDao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class MesaDao extends Mesa implements IMesaDao{

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public Mesa load(int id) throws Exception{
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Mesa response = null;
        Database db=null;

        String sql="SELECT * FROM mesas WHERE id = " + id;
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            
            if (rs.first()){
                response = new Mesa();
                
                response.setId( rs.getInt(1) );
                response.setNombre(rs.getString(2) );
                response.setDescripcion(rs.getString(3) );
                response.setCreated( sdf.parse(rs.getString(4)) );
                response.setCreator(rs.getInt(5) );
                response.setUpdated( sdf.parse(rs.getString(6)) );
                response.setUpdater( rs.getInt(7) );
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;       
    }

    @Override
    public Mesa insert(Mesa mesa) throws Exception{
        Mesa response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        ResultSet rs=null;
            
        try{
            
            String sql="INSERT INTO mesas "
                + "(`nombre`, `descripcion`, `created`, `creator`, `updated`, `updater`) "
                + "VALUES ('"+mesa.getNombre()+"','"
                + mesa.getDescripcion() + "','"
                + sdf.format(mesa.getCreated())+ "',"
                + mesa.getCreator()+ ",'"
                + sdf.format(mesa.getUpdated())+ "',"
                + mesa.getUpdater()+ ")";
        
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);
            rs=stm.getGeneratedKeys();
            
            if (rs.first()){
                mesa.setId(rs.getInt(1));
                response=mesa;
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;     
    }

    @Override
    public Mesa update(int id, Mesa mesa) throws Exception {
        Mesa response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        
        try{
            String sql="UPDATE mesas SET "
                + "`nombre`='"+mesa.getNombre()+"', "
                + "`descripcion`='"+mesa.getDescripcion()+"', "
//                + "`created`='"+sdf.format(mesa.getCreated())+"', "
//                + "`creator`="+mesa.getCreator()+", "
                + "`updated`='"+sdf.format(mesa.getUpdated())+"', "
                + "`updater`="+mesa.getUpdater()+" WHERE id="+id;

            response = this.load(id);
            
            if (response!=null){
                db=new Database();
                conn = db.open();
                stm = conn.createStatement();
                stm.execute(sql);
                
                stm.close();
                conn.close();
                db.close();
                
                response = mesa;
            }
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;   
    }

    @Override
    public boolean delete(int id) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        		
        String sql="DELETE FROM mesas WHERE id="+id;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response; 
    }

    @Override
    public List<Mesa> queryAll() throws Exception {
        List<Mesa> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Mesa mesa = null;
        Database db=null;

        String sql="SELECT * FROM mesas";
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            while (rs.next()) {
                mesa = new Mesa();
                
                mesa.setId( rs.getInt(1) );
                mesa.setNombre(rs.getString(2) );
                mesa.setDescripcion(rs.getString(3) );
                mesa.setCreated( sdf.parse(rs.getString(4)) );
                mesa.setCreator(rs.getInt(5) );
                mesa.setUpdated( sdf.parse(rs.getString(6)) );
                mesa.setUpdater( rs.getInt(7) );
                
                response.add(mesa);
            }
            
            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
}