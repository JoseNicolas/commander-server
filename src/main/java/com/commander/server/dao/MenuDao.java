package com.commander.server.dao;

import com.commander.server.model.Database;
import com.commander.server.model.Menu;
import com.commander.server.model.interfaces.IMenuDao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class MenuDao extends Menu implements IMenuDao{
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public Menu load(int id) throws Exception{
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Menu response = null;
        Database db=null;

        String sql="SELECT * FROM menus WHERE id = " + id;
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            if (rs.first()){
                response = new Menu();
                
                response.setId( rs.getInt(1) );
                response.setNombre(rs.getString(2) );
                response.setDescripcion(rs.getString(3) );
                response.setCreated( sdf.parse(rs.getString(4)) );
                response.setCreator(rs.getInt(5) );
                response.setUpdated( sdf.parse(rs.getString(6)) );
                response.setUpdater( rs.getInt(7) );
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;       
    }

    @Override
    public Menu insert(Menu menu) throws Exception{
        Menu response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        ResultSet rs=null;
        		
        String sql="INSERT INTO menus "
                + "(`nombre`, `descripcion`, `created`, `creator`, `updated`, `updater`) "
                + "VALUES ('"+menu.getNombre()+"','"
                + menu.getDescripcion() + "','"
                + sdf.format(menu.getCreated())+ "',"
                + menu.getCreator()+ ",'"
                + sdf.format(menu.getUpdated())+ "',"
                + menu.getUpdater()+ ")";
        
        try {		
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);
            rs=stm.getGeneratedKeys();
            
            if (rs.first()){
                menu.setId(rs.getInt(1));
                response=menu;
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;     
    }

    @Override
    public Menu update(int id, Menu menu) throws Exception {
        Menu response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        		
        String sql="UPDATE menus SET "
                + "`nombre`='"+menu.getNombre()+"', "
                + "`descripcion`='"+menu.getDescripcion()+"', "
//                + "`created`='"+sdf.format(menu.getCreated())+"', "
//                + "`creator`="+menu.getCreator()+", "
                + "`updated`='"+sdf.format(menu.getUpdated())+"', "
                + "`updater`="+menu.getUpdater()+" WHERE id="+id;

        try {		
            response = this.load(id);
            
            if (response!=null){
                db=new Database();
                conn = db.open();
                stm = conn.createStatement();
                stm.execute(sql);
                
                stm.close();
                conn.close();
                db.close();
                
                response = menu;
            }
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;   
    }

    @Override
    public boolean delete(int id) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        		
        String sql="DELETE FROM menus WHERE id="+id;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response; 
    }

    @Override
    public List<Menu> queryAll() throws Exception {
        List<Menu> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Menu menu = null;
        Database db=null;

        String sql="SELECT * FROM menus";
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            
            while (rs.next()) {
                menu = new Menu();
                
                menu.setId( rs.getInt(1) );
                menu.setNombre(rs.getString(2) );
                menu.setDescripcion(rs.getString(3) );
                menu.setCreated( sdf.parse(rs.getString(4)) );
                menu.setCreator(rs.getInt(5) );
                menu.setUpdated( sdf.parse(rs.getString(6)) );
                menu.setUpdater( rs.getInt(7) );
                
                response.add(menu);
            }
            
            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
}