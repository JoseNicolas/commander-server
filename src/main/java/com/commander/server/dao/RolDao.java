package com.commander.server.dao;

import com.commander.server.model.Database;
import com.commander.server.model.Rol;
import com.commander.server.model.interfaces.IRolDao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class RolDao extends Rol implements IRolDao{

    @Override
    public Rol load(int id) throws Exception{
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Rol response = null;
        Database db=null;

        String sql="SELECT * FROM roles WHERE id = " + id;
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            
            if (rs.first()){
                response = new Rol();
                
                response.setId( rs.getInt(1) );
                response.setNombre(rs.getString(2) );
                response.setDescripcion(rs.getString(3) );
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;       
    }

    @Override
    public Rol insert(Rol rol) throws Exception{
        Rol response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        ResultSet rs=null;
        	
        try{
            
            String sql="INSERT INTO roles "
                + "(`nombre`, `descripcion`) "
                + "VALUES ('"+rol.getNombre()+"','"
                + rol.getDescripcion() + "')";
        
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);
            rs=stm.getGeneratedKeys();
            
            if (rs.first()){
                rol.setId(rs.getInt(1));
                response=rol;
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;     
    }

    @Override
    public Rol update(int id, Rol rol) throws Exception {
        Rol response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        		
        try{
            String sql="UPDATE roles SET "
                + "`nombre`='"+rol.getNombre()+"', "
                + "`password`='"+rol.getDescripcion()+"' WHERE id="+id;

            response = this.load(id);
            
            if (response!=null){
                db=new Database();
                conn = db.open();
                stm = conn.createStatement();
                stm.execute(sql);
                
                stm.close();
                conn.close();
                db.close();
            }
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;   
    }

    @Override
    public boolean delete(int id) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        		
        String sql="DELETE FROM roles WHERE id="+id;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response; 
    }

    @Override
    public List<Rol> queryAll() throws Exception {
        List<Rol> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Rol rol = null;
        Database db=null;

        String sql="SELECT * FROM usuarios";
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            
            while (rs.next()) {
                rol = new Rol();
                
                rol.setId( rs.getInt(1) );
                rol.setNombre(rs.getString(2) );
                rol.setDescripcion(rs.getString(3) );
                
                response.add(rol);
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
}