package com.commander.server.dao;

import com.commander.server.model.Database;
import com.commander.server.model.Producto;
import com.commander.server.model.ProductoTipo;
import com.commander.server.model.interfaces.IProductoDao;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class ProductoDao extends Producto implements IProductoDao{
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public Producto load(int id) throws Exception{
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Producto response = null;
        Database db=null;

        String sql="SELECT * FROM productos WHERE id = " + id;
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            
            if (rs.first()){
                response = new Producto();
                
                response.setId( rs.getInt(1) );
                response.setCodigo( rs.getString(2) );
                response.setNombre( rs.getString(3) );
                response.setDescripcion( rs.getString(4) );
                response.setTipo( rs.getInt(5) );
                response.setPrecio_unit( rs.getFloat(6) );
                response.setImagen( rs.getString(7));
                response.setCreated( sdf.parse(rs.getString(8)) );
                response.setCreator(rs.getInt(9) );
                response.setUpdated( sdf.parse(rs.getString(10)) );
                response.setUpdater( rs.getInt(11) );
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;       
    }

    @Override
    public Producto insert(Producto producto) throws Exception{
        Producto response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        ResultSet rs=null;
        
        try{
            String sql="INSERT INTO productos "
                + "(`codigo`, `nombre`, `descripcion`, `tipo`, `precio_unit`, "
                + "`imagen`, `created`, `creator`, `updated`, `updater`) "
                + "VALUES ('"+producto.getCodigo()+"','"
                + producto.getNombre() + "','"
                + producto.getDescripcion()+ "',"
                + producto.getTipo()+ ","
                + producto.getPrecio_unit()+ ",'"
                + producto.getImagen()+ "','"
                + sdf.format( producto.getCreated())+ "',"
                + producto.getCreator()+ ",'"
                + sdf.format( producto.getUpdated())+ "',"
                + producto.getUpdater()+ ")";
        
            db=new Database();
            conn = db.open();
            
            stm = conn.createStatement();
            stm.execute(sql);
            rs=stm.getGeneratedKeys();
            
            if (rs.first()){
                producto.setId(rs.getInt(1));
                response=producto;
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;     
    }

    @Override
    public Producto update(int id, Producto producto) throws Exception {
        Producto response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        PreparedStatement st;

        
        try{
            String sql="UPDATE productos SET "
                + "`codigo`='"+producto.getCodigo()+"', "
                + "`nombre`='"+producto.getNombre()+"', "
                + "`descripcion`='"+producto.getDescripcion()+"', "
                + "`tipo`="+producto.getTipo()+", "
                + "`precio_unit`="+producto.getPrecio_unit()+", "
                + "`imagen`='"+producto.getImagen()+"', "
//                + "`created`='"+sdf.format(producto.getCreated())+"', "
//                + "`creator`="+producto.getCreator()+", "
                + "`updated`='"+sdf.format(producto.getUpdated())+"', "
                + "`updater`="+producto.getUpdater()+" WHERE id="+id;
            
            response = this.load(id);
            
            if (response!=null){
                db=new Database();
                conn = db.open();
                stm = conn.createStatement();
                stm.execute(sql);
                
                stm.close();
                conn.close();
                db.close();
                
                response = producto;
            }
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;   
    }

    @Override
    public boolean delete(int id) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        		
        String sql="DELETE FROM productos WHERE id="+id;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response; 
    }

    @Override
    public List<Producto> queryAll() throws Exception {
        List<Producto> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Producto producto = null;
        Database db=null;

        String sql="SELECT * FROM productos";
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            
            while (rs.next()) {
                producto = new Producto();
                
                producto.setId( rs.getInt(1) );
                producto.setCodigo(rs.getString(2) );
                producto.setNombre(rs.getString(3) );
                producto.setDescripcion( rs.getString(4) );
                producto.setTipo(rs.getInt(5) );
                producto.setPrecio_unit( rs.getFloat(6) );
                producto.setImagen( rs.getString(7) );
                producto.setCreated( sdf.parse(rs.getString(8)) );
                producto.setCreator(rs.getInt(9) );
                producto.setUpdated( sdf.parse(rs.getString(10)) );
                producto.setUpdater( rs.getInt(11) );
                
                response.add(producto);
            }
            
            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
    public List<Producto> queryByTipo(int id_producto_tipo) throws Exception{
        List<Producto> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        Producto producto = null;
        Database db=null;
        String sql="SELECT * FROM productos WHERE tipo = " + id_producto_tipo;

        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();

            while (rs.next()) {
                producto = new Producto();

                producto.setId( rs.getInt(1) );
                producto.setCodigo(rs.getString(2) );
                producto.setNombre(rs.getString(3) );
                producto.setDescripcion(rs.getString(4) );
                producto.setTipo(rs.getInt(5) );
                producto.setPrecio_unit(rs.getFloat(6) );
                producto.setImagen(rs.getString(7) );
                producto.setCreated( sdf.parse(rs.getString(8)) );
                producto.setCreator(rs.getInt(9) );
                producto.setUpdated( sdf.parse(rs.getString(10)) );
                producto.setUpdater( rs.getInt(11) );

                response.add(producto);
            }

            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;  
    }
}