package com.commander.server.dao;

import com.commander.server.model.Database;
import com.commander.server.model.DetalleComanda;
import com.commander.server.model.DetalleMenu;
import com.commander.server.model.Menu;
import com.commander.server.model.interfaces.IDetalleMenuDao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class DetalleMenuDao extends DetalleMenu implements IDetalleMenuDao{
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    @Override
    public DetalleMenu load(int id) throws Exception{
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        DetalleMenu response = null;
        Database db=null;
        String sql="SELECT * FROM detalle_menus WHERE id = " + id;
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            if (rs.first()){
                response = new DetalleMenu();
                
                response.setId( rs.getInt(1) );
                response.setMenu( rs.getInt(2) );
                response.setProducto( rs.getInt(3) );
                response.setPlato( rs.getInt(4) );
                response.setCreated( sdf.parse( rs.getString(5)) );
                response.setCreator( rs.getInt(6) );
                response.setUpdated( sdf.parse( rs.getString(7)) );
                response.setUpdater( rs.getInt(8) );
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }

        return response;       
    }

    @Override
    public DetalleMenu insert(DetalleMenu detalle_menu) throws Exception{
        DetalleMenu response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        ResultSet rs=null;
		
        try{
            String sql="INSERT INTO detalle_menus "
                + "(`menu`, `producto`, `plato`, `created`, `creator`, `updated`, `updater`) "
                + "VALUES ("+detalle_menu.getMenu()+","
                + detalle_menu.getProducto() + ","
                + detalle_menu.getPlato() + ",'"
                + sdf.format(detalle_menu.getCreated())+ "',"
                + detalle_menu.getCreator()+ ",'"
                + sdf.format(detalle_menu.getUpdated())+ "',"
                + detalle_menu.getUpdater()+ ")";
        
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);
            rs=stm.getGeneratedKeys();
            
            if (rs.first()){
                detalle_menu.setId(rs.getInt(1));
                response=detalle_menu;
            }
            
            rs.close();
            stm.close();
            conn.close();
            db.close();
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;     
    }

    @Override
    public DetalleMenu update(int id, DetalleMenu detalle_menu) throws Exception {
        DetalleMenu response=null;
        Statement stm= null;
        Connection conn=null;
        Database db=null;

        try{
            String sql="UPDATE detalle_menus SET "
                + "`menu`="+detalle_menu.getMenu()+", "
                + "`producto`="+detalle_menu.getProducto()+", "
                + "`plato`="+detalle_menu.getPlato()+", "
//                + "`created`='"+sdf.format(detalle_menu.getCreated())+"', "
//                + "`creator`="+detalle_menu.getCreator()+", "
                + "`updated`='"+sdf.format(detalle_menu.getUpdated())+"', "
                + "`updater`="+detalle_menu.getUpdater()+" WHERE id="+id;
		
            response = this.load(id);
            
            if (response!=null){
                db=new Database();
                conn = db.open();
                stm = conn.createStatement();
                stm.execute(sql);
                
                stm.close();
                conn.close();
                db.close();
                
                response = detalle_menu;
            }
        } catch (Exception e) {
                response=null;
                throw new Exception(e.getMessage());
        }
        
        return response;   
    }

    @Override
    public boolean delete(int id) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        String sql="DELETE FROM detalle_menus WHERE id="+id;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response; 
    }

    @Override
    public List<DetalleMenu> queryAll() throws Exception {
        List<DetalleMenu> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        DetalleMenu detalle_menu = null;
        Database db=null;
        String sql="SELECT * FROM detalle_menus";
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            
            while (rs.next()) {
                detalle_menu = new DetalleMenu();
                
                detalle_menu.setId( rs.getInt(1) );
                detalle_menu.setMenu( rs.getInt(2) );
                detalle_menu.setProducto( rs.getInt(3) );
                detalle_menu.setPlato( rs.getInt(4) );
                detalle_menu.setCreated( sdf.parse( rs.getString(5)) );
                detalle_menu.setCreator( rs.getInt(6) );
                detalle_menu.setUpdated( sdf.parse( rs.getString(7)) );
                detalle_menu.setUpdater( rs.getInt(8) );
                
                response.add(detalle_menu);
            }
            
            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Obtiene un listado de detalle menu en funcion del menu especificado
     * @param menu
     * @return
     * @throws Exception 
     */
    public List<DetalleMenu> queryByMenu(Menu menu) throws Exception{
        List<DetalleMenu> response=null;
        Connection conn=null;
        Statement stm=null;
        ResultSet rs=null;
        DetalleMenu detalle_menu = null;
        Database db=null;
        String sql="SELECT * FROM detalle_menus WHERE menu = " + menu.getId();
        
        try {		
            db = new Database();
            conn= db.open();
            stm=conn.createStatement();
            rs=stm.executeQuery(sql);
            response=new ArrayList<>();
            
            while (rs.next()) {
                detalle_menu = new DetalleMenu();
                
                detalle_menu.setId( rs.getInt(1) );
                detalle_menu.setMenu( rs.getInt(2) );
                detalle_menu.setProducto( rs.getInt(3) );
                detalle_menu.setPlato( rs.getInt(4) );
                detalle_menu.setCreated( sdf.parse( rs.getString(5)) );
                detalle_menu.setCreator( rs.getInt(6) );
                detalle_menu.setUpdated( sdf.parse( rs.getString(7)) );
                detalle_menu.setUpdater( rs.getInt(8) );
                
                response.add(detalle_menu);
            }
            
            rs.close();
            stm.close();
            rs.close();
            conn.close();
            db.close();
        } catch (Exception e) {
            response = null;
            throw new Exception(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Obtiene listado de detalle 
     * @param id_producto
     * @param id_menu
     * @return
     * @throws Exception 
     */
    public DetalleMenu queryByIdProductoIdMenu(int id_producto, int id_menu) throws Exception{
            DetalleMenu response=null;
            Connection conn=null;
            Statement stm=null;
            ResultSet rs=null;
            DetalleMenu detalle_menu = null;
            Database db=null;
            String sql="SELECT * FROM detalle_menus WHERE producto = "+id_producto+" AND menu = " + id_menu;

            try {		
                db = new Database();
                conn= db.open();
                stm=conn.createStatement();
                rs=stm.executeQuery(sql);

                if (rs.first()){
                    response = new DetalleMenu();

                    response.setId( rs.getInt(1) );
                    response.setMenu(rs.getInt(2) );
                    response.setProducto(rs.getInt(3) );
                    response.setPlato( rs.getInt(4) );
                    response.setCreated( sdf.parse(rs.getString(5)) );
                    response.setCreator(rs.getInt(6) );
                    response.setUpdated( sdf.parse(rs.getString(7)) );
                    response.setUpdater( rs.getInt(8) );
                }

                rs.close();
                stm.close();
                rs.close();
                conn.close();
                db.close();
            } catch (Exception e) {
                response = null;
                throw new Exception(e.getMessage());
            }

            return response;    
    }
    
    /**
     * Elimina los detalle de menu que pertenece a un menu concreto
     * @param id_menu
     * @return
     * @throws Exception 
     */
    public boolean deleteByIdMenu(int id_menu) throws Exception {
        boolean response=false;
        Statement stm= null;
        Connection conn=null;
        Database db=null;
        String sql="DELETE FROM detalle_menus WHERE menu="+id_menu;

        try {		            
            db=new Database();
            conn = db.open();
            stm = conn.createStatement();
            stm.execute(sql);

            stm.close();
            conn.close();
            db.close();
            response=true;
        } catch (Exception e) {
                response=false;
                throw new Exception(e.getMessage());
        }
        
        return response;
    }
      
    
}