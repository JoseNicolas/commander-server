package com.commander.server.dao;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class DaoFactory {
    public static ComandaDao getComandaDao(){
        return new ComandaDao();
    }
    
    public static DetalleComandaDao getDetalleComandaDao(){
        return new DetalleComandaDao();
    }
    
    public static DetalleMenuDao getDetalleMenuDao(){
        return new DetalleMenuDao();
    }
    
    public static FacturaDao getFacturaDao(){
        return new FacturaDao();
    }
    
    public static MenuDao getMenuDao(){
        return new MenuDao();
    }
    
    public static MesaDao getMesaDao(){
        return new MesaDao();
    }
    
    public static ProductoDao getProductoDao(){
        return new ProductoDao();
    }
    
    public static ProductoTipoDao getProductoTipoDao(){
        return new ProductoTipoDao();
    }
    
    public static RolDao getRolDao(){
        return new RolDao();
    }
    
    public static UsuarioDao getUsuarioDao(){
        return new UsuarioDao();
    }
}
