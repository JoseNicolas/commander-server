package com.commander.server.model.interfaces;

import com.commander.server.model.Menu;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public interface IMenuDao {
    public Menu load(int id) throws Exception;
    public Menu insert(Menu menu) throws Exception;
    public Menu update(int id, Menu menu) throws Exception;
    public boolean delete(int id) throws Exception;
    public List<Menu> queryAll() throws Exception;
}
