package com.commander.server.model.interfaces;

import com.commander.server.model.Rol;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public interface IRolDao {
    public Rol load(int id) throws Exception;
    public Rol insert(Rol rol) throws Exception;
    public Rol update(int id, Rol rol) throws Exception;
    public boolean delete(int id) throws Exception;
    public List<Rol> queryAll() throws Exception;
}
