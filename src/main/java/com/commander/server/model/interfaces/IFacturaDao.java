package com.commander.server.model.interfaces;

import com.commander.server.model.Factura;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public interface IFacturaDao {
    public Factura load(int id) throws Exception;
    public Factura insert(Factura factura) throws Exception;
    public Factura update(int id, Factura factura) throws Exception;
    public boolean delete(int id) throws Exception;
    public List<Factura> queryAll() throws Exception;
}
