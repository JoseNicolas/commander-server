package com.commander.server.model.interfaces;

import com.commander.server.model.Mesa;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public interface IMesaDao {
    public Mesa load(int id) throws Exception;
    public Mesa insert(Mesa mesa) throws Exception;
    public Mesa update(int id, Mesa mesa) throws Exception;
    public boolean delete(int id) throws Exception;
    public List<Mesa> queryAll() throws Exception;
}
