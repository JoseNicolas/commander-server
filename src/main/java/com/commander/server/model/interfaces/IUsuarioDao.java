package com.commander.server.model.interfaces;

import com.commander.server.model.Usuario;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public interface IUsuarioDao {
    public Usuario load(int id) throws Exception;
    public Usuario insert(Usuario usuario) throws Exception;
    public Usuario update(int id, Usuario usuario) throws Exception;
    public boolean delete(int id) throws Exception;
    public List<Usuario> queryAll() throws Exception;
}
