package com.commander.server.model.interfaces;

import com.commander.server.model.Comanda;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public interface IComandaDao {
    public Comanda load(int id) throws Exception;
    public Comanda insert(Comanda comanda) throws Exception;
    public Comanda update(int id, Comanda comanda) throws Exception;
    public boolean delete(int id) throws Exception;
    public List<Comanda> queryAll() throws Exception;
}
