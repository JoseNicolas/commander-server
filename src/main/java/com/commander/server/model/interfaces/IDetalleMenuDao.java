package com.commander.server.model.interfaces;

import com.commander.server.model.DetalleMenu;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public interface IDetalleMenuDao {
    public DetalleMenu load(int id) throws Exception;
    public DetalleMenu insert(DetalleMenu detalle_menu) throws Exception;
    public DetalleMenu update(int id, DetalleMenu detalle_menu) throws Exception;
    public boolean delete(int id) throws Exception;
    public List<DetalleMenu> queryAll() throws Exception;
}
