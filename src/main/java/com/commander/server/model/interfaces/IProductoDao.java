package com.commander.server.model.interfaces;

import com.commander.server.model.Producto;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public interface IProductoDao {
    public Producto load(int id) throws Exception;
    public Producto insert(Producto producto) throws Exception;
    public Producto update(int id, Producto producto) throws Exception;
    public boolean delete(int id) throws Exception;
    public List<Producto> queryAll() throws Exception;
}
