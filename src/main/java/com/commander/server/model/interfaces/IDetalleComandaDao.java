package com.commander.server.model.interfaces;

import com.commander.server.model.DetalleComanda;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public interface IDetalleComandaDao {
    public DetalleComanda load(int id) throws Exception;
    public DetalleComanda insert(DetalleComanda detalle_comanda) throws Exception;
    public DetalleComanda update(int id, DetalleComanda detalle_comanda) throws Exception;
    public boolean delete(int id) throws Exception;
    public List<DetalleComanda> queryAll() throws Exception;
}
