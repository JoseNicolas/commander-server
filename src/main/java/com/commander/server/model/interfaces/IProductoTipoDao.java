package com.commander.server.model.interfaces;

import com.commander.server.model.ProductoTipo;
import java.util.List;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public interface IProductoTipoDao {
    public ProductoTipo load(int id) throws Exception;
    public ProductoTipo insert(ProductoTipo productoTipo) throws Exception;
    public ProductoTipo update(int id, ProductoTipo productoTipo) throws Exception;
    public boolean delete(int id) throws Exception;
    public List<ProductoTipo> queryAll() throws Exception;
}
