package com.commander.server.model;

import com.commander.server.utils.Config;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class Database {
    private String user;
    private String password;
    private String database;
    private String host;
    private String port;
    private Connection conn;
    
    public Database() throws Exception{
        try{
            this.host = Config.getProperty("host");
            this.port = Config.getProperty("port");
            this.database = Config.getProperty("database");
            this.user = Config.getProperty("user");
            this.password = Config.getProperty("password");
        }catch(Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
    
    public Database(String host, String port, String database, String user, String password){
        this.host = host;
        this.port = port;
        this.database = database;
        this.user = user;
        this.password = password;
    }
    
    public Connection open() throws Exception {
        Connection conn = null;
        
        if (this.conn == null){
           try{
               String url = "jdbc:mysql://"+ this.host +":"+ this.port +"/"+this.database+"?user="+ this.user +"&password="+ this.password;
               conn = DriverManager.getConnection(url);

               if (conn != null){
                   this.conn = conn;
               }

           }catch(Exception ex){
               throw new Exception(ex.getMessage());
           }

           return conn;
           
        }else{
              conn = this.conn;
        }
        
        return conn;
    }
    
    public boolean close() throws SQLException{
        boolean response = false;
        
        if (this.conn != null){
            this.conn.close();
        }
        
        return response;
    }
}
