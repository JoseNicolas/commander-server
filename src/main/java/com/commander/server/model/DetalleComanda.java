package com.commander.server.model;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 * 2019/04/07 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class DetalleComanda implements Cloneable{
    // PROPIEDADES
    private int id;
    private int comanda;
    private int producto;
    private float cantidad;
    private Date created;
    private int creator;
    private Date updated;
    private int updater;
    
    // CONSTRUCTORES
    public DetalleComanda() {
    }

    public DetalleComanda(int comanda, int producto, float cantidad, Date created, int creator, Date updated, int updater) {
        this.comanda = comanda;
        this.producto = producto;
        this.cantidad = cantidad;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }
    
    public DetalleComanda(int id, int comanda, int producto, float cantidad, Date created, int creator, Date updated, int updater) {
        this.id = id;
        this.comanda = comanda;
        this.producto = producto;
        this.cantidad = cantidad;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }
            
    // GETTERS & SETTERS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getComanda() {
        return comanda;
    }

    public void setComanda(int comanda) {
        this.comanda = comanda;
    }

    public int getProducto() {
        return producto;
    }

    public void setProducto(int producto) {
        this.producto = producto;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getUpdater() {
        return updater;
    }

    public void setUpdater(int updater) {
        this.updater = updater;
    }


    // METHODS
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        hash = 97 * hash + Objects.hashCode(this.comanda);
        hash = 97 * hash + Objects.hashCode(this.producto);
        hash = 97 * hash + Objects.hashCode(this.cantidad);
        hash = 97 * hash + Objects.hashCode(this.created);
        hash = 97 * hash + Objects.hashCode(this.creator);
        hash = 97 * hash + Objects.hashCode(this.updated);
        hash = 97 * hash + Objects.hashCode(this.updater);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetalleComanda other = (DetalleComanda) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.comanda, other.comanda)) {
            return false;
        }
        if (!Objects.equals(this.producto, other.producto)) {
            return false;
        }
        if (!Objects.equals(this.cantidad, other.cantidad)) {
            return false;
        }
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        if (!Objects.equals(this.creator, other.creator)) {
            return false;
        }
        if (!Objects.equals(this.updated, other.updated)) {
            return false;
        }
        if (!Objects.equals(this.updater, other.updater)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DetalleComanda{" + "id=" + id + ", comanda=" + comanda + ", productos=" + producto + ", cantidad=" + cantidad + ", created=" + created + ", creator=" + creator + ", updated=" + updated + ", updater=" + updater + '}';
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException{
        Object obj=null;
        obj=super.clone();
        return obj;
    }
}
