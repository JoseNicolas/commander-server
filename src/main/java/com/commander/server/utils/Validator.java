package com.commander.server.utils;

import com.commander.server.controller.MainFormController;
import java.util.logging.Level;
import org.apache.commons.lang3.exception.ExceptionUtils;

/*
 * 2019/04/29
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class Validator {
    public static boolean isEmpty(String value){
        boolean is_empty = false;
        try{
            if (value.length() == 0){
                is_empty = true;
            }
        }catch(Exception e){
            MainFormController.Logger(Validator.class, ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
        
        return is_empty;
    }
    
    public static boolean isNull(Object obj){
        boolean is_null = false;
        try{
            if (obj == null){
                is_null = true;
            }
        }catch(Exception e){
            MainFormController.Logger(Validator.class, ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
        
        return is_null;
    }
    
    public static boolean isNotNull(Object obj){
        boolean is_not_null = false;
        try{
            if (obj != null){
                is_not_null = true;
            }
        }catch(Exception e){
            MainFormController.Logger(Validator.class, ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
        
        return is_not_null;
    }
    
    public static boolean isGreatherThan(String value, int length){
        boolean is_greather = false;
        try{
            if (value.length() > length){
                is_greather = true;
            }
        }catch(Exception e){
            MainFormController.Logger(Validator.class, ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
        
        return is_greather;
    }
    
    public static boolean isLowerThan(String value, int length){
        boolean is_lower = false;
        try{
            if (value.length() < length){
                is_lower = true;
            }
        }catch(Exception e){
            MainFormController.Logger(Validator.class, ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
        
        return is_lower;
    }
    
    public static boolean isNumber(String value){
        boolean is_number = true;
        
        try{
            Double.parseDouble(value);
        }catch(Exception e){
            is_number = false;
            MainFormController.Logger(Validator.class, ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
        
        return is_number;
    }
}
