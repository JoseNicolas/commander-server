package com.commander.server;

import com.commander.server.controller.MainFormController;
import com.commander.server.dao.DaoFactory;
import com.commander.server.model.*;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.kryonet.util.InputStreamSender;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.apache.commons.lang3.exception.ExceptionUtils;

/*
 * 2019/04/29
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class ServerAPI {
    // ========================================================================
    //                              CLIENT APP
    // ========================================================================
    public static Response getClientAppBaseData(Request request){
        Response response = new Response();
        List<Comanda> comandas;
        List<Mesa> mesas;
        Usuario user_logged;
        
        try{
            user_logged = (Usuario) request.getData();
            
            mesas = DaoFactory.getMesaDao().queryAll();
            comandas = DaoFactory.getComandaDao().queryByCamarero(user_logged.getId());
            
            List list = new ArrayList();
            list.add(mesas);
            list.add(comandas);
            
            response.setData(list);
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
           
        }catch (Exception e){
            MainFormController.Logger(ServerAPI.class, ExceptionUtils.getStackTrace(e), Level.SEVERE);
            response.setStatus(false);
            response.setError(true);
            response.setMessage("Las credenciales introducidas son incorrectas");
        }
        
        return response;
    }
    
    // ========================================================================
    //                              LOGIN
    // ========================================================================
    /**
     * Valida las credenciales de acceso a la aplicacion
     * @param request
     * @return 
     */
    public static Response validarLogin(Request request){
        Response response = new Response();
        Usuario user_login;
        
        try{
            
            user_login = (Usuario) request.getData();
            
            if (user_login instanceof Usuario){
                Usuario usuario = DaoFactory.getUsuarioDao().queryByUsuario(user_login.getUsuario());
                if (usuario != null){
                    if (usuario.getPassword().equals(user_login.getPassword())){
                        response.setStatus(true);
                        response.setError(false);
                        response.setData(usuario);
                        response.setMessage("Credenciales correctas. Bienvenido " + usuario.getNombre() + "!");
                    }else{
                        response.setStatus(false);
                        response.setError(true);
                        response.setMessage("Las credenciales introducidas son incorrectas");
                    }
                }else{
                    response.setStatus(false);
                    response.setError(true);
                    response.setMessage("Las credenciales introducidas son incorrectas");
                }
            }else{
                response.setStatus(false);
                response.setError(true);
                response.setMessage("Las credenciales introducidas son incorrectas");
            }
        }catch (Exception e){
            MainFormController.Logger(ServerAPI.class, ExceptionUtils.getStackTrace(e), Level.SEVERE);
            response.setStatus(false);
            response.setError(true);
            response.setMessage("Las credenciales introducidas son incorrectas");
        }
        
        return response;
    }
    
    // ========================================================================
    //                              USUARIO
    // ========================================================================
    /**
     * Crea un nuevo usuario en el sistema.
     * @param request
     * @return 
     */
    public static Response createUsuario(Request request){
        Response response = new Response();
        Usuario usuario;
        
        try{
            usuario = (Usuario) request.getData();
            response.setData (DaoFactory.getUsuarioDao().insert(usuario));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera un usuario registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response readUsuario(Request request){
        Response response = new Response();
        int id_usuario;
        
        try{
            id_usuario = (int) request.getData();
            response.setData (DaoFactory.getUsuarioDao().load(id_usuario));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera una lista con todos los usuariso registrados en el sistema.
     * @param request
     * @return 
     */
    public static Response readUsuarios(Request request){
        Response response = new Response();
        
        try{
            response.setData (DaoFactory.getUsuarioDao().queryAll());
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Actualiza la información de un usuario
     * @param request
     * @return 
     */
    public static Response updateUsuario(Request request){
        Response response = new Response();
        Usuario usuario;
        
        try{
            usuario = (Usuario) request.getData();
            response.setData(DaoFactory.getUsuarioDao().update(usuario.getId(), usuario));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            MainFormController.Logger(ServerAPI.class, ExceptionUtils.getStackTrace(e), Level.WARNING);
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Elimina un usuario registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response deleteUsuario(Request request){
        Response response = new Response();
        Usuario usuario;
        
        try{
            usuario = (Usuario) request.getData();
            response.setData (DaoFactory.getUsuarioDao().delete(usuario.getId()));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    // ========================================================================
    //                              MESA
    // ========================================================================
    /**
     * Crea una nueva mesa en el sistema.
     * @param request
     * @return 
     */
    public static Response createMesa(Request request){
        Response response = new Response();
        Mesa mesas;
        
        try{
            mesas = (Mesa) request.getData();
            response.setData (DaoFactory.getMesaDao().insert(mesas));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera un o varias mesas registrados en el sistema.
     * @param request
     * @return 
     */
    public static Response readMesa(Request request){
        Response response = new Response();
        int id_mesa;
        
        try{
            id_mesa = (int) request.getData();
            response.setData (DaoFactory.getMesaDao().load(id_mesa));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera una lista con todos las mesas registradas en el sistema.
     * @param request
     * @return 
     */
    public static Response readMesas(Request request){
        Response response = new Response();
        
        try{
            response.setData (DaoFactory.getMesaDao().queryAll());
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Actualiza la información de una mesa
     * @param request
     * @return 
     */
    public static Response updateMesa(Request request){
        Response response = new Response();
        Mesa mesa;
        
        try{
            mesa = (Mesa) request.getData();
            response.setData(DaoFactory.getMesaDao().update(mesa.getId(), mesa));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            MainFormController.Logger(ServerAPI.class, ExceptionUtils.getStackTrace(e), Level.WARNING);
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Elimina una mesa registrada en el sistema.
     * @param request
     * @return 
     */
    public static Response deleteMesa(Request request){
        Response response = new Response();
        Mesa mesa;
        
        try{
            mesa = (Mesa) request.getData();
            response.setData (DaoFactory.getMesaDao().delete(mesa.getId()));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    // ========================================================================
    //                           PRODUCTO TIPO
    // ========================================================================
    /**
     * Crea un nuevo producto tipo en el sistema.
     * @param request
     * @return 
     */
    public static Response createProductoTipo(Request request){
        Response response = new Response();
        ProductoTipo producto_tipo;
        
        try{
            producto_tipo = (ProductoTipo) request.getData();
            response.setData (DaoFactory.getProductoTipoDao().insert(producto_tipo));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera un producto tipo registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response readProductoTipo(Request request){
        Response response = new Response();
        int id_producto_tipo;
        
        try{
            id_producto_tipo = (int) request.getData();
            response.setData (DaoFactory.getProductoTipoDao().load(id_producto_tipo));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera una lista con todos los producto tipos registrados en el sistema.
     * @param request
     * @return 
     */
    public static Response readProductoTipos(Request request){
        Response response = new Response();
        
        try{
            response.setData (DaoFactory.getProductoTipoDao().queryAll());
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Actualiza la información de un producto tipo
     * @param request
     * @return 
     */
    public static Response updateProductoTipo(Request request){
        Response response = new Response();
        ProductoTipo producto_tipo;
        
        try{
            producto_tipo = (ProductoTipo) request.getData();
            response.setData(DaoFactory.getProductoTipoDao().update(producto_tipo.getId(), producto_tipo));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            MainFormController.Logger(ServerAPI.class, ExceptionUtils.getStackTrace(e), Level.WARNING);
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Elimina un producto tipo registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response deleteProductoTipo(Request request){
        Response response = new Response();
        ProductoTipo producto_tipo;
        
        try{
            producto_tipo = (ProductoTipo) request.getData();
            response.setData (DaoFactory.getProductoTipoDao().delete(producto_tipo.getId()));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    // ========================================================================
    //                              PRODUCTO
    // ========================================================================
    /**
     * Crea un nuevo producto en el sistema.
     * @param request
     * @return 
     */
    public static Response createProducto(Request request){
        Response response = new Response();
        Producto producto;
        int image_server_port = 0;
        
        try{
            producto = (Producto) request.getData();
            
            response.setData (DaoFactory.getProductoDao().insert(producto));
            
            image_server_port = ServerApp.initServerInputFiles(producto);
            
            response.setError(false);
            response.setStatus(true);
            response.setMessage(String.valueOf(image_server_port));
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera un producto registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response readProducto(Request request){
        Response response = new Response();
        int id_producto;
        
        try{
            id_producto = (int) request.getData();
            response.setData (DaoFactory.getProductoDao().load(id_producto));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera una lista con todos los productos registrados en el sistema.
     * @param request
     * @return 
     */
    public static Response readProductos(Request request){
        Response response = new Response();
        
        try{
            response.setData (DaoFactory.getProductoDao().queryAll());
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera una lista de productos segun su tipo de producto
     * @param request 
     */
    public static Response readProductosByTipo(Request request){
        Response response = new Response();
        String id_producto_tipo = (String) request.getData();
        
        try{
            response.setData ( DaoFactory.getProductoDao().queryByTipo(Integer.parseInt(id_producto_tipo)));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Actualiza la información de un producto
     * @param request
     * @return 
     */
    public static Response updateProducto(Request request){
        Response response = new Response();
        Producto producto;
        int image_server_port = 0;
        
        try{
            producto = (Producto) request.getData();
            response.setData(DaoFactory.getProductoDao().update(producto.getId(), producto));
            
            image_server_port = ServerApp.initServerInputFiles(producto);
            
            response.setError(false);
            response.setStatus(true);
            response.setMessage(String.valueOf(image_server_port));
            
        }catch(Exception e){
            MainFormController.Logger(ServerAPI.class, ExceptionUtils.getStackTrace(e), Level.WARNING);
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Elimina un producto registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response deleteProducto(Request request){
        Response response = new Response();
        Producto producto;
        
        try{
            producto = (Producto) request.getData();
            response.setData (DaoFactory.getProductoDao().delete(producto.getId()));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    // ========================================================================
    //                              COMANDA
    // ========================================================================
    /**
     * Crea una nueva comanda en el sistema.
     * @param request
     * @return 
     */
    public static Response createComanda(Request request){
        Response response = new Response();
        Comanda comanda;
        
        try{
            comanda = (Comanda) request.getData();
            response.setData (DaoFactory.getComandaDao().insert(comanda));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Crea una nueva comanda a partir de una lista de detalle comanda
     * @param request
     * @return 
     */
    public static Response createComandaDetalleComanda(Request request){
        Response response = new Response();
        Comanda comanda;
        List<DetalleComanda> detalle_comanda;
        
        try{
            List data = (List) request.getData();
            comanda = (Comanda) data.get(0);
            detalle_comanda = (List<DetalleComanda>) data.get(1);
            
            if (comanda != null){
                if (detalle_comanda != null){

                    Comanda comanda_inserted = DaoFactory.getComandaDao().insert(comanda);
                    
                    if (comanda_inserted != null){
                        for (int i=0; i<detalle_comanda.size(); i++){
                          detalle_comanda.get(i).setComanda(comanda_inserted.getId());
                        }
                        
                        DaoFactory.getDetalleComandaDao().inserts(detalle_comanda);
                        
                        response.setStatus(true);
                        response.setError(false);
                        response.setMessage(ResponseMessage.SUCCESS_OPERATION);
                    }
                }    
            }
            
        }catch (Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera una comanda registrada en el sistema.
     * @param request
     * @return 
     */
    public static Response readComanda(Request request){
        Response response = new Response();
        int id_comanda;
        
        try{
            id_comanda = (int) request.getData();
            response.setData (DaoFactory.getComandaDao().load(id_comanda));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera una lista con todas las comandas registradas en el sistema.
     * @param request
     * @return 
     */
    public static Response readComandas(Request request){
        Response response = new Response();
        
        try{
            response.setData (DaoFactory.getComandaDao().queryAll());
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Actualiza la información de una comanda
     * @param request
     * @return 
     */
    public static Response updateComanda(Request request){
        Response response = new Response();
        Comanda comanda;
        
        try{
            comanda = (Comanda) request.getData();
            response.setData(DaoFactory.getComandaDao().update(comanda.getId(), comanda));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            MainFormController.Logger(ServerAPI.class, ExceptionUtils.getStackTrace(e), Level.WARNING);
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Elimina una comanda registrada en el sistema.
     * @param request
     * @return 
     */
    public static Response deleteComanda(Request request){
        Response response = new Response();
        Comanda comanda;
        
        try{
            comanda = (Comanda) request.getData();
            response.setData (DaoFactory.getComandaDao().delete(comanda.getId()));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    // ========================================================================
    //                              DETALLE COMANDA
    // ========================================================================
    /**
     * Crea una nuevo detalle comanda en el sistema.
     * @param request
     * @return 
     */
    public static Response createDetalleComanda(Request request){
        Response response = new Response();
        DetalleComanda detalle_comanda;
        
        try{
            detalle_comanda = (DetalleComanda) request.getData();
            response.setData (DaoFactory.getDetalleComandaDao().insert(detalle_comanda));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Crea una lista nueva de detalle comanda en el sistema.
     * @param request
     * @return 
     */
    public static Response createDetalleComandas(Request request){
        Response response = new Response();
        List<DetalleComanda> detalle_comandas;
        
        try{
            detalle_comandas = (List<DetalleComanda>) request.getData();
            if (detalle_comandas.size() > 0){
                for (int i=0; i<detalle_comandas.size(); i++){
                    DetalleComanda detalle_comanda = detalle_comandas.get(i);
                    DaoFactory.getDetalleComandaDao().insert(detalle_comanda);
                }
            }
            
            response.setData (detalle_comandas);
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * 
     * @param request 
     */
    public static Response createDetalleComandasWithComanda(Request request){
        Response response = new Response();
        List<DetalleComanda> detalle_comandas;
        
        try{
            detalle_comandas = (List<DetalleComanda>) request.getData();
            if (detalle_comandas.size() > 0){
                for (int i=0; i<detalle_comandas.size(); i++){
                    DetalleComanda detalle_comanda = detalle_comandas.get(i);
                    DaoFactory.getDetalleComandaDao().insert(detalle_comanda);
                }
            }
            
            response.setData (detalle_comandas);
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera un detalle comanda registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response readDetalleComanda(Request request){
        Response response = new Response();
        int id_detalle_comanda;
        
        try{
            id_detalle_comanda = (int) request.getData();
            response.setData (DaoFactory.getDetalleComandaDao().load(id_detalle_comanda));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera una lista con todas los detalle de comandas registradas en el sistema.
     * @param request
     * @return 
     */
    public static Response readDetalleComandas(Request request){
        Response response = new Response();
        
        try{
            response.setData (DaoFactory.getDetalleComandaDao().queryAll());
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Actualiza la información de un detalle comanda
     * @param request
     * @return 
     */
    public static Response updateDetalleComanda(Request request){
        Response response = new Response();
        DetalleComanda detalle_comanda;
        
        try{
            detalle_comanda = (DetalleComanda) request.getData();
            response.setData(DaoFactory.getDetalleComandaDao().update(detalle_comanda.getId(), detalle_comanda));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            MainFormController.Logger(ServerAPI.class, ExceptionUtils.getStackTrace(e), Level.WARNING);
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Actualiza la información de una lista de detalle comanda
     * @param request
     * @return 
     */
    public static Response updateDetalleComandas(Request request){
        Response response = new Response();
        List<DetalleComanda> detalle_comandas;
        
        try{
            detalle_comandas = (List<DetalleComanda>) request.getData();
            if (detalle_comandas != null){
             if (detalle_comandas.size() > 0){
                 
//                DetalleComanda dc = detalle_comandas.get(0);
//                DaoFactory.getDetalleComandaDao().deleteByComanda(dc.getComanda());
                
                for (int i=0; i<detalle_comandas.size(); i++){
                    
                    DetalleComanda detalle_comanda = detalle_comandas.get(i);
                    
                    DetalleComanda dc_exist = DaoFactory.getDetalleComandaDao().queryByIdProductoIdComanda(detalle_comanda.getProducto(), detalle_comanda.getComanda());
                    
                    if (dc_exist != null){
                        DaoFactory.getDetalleComandaDao().update(dc_exist.getId(), detalle_comanda);
                    }else{
                        detalle_comanda.setCreated(new Date());
                        detalle_comanda.setCreator(detalle_comanda.getUpdater());
                        DaoFactory.getDetalleComandaDao().insert(detalle_comanda);
                    }
                    
//                    DaoFactory.getDetalleComandaDao().insert(detalle_comanda);
//                    DetalleComanda load_detalle_comanda = DaoFactory.getDetalleComandaDao().queryByIdProductoIdComanda(detalle_comanda.getProducto(), detalle_comanda.getComanda());
//                    if (load_detalle_comanda != null){
//                        DaoFactory.getDetalleComandaDao().update(load_detalle_comanda.getId(), detalle_comanda);
//                    }else{
//                        DaoFactory.getDetalleComandaDao().insert(detalle_comanda);
//                    }
                    
                }
                
            }   
            }
            
            response.setData (detalle_comandas);
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Elimina un detalle comanda registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response deleteDetalleComanda(Request request){
        Response response = new Response();
        DetalleComanda detalle_comanda;
        
        try{
            detalle_comanda = (DetalleComanda) request.getData();
            response.setData (DaoFactory.getDetalleComandaDao().delete(detalle_comanda.getId()));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }

    // ========================================================================
    //                              FACTURA
    // ========================================================================
    /**
     * Recupera una factura registrada en el sistema.
     * @param request
     * @return 
     */
    public static Response readFactura(Request request){
        Response response = new Response();
        int id_factura;
        
        try{
            id_factura = (int) request.getData();
            response.setData (DaoFactory.getFacturaDao().load(id_factura));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
        
    /**
     * Crea un nueva factura en el sistema.
     * @param request
     * @return 
     */
    public static Response createFactura(Request request){
        Response response = new Response();
        Factura factura;
        
        try{
            factura = (Factura) request.getData();
            response.setData (DaoFactory.getFacturaDao().insert(factura));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
        /**
     * Actualiza la información de una factura
     * @param request
     * @return 
     */
    public static Response updateFactura(Request request){
        Response response = new Response();
        Factura factura;
        
        try{
            factura = (Factura) request.getData();
            response.setData(DaoFactory.getFacturaDao().update(factura.getId(), factura));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            MainFormController.Logger(ServerAPI.class, ExceptionUtils.getStackTrace(e), Level.WARNING);
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Elimina una factura registrada en el sistema.
     * @param request
     * @return 
     */
    public static Response deleteFactura(Request request){
        Response response = new Response();
        Factura factura;
        
        try{
            factura = (Factura) request.getData();
            response.setData (DaoFactory.getFacturaDao().delete(factura.getId()));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    // ========================================================================
    //                              MENU
    // ========================================================================
    /**
     * Crea un nuevo menu en el sistema.
     * @param request
     * @return 
     */
    public static Response createMenu(Request request){
        Response response = new Response();
        Menu menu;
        
        try{
            menu = (Menu) request.getData();
            response.setData (DaoFactory.getMenuDao().insert(menu));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera un menu registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response readMenu(Request request){
        Response response = new Response();
        int id_menu;
        
        try{
            id_menu = (int) request.getData();
            response.setData (DaoFactory.getMenuDao().load(id_menu));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera una lista con todos los menus registrados en el sistema.
     * @param request
     * @return 
     */
    public static Response readMenus(Request request){
        Response response = new Response();
        
        try{
            response.setData (DaoFactory.getMenuDao().queryAll());
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Actualiza la información de un menu
     * @param request
     * @return 
     */
    public static Response updateMenu(Request request){
        Response response = new Response();
        Menu menu;
        
        try{
            menu = (Menu) request.getData();
            response.setData(DaoFactory.getMenuDao().update(menu.getId(), menu));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            MainFormController.Logger(ServerAPI.class, ExceptionUtils.getStackTrace(e), Level.WARNING);
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Elimina un menu registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response deleteMenu(Request request){
        Response response = new Response();
        Menu menu;
        
        try{
            menu = (Menu) request.getData();
            response.setData (DaoFactory.getMenuDao().delete(menu.getId()));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    // ========================================================================
    //                              DETALLE MENU
    // ========================================================================
    /**
     * Crea una nuevo detalle menu en el sistema.
     * @param request
     * @return 
     */
    public static Response createDetalleMenu(Request request){
        Response response = new Response();
        DetalleMenu detalle_menu;
        
        try{
            detalle_menu = (DetalleMenu) request.getData();
            response.setData (DaoFactory.getDetalleMenuDao().insert(detalle_menu));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Crea una lista nueva de detalle menu en el sistema.
     * @param request
     * @return 
     */
    public static Response createDetalleMenus(Request request){
        Response response = new Response();
        List<DetalleMenu> detalle_menus;
        
        try{
            detalle_menus = (List<DetalleMenu>) request.getData();
            if (detalle_menus.size() > 0){
                for (int i=0; i<detalle_menus.size(); i++){
                    DetalleMenu detalle_menu = detalle_menus.get(i);
                    DaoFactory.getDetalleMenuDao().insert(detalle_menu);
                }
            }
            
            response.setData (detalle_menus);
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera un detalle menu registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response readDetalleMenu(Request request){
        Response response = new Response();
        int id_detalle_menu;
        
        try{
            id_detalle_menu = (int) request.getData();
            response.setData (DaoFactory.getDetalleMenuDao().load(id_detalle_menu));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Recupera una lista con todos los detalle de menus registradas en el sistema.
     * @param request
     * @return 
     */
    public static Response readDetalleMenus(Request request){
        Response response = new Response();
        
        try{
            response.setData (DaoFactory.getDetalleMenuDao().queryAll());
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Actualiza la información de un detalle menu
     * @param request
     * @return 
     */
    public static Response updateDetalleMenu(Request request){
        Response response = new Response();
        DetalleMenu detalle_menu;
        
        try{
            detalle_menu = (DetalleMenu) request.getData();
            response.setData(DaoFactory.getDetalleMenuDao().update(detalle_menu.getId(), detalle_menu));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            MainFormController.Logger(ServerAPI.class, ExceptionUtils.getStackTrace(e), Level.WARNING);
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Actualiza la información de una lista de detalle menu
     * @param request
     * @return 
     */
    public static Response updateDetalleMenus(Request request){
        Response response = new Response();
        List<DetalleMenu> detalle_menus;
        
        try{
            detalle_menus = (List<DetalleMenu>) request.getData();
            if (detalle_menus.size() > 0){
                for (int i=0; i<detalle_menus.size(); i++){
                    DetalleMenu detalle_menu = detalle_menus.get(i);
                    DetalleMenu load_detalle_menu = DaoFactory.getDetalleMenuDao().queryByIdProductoIdMenu(detalle_menu.getProducto(), detalle_menu.getMenu());
                    if (load_detalle_menu != null){
                        DaoFactory.getDetalleMenuDao().update(load_detalle_menu.getId(), detalle_menu);
                    }else{
                        DaoFactory.getDetalleMenuDao().insert(detalle_menu);
                    }
                }
            }
            
            response.setData (detalle_menus);
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setStatus(false);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    /**
     * Elimina un detalle menu registrado en el sistema.
     * @param request
     * @return 
     */
    public static Response deleteDetalleMenu(Request request){
        Response response = new Response();
        DetalleMenu detalle_menu;
        
        try{
            detalle_menu = (DetalleMenu) request.getData();
            response.setData (DaoFactory.getDetalleMenuDao().delete(detalle_menu.getId()));
            response.setError(false);
            response.setStatus(true);
            response.setMessage(ResponseMessage.SUCCESS_OPERATION);
            
        }catch(Exception e){
            response.setError(true);
            response.setError(true);
            response.setMessage(e.getMessage());
        }
        
        return response;
    }
    
    
}
