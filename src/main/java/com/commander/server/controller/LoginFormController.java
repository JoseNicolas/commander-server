package com.commander.server.controller;

import static com.commander.server.Main.TITLE;
import static com.commander.server.Main.VERSION;
import com.commander.server.Request;
import com.commander.server.Response;
import com.commander.server.client.Client;
import com.commander.server.dao.DaoFactory;
import com.commander.server.model.Usuario;
import com.commander.server.utils.Config;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * 2019/05/14
 * @author JosÃƒÆ’Ã‚Â© Francisco NicolÃƒÆ’Ã‚Â¡s Bautista
 * @version 1.0
 */
public class LoginFormController implements Initializable, EventHandler {

    @FXML Button bt_acceder;
    @FXML TextField tf_usuario;
    @FXML PasswordField pf_password;
    Stage primary_stage;
    Alert alert;
    Client client_tcp;
    
    public LoginFormController(Stage stage) throws Exception{
        primary_stage = stage;
        client_tcp = new Client();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bt_acceder.setOnAction(this);
    }    

    @Override
    public void handle(Event event) {
        Object target = event.getTarget();
        
        if (target.getClass().equals(Button.class)){
            String usuario = tf_usuario.getText();
            String password = pf_password.getText();
            
            if (usuario != "" && password != ""){
                
                validarCredenciales(usuario, password);
                
            }else{
                alert.showAndWait();
            }
        }
        if (target.getClass().equals(Stage.class)){
            Platform.exit();
            System.exit(0);
        }
    }
    
    private void validarCredenciales(String usuario, String password){
        try{
            Usuario user = DaoFactory.getUsuarioDao().queryByUsuario(usuario);
            if (user != null){
                if (user.getPassword().equals(password)){
                    
                    initMainForm(primary_stage, user);
                    
                }else{
                    alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Credenciales Incorrectas");
                    alert.setHeaderText(null);
                    alert.setContentText("Las credenciales introducidas no son correctas");
                    alert.showAndWait();    
                }
            }else{
                alert = new Alert(AlertType.ERROR);
                alert.setTitle("Credenciales Incorrectas");
                alert.setHeaderText(null);
                alert.setContentText("Las credenciales introducidas no son correctas");
                alert.showAndWait();
            }
        }catch (Exception e){
            alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Inesperado");
            alert.setHeaderText(null);
            alert.setContentText("Se produjo un error en la validación. Póngase en contacto con su soporte ténico");
            alert.showAndWait();  
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    public void initMainForm(Stage stage, Usuario usuario_sesion){
        try{
//            this.primary_stage = stage;
//            Usuario usuario_sesion = DaoFactory.getUsuarioDao().load(1);
            // Layout
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLMainForm.fxml"));
            MainFormController controller = new MainFormController(this.primary_stage, usuario_sesion);
            loader.setController(controller);
            // Show scene
            Scene scene = new Scene((Parent) loader.load());
            scene.getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");
            this.primary_stage.setScene(scene);
            this.primary_stage.setTitle(TITLE + " " + VERSION);
            this.primary_stage.getIcons().add(new Image(Config.getProperty("icon")));
//            this.primaryStage.setResizable(false);
            this.primary_stage.sizeToScene();
            this.primary_stage.setResizable(true);
            this.primary_stage.setOnCloseRequest(this);
            this.primary_stage.setOnHidden(this);
            this.primary_stage.setX(300);
            this.primary_stage.setY(50);
            this.primary_stage.show();
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
}
