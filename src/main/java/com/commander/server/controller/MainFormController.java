package com.commander.server.controller;

import com.commander.server.utils.WorkIndicatorDialog;
import com.commander.server.Request;
import com.commander.server.Response;
import com.commander.server.utils.Config;
import com.commander.server.ServerApp;
import com.commander.server.client.Client;
import com.commander.server.dao.DaoFactory;
import com.commander.server.model.*;
import com.commander.server.utils.Validator;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.sun.javafx.scene.control.skin.LabeledText;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.exception.ExceptionUtils;

/*
 * 2019/04/27
 * @author JosÃ© Francisco NicolÃ¡s Bautista
 * @version 1.0
 */
public class MainFormController extends Listener implements Initializable, EventHandler{
    
    @FXML private MenuItem nuevo_producto;
    @FXML private MenuItem nuevo_usuario;
    @FXML private MenuItem nuevo_mesa;
    @FXML private MenuItem nuevo_menu;
    @FXML private MenuItem nuevo_producto_tipo;
    @FXML private MenuItem reportes_facturacion;
    @FXML private MenuItem salir;
    @FXML private MenuItem editar_producto;
    @FXML private MenuItem editar_usuario;
    @FXML private MenuItem editar_mesa;
    @FXML private MenuItem editar_menu;
    @FXML private MenuItem editar_producto_tipo;
    @FXML private MenuItem ayuda_acercade;
    @FXML private TitledPane tp_buscar_comandas_todas;
    @FXML private TitledPane tp_buscar_comandas_mesa;
    @FXML private TitledPane tp_buscar_comandas_camarero;
    @FXML private Accordion ac_buscar_comandas;
    @FXML private ListView lv_buscar_comandas_mesa;
    @FXML private ListView lv_buscar_comandas_camarero;
    @FXML private ListView lv_buscar_comandas_todas;
    @FXML private ListView lv_buscar_comandas_estado;
    @FXML private TabPane tp_comandas;
    @FXML private Tab tab_nuevo_editar_comanda;
    @FXML private Tab tab_lista_comandas;
    @FXML private ChoiceBox cb_buscar_comandas_mesa;
    @FXML private ChoiceBox cb_buscar_comandas_camarero;
    @FXML private ChoiceBox cb_buscar_comandas_estado;
    @FXML private Text t_text_info;
    @FXML private TextField tf_usuario_sesion;
    @FXML private TilePane tp_productos;
    @FXML private TilePane tp_tipo_productos;
    @FXML private Button bt_aceptar;
    @FXML private Button bt_cancelar;
    @FXML private Button bt_eliminar;
    @FXML private Button bt_editar;
    @FXML private Button bt_reload;
    @FXML private Button bt_reset_form;
    @FXML private Button bt_facturar;
    @FXML private ChoiceBox cb_camarero;
    @FXML private ChoiceBox cb_mesa;
    @FXML private ChoiceBox cb_estado;
    @FXML private Label lb_producto_nombre;
    @FXML private TextArea ta_log;
    @FXML private TableView<String[]> table_detalle_comanda;
    @FXML private TableView<Comanda> table_comandas;
    TableColumn<Comanda, Integer> tc_id_comanda;
    TableColumn<Comanda, Integer> tc_mesa_comanda;
    TableColumn<Comanda, Integer> tc_camarero_comanda;
    TableColumn<Comanda, Integer> tc_factura_comanda;
    TableColumn<Comanda, String> tc_estado_comanda;
    TableColumn<Comanda, Date> tc_comanda_created;
    TableColumn<Comanda, Integer> tc_comanda_creator;
    TableColumn<Comanda, Date> tc_comanda_updated;
    TableColumn<Comanda, Integer> tc_comanda_updator;
    
    TableColumn<String[], String> tc_id_producto;
    TableColumn<String[], String> tc_codigo_producto;
    TableColumn<String[], String> tc_producto;
    TableColumn<String[], String> tc_cantidad;
    
    private ServerApp server;    
    private static Usuario usuario_sesion;
    private Stage primary_stage;
    private Alert alert;
    private Client client_tcp;
    private List<Usuario> camareros;
    private List<Mesa> mesas;
    private List<ProductoTipo> producto_tipos;
    private List<Producto> productos;
    private List<Comanda> comandas;
    private Comanda comanda_update;
    private Producto producto_selected;
    private ImageView iv_producto_selected;
    
    public MainFormController(Stage stage, Usuario usuario_sesion) throws Exception{
        this.primary_stage = stage;
        this.usuario_sesion = usuario_sesion;
        this.server = new ServerApp();
        this.server.server.addListener(this);
        this.client_tcp = new Client();
        
        this.camareros = DaoFactory.getUsuarioDao().queryAll();
        this.mesas = DaoFactory.getMesaDao().queryAll();
        this.producto_tipos = DaoFactory.getProductoTipoDao().queryAll();
        this.productos = DaoFactory.getProductoDao().queryAll();
        this.comandas = DaoFactory.getComandaDao().queryAll();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tf_usuario_sesion.setText("Bienvenido: " + usuario_sesion.getNombre());
        nuevo_usuario.setOnAction(this);
        nuevo_producto.setOnAction(this);
        nuevo_mesa.setOnAction(this);
        nuevo_menu.setOnAction(this);
        nuevo_producto_tipo.setOnAction(this);
        reportes_facturacion.setOnAction(this);
        editar_usuario.setOnAction(this);
        editar_producto.setOnAction(this);
        editar_mesa.setOnAction(this);
        editar_menu.setOnAction(this);
        editar_producto_tipo.setOnAction(this);
        ayuda_acercade.setOnAction(this);
        salir.setOnAction(this);
        
        bt_aceptar.setOnAction(this);
        bt_cancelar.setOnAction(this);
        bt_editar.setOnAction(this);
        bt_eliminar.setOnAction(this);
        bt_reload.setOnAction(this);
        bt_reset_form.setOnAction(this);
        bt_facturar.setOnAction(this);
        
        tp_buscar_comandas_mesa.setOnMouseClicked(this);
        tp_buscar_comandas_camarero.setOnMouseClicked(this);
        tp_buscar_comandas_todas.setOnMouseClicked(this);
//        ac_buscar_comandas.setExpandedPane(tp_buscar_comandas_mesa);
        cb_buscar_comandas_mesa.setOnAction(this);
        cb_buscar_comandas_camarero.setOnAction(this);
        cb_buscar_comandas_mesa.setItems(FXCollections.observableList(mesas));
        cb_buscar_comandas_camarero.setItems(FXCollections.observableList(camareros));
        cb_buscar_comandas_estado.setOnAction(this);
        cb_buscar_comandas_estado.getItems().addAll("pendiente", "preparada", "servida", "facturada");
        
        cb_camarero.setItems(FXCollections.observableList(camareros));
        cb_mesa.setItems(FXCollections.observableList(mesas));
        cb_estado.getItems().addAll("pendiente", "preparada", "servida", "facturada");
        
        lv_buscar_comandas_todas.setItems(FXCollections.observableArrayList(comandas));
        lv_buscar_comandas_todas.setOnMouseClicked(this);
//        lv_buscar_comandas_estado.setItems(FXCollections.observableArrayList(comandas.stream().filter(c -> c.getFactura() > 0).collect(Collectors.toList())));
        lv_buscar_comandas_camarero.setOnMouseClicked(this);
        lv_buscar_comandas_mesa.setOnMouseClicked(this);
        lv_buscar_comandas_estado.setOnMouseClicked(this);
        
        fillProductoTipoBar(producto_tipos);
        
        tc_id_comanda = new TableColumn("ID");
        tc_mesa_comanda = new TableColumn("MESA");
        tc_camarero_comanda = new TableColumn("CAMARERO");
        tc_factura_comanda = new TableColumn("FACTURA");
        tc_estado_comanda = new TableColumn("ESTADO");
        tc_comanda_created = new TableColumn("CREATED");
        tc_comanda_creator = new TableColumn("CREATOR");
        tc_comanda_updated = new TableColumn("UPDATED");
        tc_comanda_updator = new TableColumn("UPDATER");
        
        tc_id_comanda.setCellValueFactory(new PropertyValueFactory<Comanda, Integer>("id"));
        tc_mesa_comanda.setCellValueFactory(new PropertyValueFactory<Comanda, Integer>("mesa"));
        tc_camarero_comanda.setCellValueFactory(new PropertyValueFactory<Comanda, Integer>("camarero"));
        tc_factura_comanda.setCellValueFactory(new PropertyValueFactory<Comanda, Integer>("factura"));
        tc_estado_comanda.setCellValueFactory(new PropertyValueFactory<Comanda, String>("estado"));
        tc_comanda_created.setCellValueFactory(new PropertyValueFactory<Comanda, Date>("created"));
        tc_comanda_creator.setCellValueFactory(new PropertyValueFactory<Comanda, Integer>("creator"));
        tc_comanda_updated.setCellValueFactory(new PropertyValueFactory<Comanda, Date>("updated"));
        tc_comanda_updator.setCellValueFactory(new PropertyValueFactory<Comanda, Integer>("updater"));
        tc_comanda_creator.setVisible(false);// Columna oculta por defecto
        tc_comanda_updator.setVisible(false);// Columna oculta por defecto
        tc_comanda_updator.setVisible(false);// Columna oculta por defecto
        
        tc_id_producto = new TableColumn("ID");
        tc_codigo_producto = new TableColumn("CODIGO");
        tc_producto = new TableColumn("PRODUCTO");
        tc_cantidad = new TableColumn("CATIDAD");
        
        tc_id_producto.setCellValueFactory((p)->{
                String[] x = p.getValue();
                return new SimpleStringProperty(x != null && x.length>0 ? x [0] : "<no name>");
        });
        tc_codigo_producto.setCellValueFactory((p)->{
                String[] x = p.getValue();
                return new SimpleStringProperty(x != null && x.length>0 ? x [1] : "<no name>");
        });
        tc_producto.setCellValueFactory((p)->{
                String[] x = p.getValue();
                return new SimpleStringProperty(x != null && x.length>0 ? x [2] : "<no name>");
        });
        tc_cantidad.setCellValueFactory((p)->{
                String[] x = p.getValue();
                return new SimpleStringProperty(x != null && x.length>1 ? x [3] : "<no value>");
        });
        
        table_detalle_comanda.getColumns().addAll(tc_id_producto, tc_codigo_producto, tc_producto, tc_cantidad);
        table_detalle_comanda.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table_detalle_comanda.setRowFactory( tv -> {
            TableRow<String[]> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    subtractProductoFromComanda();
                }
            });
            return row ;
        });
        
        table_comandas.getColumns().addAll(tc_id_comanda, tc_mesa_comanda,
                tc_camarero_comanda, tc_estado_comanda, tc_factura_comanda, tc_comanda_created, 
                tc_comanda_creator, tc_comanda_updated, tc_comanda_updator);
        table_comandas.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        
        reloadData();
    }    
    
    /**
     * Implement handle event from Interface EventHandler
     * @param event 
     */
    @Override
    public void handle(Event event) {
        try{
            Class target = event.getTarget().getClass();
        
            if (target.equals(MenuItem.class)){
                handleMenuBarOptions((ActionEvent)event);
            }
            if(target.equals(TitledPane.class)){
                handleTitledPaneOptions((ActionEvent)event);
            }
            if(target.equals(ChoiceBox.class)){
                handleChoiceBoxOptions((ActionEvent)event);
            }
            if(target.equals(Button.class)){
                handleButtonOptions((ActionEvent)event);
            }
            if(target.equals(LabeledText.class)){
                handleListViewOptions((MouseEvent)event);
            }
            if (target.equals(Stage.class)){
                this.reloadData();
            }
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Funcion manejadora de eventos para los items MenuBar
     * @param event 
     */
    private void handleMenuBarOptions(ActionEvent event){
        MenuItem menuItem = null;
        try{
            menuItem = (MenuItem) event.getSource();
            
            switch (menuItem.getId()){
                case "nuevo_usuario":
                    initUsuariosForm(false);
                    break;
                case "nuevo_producto":
                    initProductosForm(false);
                    break;
                case "nuevo_mesa":
                    initMesasForm(false);
                    break;
                case "nuevo_menu":
                    initMenusForm(false);
                    break;
                case "nuevo_producto_tipo":
                    initProductoTiposForm(false);
                    break;
                case "editar_usuario":
                    initUsuariosForm(true);
                    break;
                case "editar_producto":
                    initProductosForm(true);
                    break;
                case "editar_mesa":
                    initMesasForm(true);
                    break;
                case "editar_menu":
                    initMenusForm(true);
                    break;
                case "editar_producto_tipo":
                    initProductoTiposForm(true);
                    break;
                case "reportes_facturacion":
                    initFacturasForm(true, null);
                    break;
                case "ayuda_acercade":
                    initAcercaDeForm();
                    break;
                case "salir":
                    closeApplication();
                    break;
            }
        }catch(Exception ex){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(ex), Level.SEVERE);
        }
    }

    /**
     * Funcion manejadora de eventos para los items TitledPane
     * @param event 
     */
    private void handleTitledPaneOptions(ActionEvent event){
        TitledPane titled_pane = null;
        try{
            titled_pane = (TitledPane) event.getSource();
            switch (titled_pane.getId()){
                case "tp_buscar_comandas_mesa":
                    
                    break;
                case "tp_buscar_comandas_camarero":
                    
                    break;
                case "tp_buscar_comandas_todas":
                    
                    break;
            }
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Funcion manejadora de eventos para los items ChoiceBox
     * @param event 
     */
    private void handleChoiceBoxOptions(ActionEvent event){
        ChoiceBox choice_box = null;
        try{
            choice_box = (ChoiceBox) event.getSource();
            switch (choice_box.getId()){
                case "cb_buscar_comandas_mesa":
                    Mesa selected_mesa = (Mesa) cb_buscar_comandas_mesa.getSelectionModel().getSelectedItem();
                    filtrarComandasMesa(selected_mesa);
                    break;
                case "cb_buscar_comandas_camarero":
                    Usuario selected_camarero = (Usuario) cb_buscar_comandas_camarero.getSelectionModel().getSelectedItem();
                    filtrarComandasCamarero(selected_camarero);
                    break;
                case "cb_buscar_comandas_estado":
                    String selected_estado = (String) cb_buscar_comandas_estado.getSelectionModel().getSelectedItem();
                    filtrarComandasEstado(selected_estado);
                    break;
            }
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Funcion manejadora de eventos para los items Button
     * @param event 
     */
    private void handleButtonOptions (ActionEvent event){
        Button button = null;
        try{
            button = (Button) event.getSource();
            switch (button.getId()){
                case "bt_aceptar":
                    if (validarComandasForm()){
                        if (this.comanda_update == null){
                            createComanda();
                        }else{
                            updateComanda(this.comanda_update);
                        }
                    }
                    break;
                case "bt_reset_form":
                    resetComandaForm();
                    reloadData();
                    break;
                case "bt_editar":
                    Comanda comanda = (Comanda) table_comandas.getSelectionModel().getSelectedItem();
                    if (comanda != null){
                        this.comanda_update = comanda;
                        setComandaToForm(comanda);
                        tp_comandas.getSelectionModel().select(tab_nuevo_editar_comanda);
                    }
                    break;
                case "bt_facturar":
                    if (this.comanda_update != null){
                       if (this.comanda_update.getFactura() == 0){
                            facturarComanda(this.comanda_update);
                       }else{
                            alert = new Alert(AlertType.ERROR);
                            alert.setTitle("ERROR");
                            alert.setHeaderText(null);
                            alert.setContentText("Comanda " + this.comanda_update.toString() + " YA FACTURADA!");
                            alert.showAndWait();
                       }
                    }else{
                        alert = new Alert(AlertType.ERROR);
                        alert.setTitle("ERROR");
                        alert.setHeaderText(null);
                        alert.setContentText("Seleccione una comanda para facturar");
                        alert.showAndWait();
                    }
                    break;
                case "bt_reload":
                    reloadData();
                    break;
            }
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Funcion manejadora de ventos para los items ListView
     * @param event 
     */
    private void handleListViewOptions(MouseEvent event){
        ListView list_view = null;
        try{
            try{
                list_view = (ListView) event.getSource();    
            }catch (Exception e){}
            
            switch (list_view.getId()){
                case "lv_buscar_comandas_todas":
                case "lv_buscar_comandas_camarero":
                case "lv_buscar_comandas_mesa":
                case "lv_buscar_comandas_estado":
                    Comanda selected_comanda = (Comanda) list_view.getSelectionModel().getSelectedItem();
                    this.comanda_update = selected_comanda;
                    setComandaToForm(selected_comanda);
                    break;
            }
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }

    /**
     * Retorna el usuario que ha iniciado sesion en la aplicacion
     * @return Usuario usuario_sesion
     */
    public static Usuario getUserSesion(){
        return usuario_sesion;
    }
    
    /**
     * Valida los campos del formulario Menus
     */
    private boolean validarComandasForm(){
        boolean is_valid = true;
        
        Usuario camarero = (Usuario) cb_camarero.getSelectionModel().getSelectedItem();
        Mesa mesa = (Mesa) cb_mesa.getSelectionModel().getSelectedItem();
        String estado = (String) cb_estado.getSelectionModel().getSelectedItem();
        ObservableList detalle_comanda = table_detalle_comanda.getItems();
        
        if (Validator.isNull(camarero)){
            t_text_info.setText("Camarero* es un campo obligatorio. Por favor, seleccione un camarero");
            cb_camarero.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            cb_camarero.setStyle("-fx-border-color: green;");
        }
        
        if (Validator.isNull(mesa)){
            t_text_info.setText("Mesa* es un campo obligatorio. Por favor, selecciona una mesa");
            cb_mesa.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            cb_mesa.setStyle("-fx-border-color: green;");
        }
        
        if (Validator.isNull(estado)){
            t_text_info.setText("Estado* es un campo obligatorio. Por favor, selecciona un estado para la comanda");
            cb_estado.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            cb_estado.setStyle("-fx-border-color: green;");
        }
        
        if (Validator.isNotNull(detalle_comanda)){
            if (detalle_comanda.size() > 0){
                table_detalle_comanda.setStyle("-fx-border-color: green;");
            }else{
                t_text_info.setText("Por favor, aÃ±ada al menos un producto a la comanda.");
                table_detalle_comanda.setStyle("-fx-border-color: red;");
                is_valid = false;
            }
        }else{
            t_text_info.setText("Por favor, aÃ±ada al menos un producto a la comanda.");
            table_detalle_comanda.setStyle("-fx-border-color: red;");
            is_valid = false;
        }
        
        t_text_info.setFill(Color.RED);
        
        if (is_valid){
            t_text_info.setText("");
            cb_mesa.setStyle(null);
            cb_camarero.setStyle(null);
            table_detalle_comanda.setStyle(null);
        }
          
        return is_valid;
    }
    
    /**
     * Obtiene la comanda definida en el formulario de comandas
     * @return 
     */
    private Comanda getComandaFromForm(){
        Comanda comanda;
        if (this.comanda_update != null){
            comanda = this.comanda_update;
        }else{
            comanda = new Comanda();
        }
        Mesa mesa = (Mesa) cb_mesa.getSelectionModel().getSelectedItem();
        Usuario camarero = (Usuario) cb_camarero.getSelectionModel().getSelectedItem();
        String estado = (String) cb_estado.getSelectionModel().getSelectedItem();
        
        comanda.setMesa(mesa.getId());
        comanda.setCamarero(camarero.getId());
        comanda.setEstado(estado);
        
        return comanda;
    }
    
    /**
     * Obtiene un listado de DetalleComanda desde el formulario.
     * @return 
     */
    private List<DetalleComanda> getDetallesComandaFromForm(){
        List<DetalleComanda> detalles_comanda = null;
        
        if (table_detalle_comanda.getItems().size() > 0){
            detalles_comanda = new ArrayList();
            
            for (int i=0; i < table_detalle_comanda.getItems().size(); i++){

                String row_producto_id = tc_id_producto.getCellData(i);
                String row_producto_codigo = tc_codigo_producto.getCellData(i);
                String row_producto_cantidad = tc_cantidad.getCellData(i);
                
                DetalleComanda dc = new DetalleComanda();
                if (this.comanda_update != null){
                    dc.setComanda(this.comanda_update.getId());
                }else{
                    dc.setComanda(0);
                }
                dc.setProducto(Integer.parseInt(row_producto_id));
                dc.setCantidad(Float.parseFloat(row_producto_cantidad));

                detalles_comanda.add(dc);
            }    
        }
        
        return detalles_comanda;
    }
    
    /**
     * LLena el formulario de comanda con las propiedades de la comanda especificada
     * @param comanda 
     */
    private void setComandaToForm(Comanda comanda){
        List<DetalleComanda> detalles_comanda;
        
        try{
            Mesa mesa = mesas.stream().filter(m -> m.getId() == comanda.getMesa()).findAny().get();
            Usuario camarero = camareros.stream().filter(c -> c.getId() == comanda.getCamarero()).findAny().get();
            String estado = comanda.getEstado();
            
            if (mesa != null){
                cb_mesa.getSelectionModel().select(mesa);
            }
            
            if (camarero != null){
                cb_camarero.getSelectionModel().select(camarero);
            }
            
            if (estado != null){
                cb_estado.getSelectionModel().select(estado);
            }
            
            detalles_comanda = DaoFactory.getDetalleComandaDao().queryByComanda(comanda);
            setDetallesComandaToForm(detalles_comanda);
            
        }catch (Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * LLena la tabla detalle de comanda del formulario de comandas con el detalle comanda especificado
     * @param detalles_comanda 
     */
    private void setDetallesComandaToForm(List<DetalleComanda> detalles_comanda){
        table_detalle_comanda.getItems().clear();
        if (detalles_comanda != null){
            if (detalles_comanda.size() > 0){
                for (int i=0; i<detalles_comanda.size(); i++){
                    DetalleComanda dc = detalles_comanda.get(i);
                    Producto producto = productos.stream().filter(p -> p.getId() == dc.getProducto()).findAny().get();
                    if (producto != null){
                        addProductoToComanda(producto, dc.getCantidad());
                    }
                }
            }
        }
    }
    
    /**
     * Rellena el box de tipo de producto con los tipos de producto existentes en la bd
     * @param producto_tipos 
     */
    private void fillProductoTipoBar(List<ProductoTipo> producto_tipos){
        if (producto_tipos != null){
            if (producto_tipos.size() > 0){
                tp_tipo_productos.getChildren().clear();
                
                ProductoTipo pt;
                for (int i=0; i<producto_tipos.size(); i++){
                    pt = producto_tipos.get(i);
                    String pt_nombre = "("+pt.getId()+") "+pt.getNombre();
                    
                    Button b = new Button();
                    b.setId("tp-" + pt.getId());
                    b.setText(pt_nombre);
                    b.setPrefSize(150, 85);
                    b.setMinSize(150, 85);
                    b.setMaxSize(150, 85);
                    b.setTextAlignment(TextAlignment.CENTER);
                    b.setWrapText(true);
                    
                    b.setOnAction(new EventHandler<ActionEvent>() {

                        @Override
                        public void handle(ActionEvent event) {
                            ProductoTipo pt;
                            List<Producto> producto_filtered = new ArrayList();
                            Button button = (Button) event.getSource();
                            String button_id = b.getId();
                            
                            try{
                                lb_producto_nombre.setText("");
                                producto_selected = null;
                                iv_producto_selected = null;
                                String id_producto_tipo = button_id.split("-")[1];
                                if (id_producto_tipo != null){
                                    pt = DaoFactory.getProductoTipoDao().load(Integer.parseInt(id_producto_tipo));
                                    
                                    if (pt != null){
                                        producto_filtered = productos.stream()
                                                .filter(p -> p.getTipo() == pt.getId())
                                                .collect(Collectors.toList());
                                        
                                        fillProductoBar(producto_filtered);
                                    }
                                }   
                            }catch(Exception e){
                                MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                            }
                        }
                    });

                    tp_tipo_productos.getChildren().add(b);
                    tp_tipo_productos.setMaxWidth(Region.USE_PREF_SIZE);
                }
            }
        }
    }
    
    /**
     * Rellena el box de productos con los productos en funcion de tipo de producto seleccionado
     * @param productos 
     */
    private void fillProductoBar(List<Producto> productos){
        File file;
        BufferedImage bImage;
        Image image;
        DropShadow ds = new DropShadow( 14, Color.ORANGE );
        
        tp_productos.getChildren().clear();
        
        if (productos != null){
            if (productos.size() > 0){
                
                for (int i=0; i<productos.size(); i++){
                    final Producto producto = productos.get(i);
                    
                    try{
                        ImageView iv = new ImageView();
                        iv.setFitHeight(90);
                        iv.setFitWidth(90);


                        file = new File(Config.getProperty("directory_images") + "/" + producto.getImagen());
                        bImage = ImageIO.read(file);
                        image = SwingFXUtils.toFXImage(bImage, null);
                        iv.setImage(image);

                        iv.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent mouseEvent) {
                                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                                    if (mouseEvent.getClickCount() == 1){
                                        iv.requestFocus();
                                    }
                                    if(mouseEvent.getClickCount() == 2){
                                        addProductoToComanda(producto_selected, 0);
                                    }
                                }
                            }
                        });
                        
                        iv.focusedProperty().addListener(( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) ->
                        {
                            if ( newValue )
                            {
                                if (iv_producto_selected != null){
                                    iv_producto_selected.setEffect(null);
                                }
                                
                                String producto_nombre = "("+producto.getId()+") " + producto.getNombre() + " ["+producto.getPrecio_unit()+" €/U]";
                                lb_producto_nombre.setText(producto_nombre);
                                iv_producto_selected = iv;
                                iv_producto_selected.setEffect( ds );
                                this.producto_selected = producto;
                            }
                        });

                        tp_productos.getChildren().add(iv);
                        tp_productos.setMaxWidth(Region.USE_PREF_SIZE);
                    }catch(Exception e){
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        }
    }
    
    /**
     * AÃ±ade un nuevo producto al detalle de comanda
     */
    private void addProductoToComanda(Producto producto_selected, float cantidad_producto){
        if (producto_selected != null){
            try{
             
                String cantidad = cantidad_producto > 0 ? String.valueOf(cantidad_producto) : "1.0";
                String [][] row = new String[1][4];
                row [0] = new String [] { String.valueOf(producto_selected.getId()), producto_selected.getCodigo(), producto_selected.getNombre(), cantidad };

                if (table_detalle_comanda.getItems().size() > 0){
                    boolean product_exists = false;
                    for (int i=0; i < table_detalle_comanda.getItems().size(); i++){

                        String row_producto_id = tc_id_producto.getCellData(i);
                        String row_producto_selected_id = String.valueOf(producto_selected.getId());
                        if (row_producto_selected_id.equals(row_producto_id)){
                            product_exists = true;
                            String row_producto_cantidad = tc_cantidad.getCellData(i);
                            String row_producto_codigo = tc_codigo_producto.getCellData(i);
                            String row_producto_producto = tc_producto.getCellData(i);

                           String nueva_cantidad = String.valueOf(Float.parseFloat(cantidad) + Float.parseFloat(row_producto_cantidad));
                           table_detalle_comanda.getItems().set(i, new String[] {row_producto_id, row_producto_codigo, row_producto_producto, nueva_cantidad});
                        }
                    }
                    if (!product_exists){
                        table_detalle_comanda.getItems().addAll(Arrays.asList(row));    
                    }
                }else{
                    table_detalle_comanda.getItems().addAll(Arrays.asList(row));
                }
                
            }catch (Exception e){
                MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
            }
        }
    }
    
    /**
     * Reduce la cantidad del producto seleccionada en la comanda hasta poder eliminarlo por completo
     */
    private void subtractProductoFromComanda(){
        try{
            
            int row_producto_selected_index = table_detalle_comanda.getSelectionModel().getSelectedIndex();
            String[] row_producto_selected = table_detalle_comanda.getSelectionModel().getSelectedItem();
            if (row_producto_selected != null){

                float row_producto_value = Float.parseFloat(row_producto_selected [3] );
                if (row_producto_value > 1){
                    row_producto_value--;
                    row_producto_selected [3] = String.valueOf( row_producto_value );
                    table_detalle_comanda.getItems().set(row_producto_selected_index, row_producto_selected);
                }else{
                    table_detalle_comanda.getItems().remove(row_producto_selected_index);
                }
            }
            
        }catch (Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Reinicializa por completo el formulario de comandas
     */
    public void resetComandaForm(){
        cb_camarero.setValue(null);
        cb_mesa.setValue(null);
        cb_estado.setValue(null);
        table_detalle_comanda.getItems().clear();
        t_text_info.setText("");
        
        this.comanda_update = null;
        
        cb_camarero.setStyle(null);
        cb_mesa.setStyle(null);
        cb_estado.setStyle(null);
        table_detalle_comanda.setStyle(null);
    }
    
    /**
     * Recarga la informacion basica del formulario. 
     */
    public void reloadData(){
        try{
            camareros = DaoFactory.getUsuarioDao().queryAll();
            mesas = DaoFactory.getMesaDao().queryAll();
            producto_tipos = DaoFactory.getProductoTipoDao().queryAll();
            productos = DaoFactory.getProductoDao().queryAll();   
            comandas = DaoFactory.getComandaDao().queryAll();
            
            cb_mesa.setItems(FXCollections.observableArrayList(mesas));
            cb_camarero.setItems(FXCollections.observableArrayList(camareros));
            
            cb_buscar_comandas_mesa.setItems(FXCollections.observableArrayList(mesas));
            cb_buscar_comandas_camarero.setItems(FXCollections.observableArrayList(camareros));
            
            lv_buscar_comandas_todas.setItems(FXCollections.observableArrayList(comandas));
            Mesa mesa_filter = (Mesa) cb_buscar_comandas_mesa.getSelectionModel().getSelectedItem();
            filtrarComandasMesa(mesa_filter);
            String estado_filter = (String) cb_buscar_comandas_estado.getSelectionModel().getSelectedItem();
            filtrarComandasEstado(estado_filter);
            Usuario camarero_filter = (Usuario) cb_buscar_comandas_camarero.getSelectionModel().getSelectedItem();
            filtrarComandasCamarero(camarero_filter);
            
            table_comandas.setItems(FXCollections.observableArrayList(comandas));
            
            fillProductoTipoBar(producto_tipos);
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Filtra el listado de busqueda de comanda por mesa
     * @param mesa 
     */
    private void filtrarComandasMesa(Mesa mesa){
        List<Comanda> comandas_mesa;
        if (mesa != null){
            comandas_mesa = comandas.stream().filter(c -> c.getMesa() == mesa.getId())
                    .collect(Collectors.toList());
        }else{
            comandas_mesa = new ArrayList();
        }
        
        lv_buscar_comandas_mesa.setItems(FXCollections.observableArrayList(comandas_mesa));
    }
    
    /**
     * Filtra el listado de busqueda de comanda por camarero
     * @param camarero 
     */
    private void filtrarComandasCamarero(Usuario camarero){
        List<Comanda> comandas_camarero;
        if (camarero != null){
            comandas_camarero = comandas.stream().filter(c -> c.getCamarero() == camarero.getId())
                    .collect(Collectors.toList());
        }else{
            comandas_camarero = new ArrayList();
        }
        
        lv_buscar_comandas_camarero.setItems(FXCollections.observableArrayList(comandas_camarero));
    }
    
    /**
     * Filtra el listado de busqueda de comanda por su estado
     * @param camarero 
     */
    private void filtrarComandasEstado(String estado){
        List<Comanda> comandas_estado;
        if (estado != null){
            comandas_estado = comandas.stream().filter(c -> c.getEstado().equals(estado))
                    .collect(Collectors.toList());
        }else{
            comandas_estado = new ArrayList();
        }
        
        lv_buscar_comandas_estado.setItems(FXCollections.observableArrayList(comandas_estado));
    }
    
    
    
    /**
     * Define una comanda como facturada
     * @param comanda 
     */
    private void facturarComanda(Comanda comanda){
        initFacturasForm(false, comanda);
    }
    
    // ========================================================================
    //                              CRUD FUNCTIONS
    // ========================================================================
    /**
     * AÃƒÂ±adir comanda a BD
     */
    private void createComanda(){
        Request request;
        Usuario usuario_sesion;
        String server_action = "create-comanda";
        Comanda comanda;
        
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
        
            if (validarComandasForm()){
                comanda = getComandaFromForm();
                comanda.setCreated(new Date());
                comanda.setCreator(usuario_sesion.getId());
                comanda.setUpdated(new Date());
                comanda.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(comanda);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(createComandaClientListener());
                this.client_tcp.sendTCP(request);
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Actualizar comanda de la BD
     */
    private void updateComanda(Comanda comanda){
        Request request;
        Usuario usuario_sesion;
        Comanda comanda_update;
        String server_action = "update-comanda";
        
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
            comanda_update = getComandaFromForm();
            
            if (validarComandasForm()){
//                comanda_update.setId(comanda.getId());
//                comanda_update.setCreated(new Date());
//                comanda_update.setCreator(comanda.getCreator());
                comanda_update.setUpdated(new Date());
                comanda_update.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(comanda_update);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(updateComandaClientListener());
                this.client_tcp.sendTCP(request);
                
                this.comanda_update = null;
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * AÃ±ade una lista de detalle de comandas a BD
     */
    private void createDetalleComandas(List<DetalleComanda> detalle_comandas, Comanda comanda){
        Request request;
        Usuario usuario_sesion;
        String server_action = "create-detalle_comanda";
        
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
        
            if (validarComandasForm()){
//                List<DetalleComanda> detalle_comandas = getDetallesComandaFromForm();
                for (int i=0; i<detalle_comandas.size(); i++){
                    DetalleComanda dc = detalle_comandas.get(i);
                    dc.setCreated(new Date());
                    dc.setCreator(usuario_sesion.getId());
                    dc.setUpdated(new Date());
                    dc.setUpdater(usuario_sesion.getId());
                }

                request.setAction(server_action);
                request.setData(detalle_comandas);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(createDetalleComandasClientListener(comanda));
                this.client_tcp.sendTCP(request);
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Actualiza el detalle de una comanda
     * @param detalle_comandas
     * @param comanda 
     */
    private void updateDetalleComandas(List<DetalleComanda> detalle_comandas, Comanda comanda){
        Request request;
        Usuario usuario_sesion;
        String server_action = "update-detalle_comanda";
        
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
        
            if (validarComandasForm()){
//                List<DetalleComanda> detalle_comandas = getDetallesComandaFromForm();
                for (int i=0; i<detalle_comandas.size(); i++){
                    DetalleComanda dc = detalle_comandas.get(i);
//                    dc.setCreated(new Date());
//                    dc.setCreator(usuario_sesion.getId());
                    
                    dc.setUpdated(new Date());
                    dc.setUpdater(usuario_sesion.getId());
                }

                request.setAction(server_action);
                request.setData(detalle_comandas);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(createDetalleComandasClientListener(comanda));
                this.client_tcp.sendTCP(request);
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Elimina una comandadel sistema
     */
    private void deleteComanda(){
        List<Comanda> comandas = table_comandas.getSelectionModel().getSelectedItems();
        Comanda comanda;
        String server_action = "delete-comanda";
        
        if (comandas.size() == 1){
            comanda = comandas.get(0);
            
            alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Eliminar comanda");
            alert.setHeaderText(null);
            alert.setContentText("Se va a proceder a eliminar la comanda con ID ("+comanda.getId()+"). Ãƒâ€šÃ‚Â¿EstÃƒÆ’Ã‚Â¡ seguro?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                try{
                    Request request = new Request();
                    request.setAction(server_action);
                    request.setData(comanda);

                    this.client_tcp = new Client();
                    this.client_tcp.start();
                    if (!this.client_tcp.isConnected()){
                        this.client_tcp.connect();    
                    }
                    this.client_tcp.addListener(deleteComandaClientListener());
                    this.client_tcp.sendTCP(request);
                }catch(Exception e){
                    MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            alert = new Alert(AlertType.ERROR);
                            alert.setTitle("ERROR");
                            alert.setHeaderText(null);
                            alert.setContentText("Ups! Parace que algo no salÃƒÆ’Ã‚Â­o bien...");
                            alert.showAndWait();
                        }
                   });
                }
            }
        }
    }
    
    
    // ========================================================================
    //                           CLIENT LISTENERS
    // ========================================================================
    public Listener createComandaClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;
                        if (response.isStatus() && !response.isError()){
                            Comanda comanda = (Comanda) response.getData();
                            List<DetalleComanda> detalle_comandas = getDetallesComandaFromForm();
                            
                            for (int i=0; i<detalle_comandas.size(); i++){
                                DetalleComanda dc = detalle_comandas.get(i);
                                dc.setComanda(comanda.getId());
                            }
                            
                            createDetalleComandas(detalle_comandas, comanda);
                        }else{
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    alert = new Alert(AlertType.ERROR);
                                    alert.setTitle("ERROR");
                                    alert.setHeaderText(null);
                                    alert.setContentText(response.getMessage());
                                    alert.showAndWait();
                                }
                            });
                        }
                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener deleteComandaClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });

                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener updateComandaClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;
                        if (response.isStatus() && !response.isError()){
                            Comanda comanda = (Comanda) response.getData();
                            List<DetalleComanda> detalle_comandas = getDetallesComandaFromForm();
                            
                            for (int i=0; i<detalle_comandas.size(); i++){
                                DetalleComanda dc = detalle_comandas.get(i);
                                dc.setComanda(comanda.getId());
                            }
                            
                            updateDetalleComandas(detalle_comandas, comanda);
                        }else{
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    alert = new Alert(AlertType.ERROR);
                                    alert.setTitle("ERROR");
                                    alert.setHeaderText(null);
                                    alert.setContentText(response.getMessage());
                                    alert.showAndWait();
                                }
                            });
                        }
                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    } 
    public Listener createDetalleComandasClientListener(Comanda comanda){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                final Response response = (Response)object;

                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");

                                     resetComandaForm();
                                     
                                     reloadData();
                                }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                     try{
                                        DaoFactory.getComandaDao().delete(comanda.getId());    
                                     }catch(Exception e){
                                         MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                                     }
                                } 

                                reloadData();
                                alert.setHeaderText(null);
                                alert.setContentText(response.getMessage());
                                alert.showAndWait();
                             }
                        });
                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    
    // ========================================================================
    //                        LAUNCH FORMS FUNCTIONS
    // ========================================================================
    
    /**
     * Inicializa el formulario de usuarios
     */
    public void initUsuariosForm(boolean open_on_edit){
        UsuarioFormController controller;
        FXMLLoader loader;
        Stage modal;
        Scene scene;
        try{
            // Modal Stage
            modal = new Stage();
            modal.initModality(Modality.APPLICATION_MODAL);
            // Layout
            loader = new FXMLLoader(getClass().getResource("/fxml/FXMLUsuarioForm.fxml"));
            controller = new UsuarioFormController(open_on_edit);    
            loader.setController(controller);
            // Show scene
            scene = new Scene((Parent) loader.load());
            scene.getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");
            modal.getIcons().add(new Image(Config.getProperty("icon")));
            modal.setScene(scene);
            modal.setTitle("Gestion de Usuarios");
            modal.setResizable(false);
            modal.setOnHiding(this);
            modal.showAndWait();
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }

    /**
     * Inicializa el formulario de productos
     */
    public void initProductosForm(boolean open_on_edit){
        ProductoFormController controller;
        FXMLLoader loader;
        Stage modal;
        Scene scene;
        try{
            // Modal Stage
            modal = new Stage();
            modal.initModality(Modality.APPLICATION_MODAL);
            // Layout
            loader = new FXMLLoader(getClass().getResource("/fxml/FXMLProductoForm.fxml"));
            controller = new ProductoFormController(this, open_on_edit);
            loader.setController(controller);
            // Show scene
            scene = new Scene((Parent) loader.load());
            scene.getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");
            modal.getIcons().add(new Image(Config.getProperty("icon")));
            modal.setScene(scene);
            modal.setTitle("Gestion de Productos");
            modal.setResizable(false);
            modal.setOnHiding(this);
            modal.showAndWait();
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Inicializa el formulario de producto tipos
     */
    public void initProductoTiposForm(boolean open_on_edit){
        ProductoTipoFormController controller;
        FXMLLoader loader;
        Stage modal;
        Scene scene;
        try{
            // Modal Stage
            modal = new Stage();
            modal.initModality(Modality.APPLICATION_MODAL);
            // Layout
            loader = new FXMLLoader(getClass().getResource("/fxml/FXMLProductoTipoForm.fxml"));
            controller = new ProductoTipoFormController(open_on_edit);    
            loader.setController(controller);
            // Show scene
            scene = new Scene((Parent) loader.load());
            scene.getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");
            modal.getIcons().add(new Image(Config.getProperty("icon")));
            modal.setScene(scene);
            modal.setTitle("Gestion de Tipos de Producto");
            modal.setResizable(false);
            modal.setOnHiding(this);
            modal.showAndWait();
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Inicializa el formulario de mesas
     */
    public void initMesasForm(boolean open_on_edit){
        MesaFormController controller;
        FXMLLoader loader;
        Stage modal;
        Scene scene;
        try{
            // Modal Stage
            modal = new Stage();
            modal.initModality(Modality.APPLICATION_MODAL);
            // Layout
            loader = new FXMLLoader(getClass().getResource("/fxml/FXMLMesaForm.fxml"));
            controller = new MesaFormController(open_on_edit);    
            loader.setController(controller);
            // Show scene
            scene = new Scene((Parent) loader.load());
            scene.getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");
            modal.getIcons().add(new Image(Config.getProperty("icon")));
            modal.setScene(scene);
            modal.setTitle("Gestion de Mesas");
            modal.setResizable(false);
            modal.setOnHiding(this);
            modal.showAndWait();
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Inicializa el formulario de facturas
     */
    public void initFacturasForm(boolean open_on_edit, Comanda comanda){
        FacturaFormController controller;
        FXMLLoader loader;
        Stage modal;
        Scene scene;
        try{
            // Modal Stage
            modal = new Stage();
            modal.initModality(Modality.APPLICATION_MODAL);
            // Layout
            loader = new FXMLLoader(getClass().getResource("/fxml/FXMLFacturaForm.fxml"));
            if (comanda != null){
                controller = new FacturaFormController(open_on_edit, comanda);        
            }else{
                controller = new FacturaFormController(open_on_edit);
            }
            
            loader.setController(controller);
            // Show scene
            scene = new Scene((Parent) loader.load());
            scene.getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");
            modal.getIcons().add(new Image(Config.getProperty("icon")));
            modal.setScene(scene);
            modal.setTitle("Gestión de Facturas");
            modal.setResizable(false);
            modal.setOnHiding(this);
            modal.setOnCloseRequest(new EventHandler<WindowEvent>() {

                @Override
                public void handle(WindowEvent event) {
                    reloadData();
                }
            });
            modal.showAndWait();
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Inicializa el formulario de menus
     */
    public void initMenusForm(boolean open_on_edit){
        MenuFormController controller;
        FXMLLoader loader;
        Stage modal;
        Scene scene;
        try{
            // Modal Stage
            modal = new Stage();
            modal.initModality(Modality.APPLICATION_MODAL);
            // Layout
            loader = new FXMLLoader(getClass().getResource("/fxml/FXMLMenuForm.fxml"));
            controller = new MenuFormController(open_on_edit);    
            loader.setController(controller);
            // Show scene
            scene = new Scene((Parent) loader.load());
            scene.getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");
            modal.getIcons().add(new Image(Config.getProperty("icon")));
            modal.setScene(scene);
            modal.setTitle("Gestion de Menus");
            modal.setResizable(false);
            modal.setOnHiding(this);
            modal.showAndWait();
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Inicializa el formulario Acerca de Commander
     */
    public void initAcercaDeForm(){
        Stage modal;
        Scene scene;
        FXMLLoader loader;
        
        try{
            loader = new FXMLLoader(getClass().getResource("/fxml/FXMLAcercaDeForm.fxml"));
            scene = new Scene((Parent) loader.load());
            scene.getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");
            modal = new Stage();
            modal.setScene(scene);
            this.primary_stage.setTitle("Acerca de Commander");
            modal.setResizable(false);
            modal.getIcons().add(new Image(Config.getProperty("icon")));
            modal.showAndWait();

        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Solicita confirmaciÃ³n para salir de la aplicacion de manera completa.
     */
    public void closeApplication() throws Exception{
        alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Salir de la aplicacion");
        alert.setHeaderText(null);
        alert.setContentText("Se va a proceder a cerrar la aplicacion. ¿Esta seguro?");
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        // Add a custom icon.
        stage.getIcons().add(new Image(Config.getProperty("icon")));
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            System.exit(0);
        }
    }
    
    /**
     * Funcion manejadora del log de la aplicacion
     * @param c
     * @param text
     * @param level 
     */
    public static void Logger(Class c, String text, Level level){
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd");
        final String LOG_FILE = "log/log_" + sdf.format(new Date()) + ".log";
        Logger logger;
        FileHandler fileHandler;
        SimpleFormatter formatter;
        
        try{
            logger = Logger.getLogger(c.getName());
            fileHandler = new FileHandler(LOG_FILE, true);
            logger.addHandler(fileHandler);
            formatter = new SimpleFormatter();  
            fileHandler.setFormatter(formatter); 
            
            if (level.equals(Level.INFO)){
                logger.info(text);
            }
            
            if (level.equals(Level.WARNING)){
                logger.warning(text);
            }
            
            if (level.equals(Level.SEVERE)){
                logger.severe(text);
            }
            
            fileHandler.close();
            
            System.out.println(text);

        }catch (Exception e){
            MainFormController.Logger(MainFormController.class, ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    public void LoggerArea(String text){
        ta_log.setText( ta_log.getText() + text );
    }

    @Override
    public void received(Connection cnctn, Object o) {
        super.received(cnctn, o); //To change body of generated methods, choose Tools | Templates.
        
        try{
            if (o instanceof Request){
               Request request = (Request) o;
               if (request.getAction().equals("create_detalle-comanda")){
                   List data = (List) request.getData();
                   Comanda comanda = (Comanda) data.get(0);

                   reloadData();
                   t_text_info.setText("Nueva comanda recibida: " + comanda.toString());
               }
           }   
        }catch (Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
}
