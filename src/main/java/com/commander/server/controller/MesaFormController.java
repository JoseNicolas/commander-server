package com.commander.server.controller;

import com.commander.server.Request;
import com.commander.server.Response;
import com.commander.server.client.Client;
import com.commander.server.dao.DaoFactory;
import com.commander.server.model.Mesa;
import com.commander.server.model.Usuario;
import com.commander.server.utils.Validator;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.commons.lang3.exception.ExceptionUtils;

/*
 * 2019/04/30
 * @author JosÃƒÆ’Ã‚Â© Francisco NicolÃƒÆ’Ã‚Â¡s Bautista
 * @version 1.0
 */
public class MesaFormController implements Initializable, EventHandler {

    @FXML private Text t_text_info;
    @FXML private Button bt_eliminar;
    @FXML private Button bt_reset_form;
    @FXML private Button bt_aceptar;
    @FXML private Tab tab_lista_mesas;
    @FXML private TextField tf_mesa_nombre;
    @FXML private TextArea ta_mesa_descripcion;
    @FXML private TabPane tp_mesas;
    @FXML private Button bt_cancelar;
    @FXML private TableView<Mesa> table_mesas;
    @FXML private Tab tab_nuevo_editar_mesa;
    @FXML private Button bt_reload;
    @FXML private Button bt_editar;
    TableColumn<Mesa, Integer> tc_id;
    TableColumn<Mesa, String> tc_nombre;
    TableColumn<Mesa, String> tc_descripcion;
    TableColumn<Mesa, Date> tc_created;
    TableColumn<Mesa, Integer> tc_creator;
    TableColumn<Mesa, Date> tc_updated;
    TableColumn<Mesa, Integer> tc_updator;
    
    private Mesa mesa_update;
    private List<Mesa> mesas;
    private Client client_tcp;
    private Alert alert;
    private boolean open_on_edit;
    
    public MesaFormController(boolean open_on_edit) throws Exception{
        this.mesa_update = null;
        this.client_tcp = new Client();
        this.mesas = DaoFactory.getMesaDao().queryAll();
        this.open_on_edit = open_on_edit;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bt_aceptar.setOnAction(this);
        bt_cancelar.setOnAction(this);
        bt_editar.setOnAction(this);
        bt_eliminar.setOnAction(this);
        bt_reload.setOnAction(this);
        bt_reset_form.setOnAction(this);
        tab_nuevo_editar_mesa.setOnSelectionChanged(this);
        tab_lista_mesas.setOnSelectionChanged(this);
        
        tc_id = new TableColumn<>("ID");
        tc_nombre = new TableColumn<>("NOMBRE");
        tc_descripcion = new TableColumn<>("DESCRIPCION");
        tc_created = new TableColumn<>("CREATED");
        tc_creator = new TableColumn<>("CREATOR");
        tc_updated = new TableColumn<>("UPDATED");
        tc_updator = new TableColumn<>("UPDATER");
        
        tc_id.setCellValueFactory(new PropertyValueFactory<Mesa, Integer>("id"));
        tc_nombre.setCellValueFactory(new PropertyValueFactory<Mesa, String>("nombre"));
        tc_descripcion.setCellValueFactory(new PropertyValueFactory<Mesa, String>("descripcion"));
        tc_created.setCellValueFactory(new PropertyValueFactory<Mesa, Date>("created"));
        tc_creator.setCellValueFactory(new PropertyValueFactory<Mesa, Integer>("creator"));
        tc_updated.setCellValueFactory(new PropertyValueFactory<Mesa, Date>("updated"));
        tc_updator.setCellValueFactory(new PropertyValueFactory<Mesa, Integer>("updater"));
        tc_creator.setVisible(false);// Columna oculta por defecto
        tc_created.setVisible(false);// Columna oculta por defecto
        tc_updator.setVisible(false);// Columna oculta por defecto
        
        table_mesas.getColumns().addAll(tc_id, tc_nombre, tc_descripcion, 
                tc_created,tc_creator, tc_updated, tc_updator);
        table_mesas.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        if (open_on_edit){
            tp_mesas.getSelectionModel().select(tab_lista_mesas); 
        }  
    }    

    /**
     * Implement handle event from Interface EventHandler
     * @param event 
     */
    @Override
    public void handle(Event event) {
        try{
            Class target = event.getTarget().getClass();
            
            // Botones Aceptar, Cancelar
            if (target.equals(Button.class)){
                Button button = (Button) event.getSource();
                if (bt_aceptar.equals(button)){
                    if (this.mesa_update == null){
                        createMesa();
                    }else{
                        updateMesa(this.mesa_update);
                    }
                }
                
                if (bt_cancelar.equals(button)){
                    Stage stage = (Stage) button.getScene().getWindow();
                    stage.close();
                }
                
                if (bt_eliminar.equals(button)){
                    deleteMesa();
                }
                
                if (bt_editar.equals(button)){
                    Mesa mesa = (Mesa) table_mesas.getSelectionModel().getSelectedItem();
                    if (mesa != null){
                        this.mesa_update = mesa;
                        setMesaToForm(mesa);
                        tp_mesas.getSelectionModel().select(tab_nuevo_editar_mesa);   
                    }
                }
                
                if (bt_reload.equals(button)){
                    reloadMesasTable();
                    ObservableList<Mesa> ol_mesas = FXCollections.observableArrayList(mesas);
                    table_mesas.setItems(ol_mesas);
                }
                
                if (bt_reset_form.equals(button)){
                    resetMesaForm();
                }
            }
            
            // PestaÃƒÂ±as (Tab)
            if (target.equals(Tab.class)){
                if (tab_nuevo_editar_mesa.isSelected()){
                }
               
                if (tab_lista_mesas.isSelected()){
                    ObservableList<Mesa> ol_mesas = FXCollections.observableArrayList(mesas);
                    table_mesas.setItems(ol_mesas);
                }
            }
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Valida los campos del formulario Mesas
     */
    private boolean validarMesasForm(){
        boolean is_valid = true;
        
        String nombre = tf_mesa_nombre.getText();
        String descripcion = ta_mesa_descripcion.getText();

        if (Validator.isEmpty(nombre)){
            t_text_info.setText("Nombre* es un campo obligatorio");
            tf_mesa_nombre.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            tf_mesa_nombre.setStyle("-fx-border-color: green;");
        }
        
        if (Validator.isGreatherThan(descripcion, 250)){
            t_text_info.setText("DescripciÃƒÆ’Ã‚Â³n* no puede contener mÃƒÆ’Ã‚Â¡s de 250 caracteres");
            ta_mesa_descripcion.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            ta_mesa_descripcion.setStyle("-fx-border-color: green;");        
        }
        
        t_text_info.setFill(Color.RED);
        
        if (is_valid){
            t_text_info.setText("");
        }
        
        return is_valid;
    }
    
    /**
     * Inicializa los campos del formulario con los valores de una mesa
     * @param mesa 
     */
    private void setMesaToForm(Mesa mesa){
        try{
            tf_mesa_nombre.setText(mesa.getNombre());
            ta_mesa_descripcion.setText(mesa.getDescripcion());
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Devuelve la mesa obtenida desde el formulario de mesas
     * @return Mesa mesa
     */
    private Mesa getMesaFromForm(){
        Mesa mesa = new Mesa();
        
        try{        
            mesa.setNombre(tf_mesa_nombre.getText());
            mesa.setDescripcion(ta_mesa_descripcion.getText());
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }

        return mesa;
    }
    
    /**
     * Reinicializa el formulario de nuevo mesa
     */
    private void resetMesaForm(){
        tf_mesa_nombre.setText("");
        ta_mesa_descripcion.setText("");
        t_text_info.setText("");
        
        tf_mesa_nombre.setStyle(null);
        ta_mesa_descripcion.setStyle(null);
    }
    
    /**
     * Recarga el listado de mesas en la tabla
     */
    private void reloadMesasTable(){
        try{
            this.mesas = DaoFactory.getMesaDao().queryAll();
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        } 
    }
    
    // ========================================================================
    //                              CRUD FUNCTIONS
    // ========================================================================
    /**
     * AÃƒÆ’Ã‚Â±adir mesa a BD
     */
    private void createMesa(){
        Request request;
        Usuario usuario_sesion;
        String server_action = "create-mesa";
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
        
            if (validarMesasForm()){
                Mesa mesa = getMesaFromForm();
                mesa.setCreated(new Date());
                mesa.setCreator(usuario_sesion.getId());
                mesa.setUpdated(new Date());
                mesa.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(mesa);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(createMesaClientListener());
                this.client_tcp.sendTCP(request);
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Actualizar mesa de la BD
     */
    private void updateMesa(Mesa mesa){
        Request request;
        Usuario usuario_sesion;
        Mesa mesa_updated;
        String server_action = "update-mesa";
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
            mesa_updated = getMesaFromForm();
            
            if (validarMesasForm()){
                mesa_updated.setId(mesa.getId());
//                mesa_updated.setCreated(mesa.getCreated());
//                mesa_updated.setCreator(mesa.getCreator());
                mesa_updated.setUpdated(new Date());
                mesa_updated.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(mesa_updated);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(updateMesaClientListener());
                this.client_tcp.sendTCP(request);
                
                this.mesa_update = null;
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Elimina mesa del sistema
     */
    private void deleteMesa(){
        List<Mesa> mesas = table_mesas.getSelectionModel().getSelectedItems();
        Mesa mesa;
        String server_action = "delete-mesa";
        
        if (mesas.size() == 1){
            mesa = mesas.get(0);
            
            alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Eliminar mesa");
            alert.setHeaderText(null);
            alert.setContentText("Se va a proceder a eliminar la mesa con ID ("+mesa.getId()+"). Â¿EstÃ¡ seguro?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                try{
                    Request request = new Request();
                    request.setAction(server_action);
                    request.setData(mesa);

                    this.client_tcp = new Client();
                    this.client_tcp.start();
                    if (!this.client_tcp.isConnected()){
                        this.client_tcp.connect();    
                    }
                    this.client_tcp.addListener(deleteMesaClientListener());
                    this.client_tcp.sendTCP(request);
                }catch(Exception e){
                    MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            alert = new Alert(AlertType.ERROR);
                            alert.setTitle("ERROR");
                            alert.setHeaderText(null);
                            alert.setContentText("Ups! Parace que algo no saliÃƒÂ³ bien...");
                            alert.showAndWait();
                        }
                   });
                }
            }
        }
    }
    
    // ========================================================================
    //                           CLIENT LISTENERS
    // ========================================================================
    public Listener createMesaClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");

                                     resetMesaForm();
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });

                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener deleteMesaClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });
                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener updateMesaClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");

                                     resetMesaForm();
                                     reloadMesasTable();
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });

                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
}
