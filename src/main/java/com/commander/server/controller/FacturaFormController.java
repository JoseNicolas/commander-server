package com.commander.server.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.commander.server.Request;
import com.commander.server.Response;
import com.commander.server.ResponseMessage;
import com.commander.server.client.Client;
import com.commander.server.dao.DaoFactory;
import com.commander.server.model.Comanda;
import com.commander.server.model.DetalleComanda;
import com.commander.server.model.Factura;
import com.commander.server.model.Mesa;
import com.commander.server.model.Producto;
import com.commander.server.model.ProductoTipo;
import com.commander.server.model.Usuario;
import com.commander.server.utils.Config;
import com.commander.server.utils.HeaderFooterPageEvent;
import com.commander.server.utils.Validator;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
//import com.qoppa.pdfViewerFX.PDFViewer;
import com.sun.scenario.effect.ImageData;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.swing.text.StyleConstants.FontConstants;
import org.apache.commons.lang3.exception.ExceptionUtils;

/*
 * 2019/05/19
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class FacturaFormController extends PdfPageEventHelper implements Initializable, EventHandler {

    @FXML private TextField tf_factura_importe;
    @FXML private Tab tab_nueva_editar_factura;
    @FXML private TabPane tp_facturas;
    @FXML private TextField tf_factura_codigo;
    @FXML private ChoiceBox<Comanda> cb_factura_comanda;
    @FXML private TextArea ta_factura_observaciones;
    @FXML private Tab tab_lista_facturas;
    @FXML private Text t_text_info;
    @FXML private ChoiceBox<String> cb_factura_pagada;
    @FXML private TableView<Factura> table_facturas;
    @FXML private Button bt_eliminar;
    @FXML private Button bt_reload;
    @FXML private Button bt_aceptar;
    @FXML private Button bt_cancelar;
    @FXML private Button bt_editar;
    @FXML private Button bt_reset_form;
    @FXML private Button bt_calcular_importe;
    @FXML private Button bt_codigo_factura_auto;
    @FXML private DatePicker dp_factura_fecha;
    TableColumn<Factura, Integer> tc_id;
    TableColumn<Factura, String> tc_codigo;
    TableColumn<Factura, Date> tc_fecha;
    TableColumn<Factura, Integer> tc_comanda;
    TableColumn<Factura, Float> tc_importe;
    TableColumn<Factura, Integer> tc_pagada;
    TableColumn<Factura, String> tc_observaciones;
    TableColumn<Factura, Date> tc_created;
    TableColumn<Factura, Integer> tc_creator;
    TableColumn<Factura, Date> tc_updated;
    TableColumn<Factura, Integer> tc_updator;
    
    List<Factura> facturas;
    Factura factura_update;
    List<Comanda> comandas;
    boolean open_on_edit;
    private Alert alert;
    Comanda comanda_selected;
    private PdfTemplate t;
    private Image total;
    private Client client_tcp;

    public FacturaFormController(boolean open_on_edit) throws Exception{
        this.open_on_edit = open_on_edit;
        facturas = DaoFactory.getFacturaDao().queryAll();
        comandas = DaoFactory.getComandaDao().queryAll();
        comanda_selected = null;
        this.client_tcp = new Client();
    }
    
    public FacturaFormController(boolean open_on_edit, Comanda comanda) throws Exception{
        this.open_on_edit = open_on_edit;
        facturas = DaoFactory.getFacturaDao().queryAll();
        comandas = DaoFactory.getComandaDao().queryAll();
        comanda_selected = comanda;
        this.client_tcp = new Client();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bt_eliminar.setOnAction(this);
        bt_reload.setOnAction(this);
        bt_aceptar.setOnAction(this);
        bt_cancelar.setOnAction(this);
        bt_editar.setOnAction(this);
        bt_reset_form.setOnAction(this);
        bt_calcular_importe.setOnAction(this);
        bt_codigo_factura_auto.setOnAction(this);
        
        tab_lista_facturas.setOnSelectionChanged(this);
        tab_nueva_editar_factura.setOnSelectionChanged(this);
        
        dp_factura_fecha.setValue(LocalDate.now());
        
        cb_factura_pagada.getItems().add("Si");
        cb_factura_pagada.getItems().add("No");
        cb_factura_comanda.getSelectionModel().select(0);
        
        cb_factura_comanda.setItems(FXCollections.observableArrayList(comandas));
        if (comanda_selected != null){
            cb_factura_comanda.getSelectionModel().select(comanda_selected);
            Comanda comanda_selected = cb_factura_comanda.getSelectionModel().getSelectedItem();
            float importe = calcularImporteFactura(comanda_selected);
            if (importe > 0){
                tf_factura_importe.setText(String.valueOf(importe));    
            }
            String codigo = calcularCodigoFactura();
            if (codigo != ""){
                tf_factura_codigo.setText(codigo);
            }
            cb_factura_pagada.getSelectionModel().select(0);
            
            factura_update = getFacturaFromForm();
            
            try{
                comanda_selected.setEstado("facturada");
                DaoFactory.getComandaDao().update(comanda_selected.getId(), comanda_selected);
            }catch (Exception e){
                MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
            }
            
            createFactura();
        }
        
        if (open_on_edit){
            tp_facturas.getSelectionModel().select(tab_lista_facturas);
        }

        tc_id = new TableColumn<>("ID");
        tc_codigo = new TableColumn<>("CODIGO");
        tc_fecha = new TableColumn<>("FECHA");
        tc_comanda = new TableColumn<>("COMANDA");
        tc_importe = new TableColumn<>("IMPORTE");
        tc_pagada = new TableColumn<>("PAGADA");
        tc_observaciones = new TableColumn<>("OBSERV.");
        tc_created = new TableColumn<>("CREATED");
        tc_creator = new TableColumn<>("CREATOR");
        tc_updated = new TableColumn<>("UPDATED");
        tc_updator = new TableColumn<>("UPDATOR");
        
        tc_id.setCellValueFactory(new PropertyValueFactory<Factura, Integer>("id"));
        tc_codigo.setCellValueFactory(new PropertyValueFactory<Factura, String>("codigo"));
        tc_fecha.setCellValueFactory(new PropertyValueFactory<Factura, Date>("fecha"));
        tc_comanda.setCellValueFactory(new PropertyValueFactory<Factura, Integer>("comanda"));
        tc_importe.setCellValueFactory(new PropertyValueFactory<Factura, Float>("importe"));
        tc_pagada.setCellValueFactory(new PropertyValueFactory<Factura, Integer>("pagada"));
        tc_observaciones.setCellValueFactory(new PropertyValueFactory<Factura, String>("observaciones"));
        tc_created.setCellValueFactory(new PropertyValueFactory<Factura, Date>("created"));
        tc_creator.setCellValueFactory(new PropertyValueFactory<Factura, Integer>("creator"));
        tc_updated.setCellValueFactory(new PropertyValueFactory<Factura, Date>("updated"));
        tc_updator.setCellValueFactory(new PropertyValueFactory<Factura, Integer>("updater"));
        tc_creator.setVisible(false);// Columna oculta por defecto
        tc_created.setVisible(false);// Columna oculta por defecto
        tc_updator.setVisible(false);// Columna oculta por defecto

        table_facturas.getColumns().addAll(tc_id, tc_codigo, tc_fecha, tc_comanda,
                tc_importe, tc_pagada, tc_observaciones, tc_created,
                tc_creator, tc_updated, tc_updator);
        table_facturas.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table_facturas.setRowFactory( tv -> {
            TableRow<Factura> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    try{
                        Factura factura = table_facturas.getSelectionModel().getSelectedItem();
                        mostrarFactura(factura);
                    }catch (Exception e){
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            });
            return row;
        });
        
        reloadFacturasTable();
    }    

    @Override
    public void handle(Event event) {
        try{
            Class target = event.getTarget().getClass();
            
            // Botones Aceptar, Cancelar
            if (target.equals(Button.class)){
                Button button = (Button) event.getSource();
                if (bt_aceptar.equals(button)){
                    if (factura_update == null){
                        createFactura();   
                    }else{
                        updateFactura(factura_update);
                    }
                }
                if (bt_editar.equals(button)){
                    Factura factura = table_facturas.getSelectionModel().getSelectedItem();
                    if (factura != null){
                        this.factura_update = factura;
                        setFacturaToForm(factura);
                        tp_facturas.getSelectionModel().select(tab_nueva_editar_factura);
                    }
                }
                if (bt_reset_form.equals(button)){
                    comanda_selected = null;
                    resetFacturasForm();
                }
                if (bt_cancelar.equals(button)){
                    Stage stage = (Stage) button.getScene().getWindow();
                    stage.close();
                }
                if (bt_calcular_importe.equals(button)){
                    Comanda comanda_selected = cb_factura_comanda.getSelectionModel().getSelectedItem();
                    float importe = calcularImporteFactura(comanda_selected);
                    if (importe > 0){
                        tf_factura_importe.setText(String.valueOf(importe));    
                    }
                }
                if (bt_codigo_factura_auto.equals(button)){
                    String codigo = calcularCodigoFactura();
                    if (codigo != ""){
                        tf_factura_codigo.setText(codigo);
                    }
                }
                if (bt_reload.equals(button)){
                    reloadFacturasTable();
                }
            }
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Obtiene la factura definida en el formulario (cast)
     * @return 
     */
    public Factura getFacturaFromForm(){
        Factura factura = new Factura();
        
        ProductoTipo pt = null;
        Usuario usuario_sesion;
        
        try{        
            usuario_sesion = MainFormController.getUserSesion();
            LocalDate localDate = dp_factura_fecha.getValue();
            Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
            Date date = Date.from(instant);

            factura.setCodigo(tf_factura_codigo.getText());
            factura.setFecha(date);
            factura.setComanda(cb_factura_comanda.getSelectionModel().getSelectedItem().getId());
            factura.setImporte(Float.parseFloat(tf_factura_importe.getText()));
            factura.setPagada(cb_factura_pagada.getSelectionModel().getSelectedItem().equals("Si") ? true : false);
            factura.setObservaciones(ta_factura_observaciones.getText());
            
            factura.setCreated(new Date());
            factura.setCreator(usuario_sesion.getId());
            factura.setUpdated(new Date());
            factura.setUpdater(usuario_sesion.getId());
            
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }

        return factura;
    }
    
    /**
     * Inicializa los campos del formulario con los valores de una factura
     * @param factura 
     */
    public void setFacturaToForm(Factura factura){
        Optional<Comanda> comanda_factura_filter = comandas.stream().filter(c -> c.getId() == factura.getComanda()).findFirst();
        Comanda comanda_factura = null;
        if (comanda_factura_filter.isPresent()){
            comanda_factura = comanda_factura_filter.get();
        }
        
        try{
            tf_factura_codigo.setText(factura.getCodigo());
            dp_factura_fecha.setValue(factura.getFecha().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            cb_factura_comanda.getSelectionModel().select(comanda_factura);
            tf_factura_importe.setText(String.valueOf(factura.getImporte()));
            cb_factura_pagada.getSelectionModel().select(factura.isPagada() ? 0 : 1);
            ta_factura_observaciones.setText(factura.getObservaciones());
        }catch (Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
        
    }
    
    /**
     * Obtiene el importe total de una factura en funcionn del detalle de la comanda especificada en dicha factura.
     * @param comanda 
     */
    public float calcularImporteFactura(Comanda comanda){
        float importe_total = 0;
        if (comanda != null){
            try{
                
                List<DetalleComanda> detalles_comanda = DaoFactory.getDetalleComandaDao().queryByComanda(comanda);
                if (detalles_comanda.size() > 0){

                    for (int i=0; i<detalles_comanda.size(); i++){
                        try{
                            DetalleComanda dt = detalles_comanda.get(i);
                            Producto p = DaoFactory.getProductoDao().load(dt.getProducto());
                            if (p != null){
                                float cantidad = dt.getCantidad();
                                float precio_unit = p.getPrecio_unit();

                                importe_total += cantidad * precio_unit;
                            }
                        }catch (Exception e){
                            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                            alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("ERROR");
                            alert.setHeaderText(null);
                            alert.setContentText("Se produjo un error calculando el importe de la factura con comanda ID:" + comanda.getId());
                            alert.showAndWait();
                        }
                    }

                    
                }
                
            }catch(Exception e){
                MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
            }
        }else{
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText(null);
            alert.setContentText("Seleccione una comanda para calcular el importe");
            alert.showAndWait(); 
        }
        
        return importe_total;
    }
    
    /**
     * Calcula el codigo de la siguiente factura
     * @return 
     */
    public String calcularCodigoFactura(){
        String codigo = "";
        try{
            codigo = "F" + new SimpleDateFormat("yy").format(new Date()) + (1001 + facturas.size());    
        }catch (Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText(null);
            alert.setContentText("Se produjo calculando el codigo de factura");
            alert.showAndWait();
        }
        
        return codigo;
    }
    
    /**
     * Valida los campos del formulario Productos
     */
    private boolean validarFacturasForm(){
        boolean is_valid = true;
        
        String codigo = tf_factura_codigo.getText();
        LocalDate fecha = dp_factura_fecha.getValue();
        Comanda comanda = cb_factura_comanda.getSelectionModel().getSelectedItem();
        String importe = tf_factura_importe.getText();
        String pagada = cb_factura_pagada.getSelectionModel().getSelectedItem();

        if (Validator.isEmpty(codigo)){
            t_text_info.setText("Codigo* es un campo obligatorio");
            tf_factura_codigo.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            tf_factura_codigo.setStyle("-fx-border-color: green;");
        }
        
        if (Validator.isNull(fecha)){
            t_text_info.setText("Fecha* es un campo obligatorio");
            dp_factura_fecha.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            dp_factura_fecha.setStyle("-fx-border-color: green;");
        }
        
        if (Validator.isNull(comanda)){
            t_text_info.setText("Comanda* es un campo obligatorio");
            cb_factura_comanda.setStyle("-fx-border-color: red;");
        }else{
            cb_factura_comanda.setStyle("-fx-border-color: green;");
        }
        
        if (!Validator.isNumber(importe)){
            t_text_info.setText("Importe(€)* es un campo obligatorio");
            tf_factura_importe.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            tf_factura_importe.setStyle("-fx-border-color: green;");        
        }
        
        if (Validator.isNull(pagada)){
            t_text_info.setText("Pagada* es un campo obligatorio");
            cb_factura_pagada.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            cb_factura_pagada.setStyle("-fx-border-color: green;");        
        }
        
        t_text_info.setFill(Color.RED);
        
        if (is_valid){
            t_text_info.setText("");
        }
        
        return is_valid;
    }
    
    /**
     * Reinicializa el formulario de facturas
     */
    private void resetFacturasForm(){
        tf_factura_codigo.setText("");
        dp_factura_fecha.setValue(null);
        cb_factura_comanda.getSelectionModel().select(null);
        tf_factura_importe.setText("");
        cb_factura_pagada.getSelectionModel().select(null);
        ta_factura_observaciones.setText("");
        t_text_info.setText("");
        
        tf_factura_codigo.setStyle(null);
        dp_factura_fecha.setStyle(null);
        cb_factura_comanda.setStyle(null);
        tf_factura_importe.setStyle(null);
        cb_factura_pagada.setStyle(null);
        ta_factura_observaciones.setStyle(null);
    }
    
    /**
     * Recarga el listado de facturas en la tabla
     */
    private void reloadFacturasTable(){
        try{
            facturas = DaoFactory.getFacturaDao().queryAll();
            ObservableList<Factura> ol_facturas = FXCollections.observableArrayList(facturas);
            table_facturas.setItems(ol_facturas);
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        } 
    }
    
    /**
     * Añade una factura a BD
     */
    private void createFactura(){
        Request request;
        Usuario usuario_sesion;
        String server_action = "create-factura";
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
        
            if (validarFacturasForm()){
                Factura factura = getFacturaFromForm();
//                factura.setId(producto.getId());
                factura.setFecha(new Date());
                factura.setCreated(new Date());
                factura.setCreator(usuario_sesion.getCreator());
                factura.setUpdated(new Date());
                factura.setUpdater(usuario_sesion.getId());
                               
                request.setAction(server_action);
                request.setData(factura);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(createFacturaClientListener());
                this.client_tcp.sendTCP(request);
            }   
           
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Actualizar factura de la BD
     */
    private void updateFactura(Factura factura){
        Request request;
        Usuario usuario_sesion;
        Factura factura_updated;
        String server_action = "update-factura";
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
            factura_updated = getFacturaFromForm();
            
            if (validarFacturasForm()){
                factura_updated.setId(factura.getId());
//                mesa_updated.setCreated(mesa.getCreated());
//                mesa_updated.setCreator(mesa.getCreator());
                factura_updated.setUpdated(new Date());
                factura_updated.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(factura_updated);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(updateFacturaClientListener());
                this.client_tcp.sendTCP(request);
                
                this.factura_update = null;
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Imprime el ticket de factura
     */
    public boolean imprimirFactura(Factura factura){
        Comanda comanda;
        List<DetalleComanda> detalle_comanda;
        Document document;
        PdfPTable table_detalle_productos;
        PdfPTable table_cabecera;
        PdfPTable table_detalle_comanda;
        PdfPTable table_pie;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        float total_comanda;
        boolean is_factura_impresa = false;
                
        try{
            comanda = DaoFactory.getComandaDao().load(factura.getComanda());
            if (comanda != null){
                detalle_comanda = DaoFactory.getDetalleComandaDao().queryByComanda(comanda);    
                if (detalle_comanda != null){

                    document = new Document();
                    PdfWriter.getInstance(document, new FileOutputStream(Config.getProperty("directory_facturas") + "/" + factura.getCodigo() + ".pdf"));

                    document.open();

                    // CABECERA FACTURA
                    Image image = Image.getInstance(Config.getProperty("icon_factura"));
                    image.scalePercent(15);

                    table_cabecera = new PdfPTable(2);
                    table_cabecera.setWidths(new float[] {20f, 80f});
                    
                    PdfPCell imagen_factura_cell = new PdfPCell();
                    imagen_factura_cell.setRowspan(4);
                    imagen_factura_cell.addElement(image);
                    imagen_factura_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    
                    table_cabecera.addCell(imagen_factura_cell);
                    table_cabecera.addCell(new Paragraph(Config.getProperty("restaurante_nombre")));
                    table_cabecera.addCell(new Paragraph(Config.getProperty("restaurante_direccion")));
                    table_cabecera.addCell(new Paragraph(Config.getProperty("restaurante_telefono")));
                    table_cabecera.addCell(new Paragraph(Config.getProperty("restaurante_nif")));
                    table_cabecera.setSpacingAfter(25f);
                    
                    document.add(table_cabecera);
                    document.add(Chunk.NEWLINE);

                    // DETALLE COMANDA
                    table_detalle_comanda = new PdfPTable(1);
                    table_detalle_comanda.addCell("Fecha: "+sdf.format(new Date())+" Mesa: "+comanda.getMesa()+" Camarero: "+comanda.getCamarero());
                    table_detalle_comanda.setSpacingAfter(25f);
                    
                    document.add(table_detalle_comanda);
                    document.add(Chunk.NEWLINE);

                    // DETALLE PRODUCTOS
                    table_detalle_productos = new PdfPTable(3);
                    addTableHeader(table_detalle_productos);
                    total_comanda = addRows(table_detalle_productos, detalle_comanda);
                    table_detalle_productos.setSpacingAfter(25f);
                    
                    document.add(table_detalle_productos);
                    document.add(Chunk.NEWLINE);

                    // PIE DE FACTURA
                    table_pie = new PdfPTable(1);
                    table_pie.addCell("10.00% IVA Sobre [" + 0.9f * total_comanda + " €] " + 0.1f * total_comanda + "€");
                    table_pie.setSpacingAfter(25f);

                    document.add(table_pie);
                    document.add(Chunk.NEWLINE);
                    
                    Paragraph saludo =new Paragraph("GRACIAS POR SU VISITA, VUELVA PRONTO!");
                    saludo.setAlignment(Element.ALIGN_CENTER);
                    document.add(saludo);

                    document.close();
                    
                    is_factura_impresa = true;
                }
            }
        }catch (Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
        
        return is_factura_impresa;
    }
    private void addTableHeader(PdfPTable table) {
        Stream.of("CANT.", "PRODUCTO", "€/UNIDAD")
          .forEach(columnTitle -> {
            PdfPCell header = new PdfPCell();
            header.setBackgroundColor(BaseColor.LIGHT_GRAY);
            header.setBorderWidth(1);
            header.setPhrase(new Phrase(columnTitle));
            try {
                table.setWidths(new float[] { 15f, 65f, 20f });
            } catch (DocumentException ex) {
                Logger.getLogger(FacturaFormController.class.getName()).log(Level.SEVERE, null, ex);
            }
            table.addCell(header);
        });
    }
    private float addRows(PdfPTable table, List<DetalleComanda> detalle_comanda) throws Exception {
        float total_comanda = 0;
        for (int i=0; i<detalle_comanda.size(); i++){
            DetalleComanda detalle = detalle_comanda.get(i);
            Producto producto = DaoFactory.getProductoDao().load(detalle.getProducto());
            
            String producto_nombre = producto.getNombre();
            String producto_cantidad = String.valueOf(detalle.getCantidad());
            String producto_precio = String.valueOf(producto.getPrecio_unit());
            try{
                total_comanda += detalle.getCantidad() * producto.getPrecio_unit();    
            }catch (Exception e){
                MainFormController.Logger(this.getClass(), ("Se produjo un error calculando total de factura: " + factura_update.getCodigo()), Level.SEVERE);
                total_comanda = 0;
            }
            table.addCell(producto_cantidad);
            table.addCell(producto_nombre);
            table.addCell(producto_precio);
        }
        table.addCell("");
        table.addCell("");
        table.addCell("");
        
        PdfPCell total_importe_text = new PdfPCell();
        total_importe_text.setBackgroundColor(BaseColor.LIGHT_GRAY);
        total_importe_text.setBorderWidth(1);
        total_importe_text.setColspan(2);
        total_importe_text.setHorizontalAlignment(Element.ALIGN_CENTER);
        total_importe_text.setPhrase(new Phrase("TOTAL IMPORTE"));
        table.addCell(total_importe_text);
//        table.addCell(header);

        table.addCell(String.valueOf(total_comanda) + " €");
        
        return total_comanda;
    }
    private void addCustomRows(PdfPTable table) throws URISyntaxException, BadElementException, IOException, Exception {
//      Path path = Paths.get(ClassLoader.getSystemResource("/images/icon_app.png").toURI());
      Path path = Paths.get(Config.getProperty("icon_factura"));
      Image img = Image.getInstance(path.toAbsolutePath().toString());
      img.scalePercent(10);

      PdfPCell imageCell = new PdfPCell(img);
      table.addCell(imageCell);

      PdfPCell horizontalAlignCell = new PdfPCell(new Phrase("row 2, col 2"));
      horizontalAlignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
      table.addCell(horizontalAlignCell);

      PdfPCell verticalAlignCell = new PdfPCell(new Phrase("row 2, col 3"));
      verticalAlignCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
      table.addCell(verticalAlignCell);
    }
    
    /**
     * Lanza el visor PDF por defecto en el sistema con la factura seleccionada
     * @param factura
     * @throws Exception 
     */
    private void mostrarFactura(Factura factura) throws Exception{
        try {
            File file = new File(Config.getProperty("directory_facturas") + "/" + factura.getCodigo() + ".pdf");
            Desktop desktop = Desktop.getDesktop();

            // Open a file using the default program for the file type. In the example 
            // we will launch a default registered program to open a text file. For 
            // example on Windows operating system this call might launch a notepad.exe 
            // to open the file.
            desktop.open(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
    
    // ========================================================================
    //                           CLIENT LISTENERS
    // ========================================================================
    public Listener createFacturaClientListener(){
        
        return new Listener(){
            public void received (Connection connection, Object object) {
              
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(Alert.AlertType.INFORMATION);
                                     alert.setTitle("INFO");
                                     
                                     try{
                                        Factura factura_updated = (Factura) response.getData();
                                        Comanda comanda_factura = cb_factura_comanda.getSelectionModel().getSelectedItem();
                                        comanda_factura.setFactura(factura_updated.getId());
                                        DaoFactory.getComandaDao().update(comanda_factura.getId(), comanda_factura);
                                     }catch (Exception e){}
                                     
                                     imprimirFactura(factura_update);
                                     resetFacturasForm();
                                     reloadFacturasTable();
                                     factura_update = null;
                                 }else{
                                     alert = new Alert(Alert.AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });
                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener updateFacturaClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(Alert.AlertType.INFORMATION);
                                     alert.setTitle("INFO");

                                     resetFacturasForm();
                                     reloadFacturasTable();
                                 }else{
                                     alert = new Alert(Alert.AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });

                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
}


