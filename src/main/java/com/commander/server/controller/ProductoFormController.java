package com.commander.server.controller;

import com.commander.server.Request;
import com.commander.server.Response;
import com.commander.server.ResponseMessage;
import com.commander.server.client.Client;
import com.commander.server.dao.DaoFactory;
import com.commander.server.model.Producto;
import com.commander.server.model.ProductoTipo;
import com.commander.server.model.Usuario;
import com.commander.server.utils.Config;
import com.commander.server.utils.Validator;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.util.InputStreamSender;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.exception.ExceptionUtils;

/*
 * 2019/04/29
 * @author JosÃ© Francisco NicolÃ¡s Bautista
 * @version 1.0
 */
public class ProductoFormController implements Initializable, EventHandler {

    @FXML private Button bt_eliminar;
    @FXML private TextField tf_producto_precio_unit;
    @FXML private TabPane tp_productos;
    @FXML private Text t_text_info;
    @FXML private Tab tab_lista_productos;
    @FXML private Button bt_reset_form;
    @FXML private Button bt_reload;
    @FXML private Button bt_aceptar;
    @FXML private Button bt_codigo_producto_auto;
    @FXML private ChoiceBox<ProductoTipo> cb_producto_tipo;
    @FXML private Tab tab_nuevo_editar_producto;
    @FXML private TextField tf_producto_codigo;
    @FXML private Button bt_cancelar;
    @FXML private TableView<Producto> table_productos;
    @FXML private TextField tf_producto_nombre;
    @FXML private Button bt_editar;
    @FXML private TextArea ta_producto_descripcion;
    @FXML private ImageView iv_imagen;
    TableColumn<Producto, Integer> tc_id;
    TableColumn<Producto, String> tc_nombre;
    TableColumn<Producto, String> tc_descripcion;
    TableColumn<Producto, String> tc_codigo;
    TableColumn<Producto, Integer> tc_tipo;
    TableColumn<Producto, Float> tc_precio_unit;
    TableColumn<Producto, String> tc_imagen;
    TableColumn<Producto, Date> tc_created;
    TableColumn<Producto, Integer> tc_creator;
    TableColumn<Producto, Date> tc_updated;
    TableColumn<Producto, Integer> tc_updator;
    
    private MainFormController main_controller;
    private Producto producto_update;
    private List<Producto> productos;
    private List<ProductoTipo> producto_tipos;
    private Client client_tcp;
    private Alert alert;
    private File image_file;
    private boolean open_on_edit;
    
    public ProductoFormController(MainFormController main_controller, boolean open_on_edit) throws Exception{
        this.main_controller = main_controller;
        this.producto_update = null;
        this.client_tcp = new Client();
        this.productos = DaoFactory.getProductoDao().queryAll();
        this.producto_tipos = DaoFactory.getProductoTipoDao().queryAll();
        this.open_on_edit = open_on_edit;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bt_aceptar.setOnAction(this);
        bt_cancelar.setOnAction(this);
        bt_editar.setOnAction(this);
        bt_eliminar.setOnAction(this);
        bt_reload.setOnAction(this);
        bt_reset_form.setOnAction(this);
        bt_codigo_producto_auto.setOnAction(this);
        tab_nuevo_editar_producto.setOnSelectionChanged(this);
        tab_lista_productos.setOnSelectionChanged(this);
        iv_imagen.setOnMouseClicked(this);
        
        tc_id = new TableColumn<>("ID");
        tc_nombre = new TableColumn<>("NOMBRE");
        tc_descripcion = new TableColumn<>("DESCRIPCION");
        tc_codigo = new TableColumn<>("CODIGO");
        tc_tipo = new TableColumn<>("TIPO");
        tc_precio_unit = new TableColumn<>("PRECIO UNIT");
        tc_imagen = new TableColumn<>("IMAGEN");
        tc_created = new TableColumn<>("CREATED");
        tc_creator = new TableColumn<>("CREATOR");
        tc_updated = new TableColumn<>("UPDATED");
        tc_updator = new TableColumn<>("UPDATER");
        
        tc_id.setCellValueFactory(new PropertyValueFactory<Producto, Integer>("id"));
        tc_nombre.setCellValueFactory(new PropertyValueFactory<Producto, String>("nombre"));
        tc_descripcion.setCellValueFactory(new PropertyValueFactory<Producto, String>("descripcion"));
        tc_codigo.setCellValueFactory(new PropertyValueFactory<Producto, String>("codigo"));
        tc_tipo.setCellValueFactory(new PropertyValueFactory<Producto, Integer>("tipo"));
        tc_precio_unit.setCellValueFactory(new PropertyValueFactory<Producto, Float>("precio_unit"));
        tc_imagen.setCellValueFactory(new PropertyValueFactory<Producto, String>("imagen"));
        tc_created.setCellValueFactory(new PropertyValueFactory<Producto, Date>("created"));
        tc_creator.setCellValueFactory(new PropertyValueFactory<Producto, Integer>("creator"));
        tc_updated.setCellValueFactory(new PropertyValueFactory<Producto, Date>("updated"));
        tc_updator.setCellValueFactory(new PropertyValueFactory<Producto, Integer>("updater"));
        tc_descripcion.setVisible(false);// Columna oculta por defecto
        tc_creator.setVisible(false);// Columna oculta por defecto
        tc_created.setVisible(false);// Columna oculta por defecto
        tc_updator.setVisible(false);// Columna oculta por defecto
        
        table_productos.getColumns().addAll(tc_id, tc_codigo, tc_nombre, tc_precio_unit,
                tc_imagen, tc_descripcion, tc_created, tc_creator, tc_updated, tc_updator);
        table_productos.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        ObservableList<ProductoTipo> ol_producto_tipos = FXCollections.observableArrayList(producto_tipos);
        cb_producto_tipo.setItems(ol_producto_tipos);
        cb_producto_tipo.getSelectionModel().select(0);
        
        if (open_on_edit){
            tp_productos.getSelectionModel().select(tab_lista_productos); 
        }  
    }    

    @Override
    public void handle(Event event) {
        try{
            Class target = event.getTarget().getClass();
            
            // Botones Aceptar, Cancelar
            if (target.equals(Button.class)){
                Button button = (Button) event.getSource();
                if (bt_aceptar.equals(button)){
                    if (this.producto_update == null){
                        createProducto();
                    }else{
                        updateProducto(this.producto_update);
                    }
                }
                
                if (bt_cancelar.equals(button)){
                    Stage stage = (Stage) button.getScene().getWindow();
                    stage.close();
                }
                
                if (bt_eliminar.equals(button)){
                    deleteProducto();
                }
                
                if (bt_editar.equals(button)){
                    resetProductoForm();
                    Producto producto = (Producto) table_productos.getSelectionModel().getSelectedItem();
                    if (producto != null){
                        this.producto_update = producto;
                        setProductoToForm(producto);
                        tp_productos.getSelectionModel().select(tab_nuevo_editar_producto);   
                    }
                }
                
                if (bt_reload.equals(button)){
                    reloadProductosTable();
                    ObservableList<Producto> ol_productos = FXCollections.observableArrayList(productos);
                    table_productos.setItems(ol_productos);
                }
                
                if (bt_reset_form.equals(button)){
                    resetProductoForm();
                }
                
                if (bt_codigo_producto_auto.equals(button)){
                    String codigo = calcularCodigoProducto();
                    if (codigo != ""){
                        tf_producto_codigo.setText(codigo);
                    }
                }
            }
            
            // PestaÃƒÆ’Ã‚Â±as (Tab)
            if (target.equals(Tab.class)){
                if (tab_nuevo_editar_producto.isSelected()){
                }
               
                if (tab_lista_productos.isSelected()){
                    ObservableList<Producto> ol_mesas = FXCollections.observableArrayList(productos);
                    table_productos.setItems(ol_mesas);
                }
            }
            
            // Imagen (ImageView)
            if (target.equals(ImageView.class)){
                ImageView imagen = (ImageView) event.getSource();
                
                if (iv_imagen.equals(imagen)){
                    selectImageProducto();
                }
            }
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Valida los campos del formulario Productos
     */
    private boolean validarProductosForm(){
        boolean is_valid = true;
        
        String nombre = tf_producto_nombre.getText();
        String codigo = tf_producto_codigo.getText();
        String descripcion = ta_producto_descripcion.getText();
        String precio_unit = tf_producto_precio_unit.getText();
        ProductoTipo producto_tipo = cb_producto_tipo.getSelectionModel().getSelectedItem();

        if (Validator.isEmpty(codigo)){
            t_text_info.setText("Codigo* es un campo obligatorio");
            tf_producto_codigo.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            tf_producto_codigo.setStyle("-fx-border-color: green;");
        }
        
        if (Validator.isEmpty(nombre)){
            t_text_info.setText("Nombre* es un campo obligatorio");
            tf_producto_nombre.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            tf_producto_nombre.setStyle("-fx-border-color: green;");
        }
        
        if (Validator.isNull(producto_tipo)){
            t_text_info.setText("Tipo Producto* es un campo obligatorio");
            tf_producto_codigo.setStyle("-fx-border-color: red;");
        }else{
            cb_producto_tipo.setStyle("-fx-border-color: green;");
        }
        
        if (!Validator.isNumber(precio_unit)){
            t_text_info.setText("Precio Unitario* es un campo obligatorio");
            tf_producto_precio_unit.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            tf_producto_precio_unit.setStyle("-fx-border-color: green;");        
        }
        
        if (Validator.isGreatherThan(descripcion, 250)){
            t_text_info.setText("DescripciÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â³n* no puede contener mÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¡s de 250 caracteres");
            ta_producto_descripcion.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            ta_producto_descripcion.setStyle("-fx-border-color: green;");        
        }
        
        t_text_info.setFill(Color.RED);
        
        if (is_valid){
            t_text_info.setText("");
        }
        
        return is_valid;
    }
    
    /**
     * Inicializa los campos del formulario con los valores de un producto
     * @param producto 
     */
    private void setProductoToForm(Producto producto){
        ProductoTipo producto_tipo = null;
        String save_image_format;
        BufferedImage bImage = null;
        File file = null;
        Image image = null;
        
        try{
            save_image_format = Config.getProperty("images_format");
            
            for(int i=0; i<producto_tipos.size(); i++){
                ProductoTipo pt = producto_tipos.get(i);
                if (pt.getId() == producto.getTipo()){
                    producto_tipo = pt;
                }
            }
            ObservableList<ProductoTipo> ol_producto_tipos = FXCollections.observableArrayList(producto_tipos);
            
            tf_producto_codigo.setText(producto.getCodigo());
            tf_producto_nombre.setText(producto.getNombre());
            tf_producto_precio_unit.setText(String.valueOf(producto.getPrecio_unit()));
            ta_producto_descripcion.setText(producto.getDescripcion());
            cb_producto_tipo.setItems(ol_producto_tipos);
            cb_producto_tipo.getSelectionModel().select(producto_tipo);
            
            file = new File(Config.getProperty("directory_images") + "/" + producto.getImagen());
            if (file != null){
                if (file.exists()){
                    bImage = ImageIO.read(file);
                    if (bImage != null){
                        image = SwingFXUtils.toFXImage(bImage, null);
                        if (image != null){
                            iv_imagen.setImage(image);
                        }
                    }   
                }
            }
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Devuelve el producto obtenido desde el formulario de productos
     * @return Producto producto
     */
    private Producto getProductoFromForm(){
        Producto producto = new Producto();
        
        ProductoTipo pt = null;
        Usuario usuario_sesion;
        Image image = null;
        
        try{        
            usuario_sesion = MainFormController.getUserSesion();
            producto.setCodigo(tf_producto_codigo.getText());
            producto.setNombre(tf_producto_nombre.getText());
            producto.setPrecio_unit(Float.parseFloat(tf_producto_precio_unit.getText().toString().replace(",", ".")));
            producto.setDescripcion(ta_producto_descripcion.getText());
            
            pt = cb_producto_tipo.getSelectionModel().getSelectedItem();
            if (pt != null){
                producto.setTipo(pt.getId());
            }
            
            if (this.image_file != null){
                producto.setImagen(this.image_file.getName().replaceFirst("[.][^.]+$", "") + Config.getProperty("images_format"));
            }else{
                producto.setImagen(this.producto_update.getImagen());
            }
        
            producto.setCreated(new Date());
            producto.setCreator(usuario_sesion.getId());
            producto.setUpdated(new Date());
            producto.setUpdater(usuario_sesion.getId());
            
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }

        return producto;
    }
     
    /**
     * Reinicializa el formulario de nuevo producto
     */
    private void resetProductoForm(){
        tf_producto_codigo.setText("");
        tf_producto_nombre.setText("");
        tf_producto_precio_unit.setText("");
        ta_producto_descripcion.setText("");
        cb_producto_tipo.getSelectionModel().select(0);
        iv_imagen.setImage(null);
        this.image_file = null;
        this.producto_update = null;
        t_text_info.setText("");
        
        tf_producto_codigo.setStyle(null);
        tf_producto_nombre.setStyle(null);
        tf_producto_precio_unit.setStyle(null);
        ta_producto_descripcion.setStyle(null);
        cb_producto_tipo.setStyle(null);
    }
    
    /**
     * Recarga el listado de productos en la tabla
     */
    private void reloadProductosTable(){
        try{
            this.productos = DaoFactory.getProductoDao().queryAll();
            cb_producto_tipo.setItems(FXCollections.observableArrayList(producto_tipos));
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        } 
    }
    
    /**
     * Selecciona una imagen para el producto
     */
    private void selectImageProducto(){
        FileChooser file_chooser = new FileChooser();
        File image_file = null;
             
        //Set extension filter
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        file_chooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);

        //Show open file dialog
        image_file = file_chooser.showOpenDialog(iv_imagen.getScene().getWindow());

        try {
            if (image_file != null){
                this.image_file = image_file;
                BufferedImage bufferedImage = ImageIO.read(image_file);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                iv_imagen.setImage(image);
            }
        } catch (IOException e) {
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Calcula el codigo del siguiente producto
     * @return 
     */
    public String calcularCodigoProducto(){
        String codigo = "";
        try{
            codigo = "P" + new SimpleDateFormat("yy").format(new Date()) + (1001 + productos.size());    
        }catch (Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText(null);
            alert.setContentText("Se produjo calculando el codigo de producto");
            alert.showAndWait();
        }
        
        return codigo;
    }
    
    // ========================================================================
    //                              CRUD FUNCTIONS
    // ========================================================================
    /**
     * AÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â±adir mesa a BD
     */
    private void createProducto(){
        Request request;
        Usuario usuario_sesion;
        String server_action = "create-producto";
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
        
            if (validarProductosForm()){
                Producto producto = getProductoFromForm();
//                producto.setId(producto.getId());
                producto.setCreated(new Date());
                producto.setCreator(usuario_sesion.getCreator());
                producto.setUpdated(new Date());
                producto.setUpdater(usuario_sesion.getId());
                               
                request.setAction(server_action);
                request.setData(producto);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(createProductoClientListener(this.image_file));
                this.client_tcp.sendTCP(request);
            }   
            
//            ByteArrayInputStream bais = getBytesFromImage(iv_imagen.getImage());
//            setProductoImage(bais);

        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Actualizar mesa de la BD
     */
    private void updateProducto(Producto producto){
        Request request;
        Usuario usuario_sesion;
        Producto producto_updated;
        String server_action = "update-producto";
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
            
            if (validarProductosForm()){
                producto_updated = getProductoFromForm();
                producto_updated.setId(producto.getId());
//                producto_updated.setCreated(producto.getCreated());
//                producto_updated.setCreator(producto.getCreator());
                producto_updated.setUpdated(new Date());
                producto_updated.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(producto_updated);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(updateProductoClientListener(this.image_file));
                this.client_tcp.sendTCP(request);
                
                this.producto_update = null;
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Elimina mesa del sistema
     */
    private void deleteProducto(){
        List<Producto> productos = table_productos.getSelectionModel().getSelectedItems();
        Producto producto;
        String server_action = "delete-producto";
        
        if (productos.size() == 1){
            producto = productos.get(0);
            
            alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Eliminar producto");
            alert.setHeaderText(null);
            alert.setContentText("Se va a proceder a eliminar el producto con ID ("+producto.getId()+"). Ã‚Â¿EstÃƒÂ¡ seguro?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                try{
                    Request request = new Request();
                    request.setAction(server_action);
                    request.setData(producto);

                    this.client_tcp = new Client();
                    this.client_tcp.start();
                    if (!this.client_tcp.isConnected()){
                        this.client_tcp.connect();    
                    }
                    this.client_tcp.addListener(deleteProductoClientListener());
                    this.client_tcp.sendTCP(request);
                }catch(Exception e){
                    MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            alert = new Alert(AlertType.ERROR);
                            alert.setTitle("ERROR");
                            alert.setHeaderText(null);
                            alert.setContentText("Ups! Parace que algo no saliÃƒÆ’Ã‚Â³ bien...");
                            alert.showAndWait();
                        }
                   });
                }
            }
        }
    }
    
    /**
     * Envia al servidor y define una imagen para un producto
     */
    private boolean sendProductoImage(File image_file, int server_port){
       
        boolean is_send = false;
        Socket client_socket;
        FileInputStream fis;
        BufferedInputStream bis;
        OutputStream os;
        byte[] mybytearray;
        
        try{
            
            client_socket = new Socket(Config.getProperty("server_ip_addr"), server_port);
            mybytearray = new byte[(int) image_file.length()];

            fis = new FileInputStream(image_file);
            bis = new BufferedInputStream(fis);
            
            bis.read(mybytearray, 0, mybytearray.length);
            os = client_socket.getOutputStream();
            os.write(mybytearray, 0, mybytearray.length);
            
            os.flush();
            os.close();
            client_socket.close();
            
            is_send = true;
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
            is_send = false;
        }
        
        return is_send;
    }
    
    // ========================================================================
    //                           CLIENT LISTENERS
    // ========================================================================
    public Listener createProductoClientListener(final File image){
        
        return new Listener(){
            public void received (Connection connection, Object object) {
              
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        if (response.isStatus() && !response.isError()){
                            reloadProductosTable();
                            if (image != null){
                                try{
                                    int server_port = Integer.parseInt(response.getMessage());
                                    final boolean is_send = sendProductoImage(image, server_port);
                                    
                                    Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (is_send){
                                                alert = new Alert(AlertType.INFORMATION);
                                                alert.setTitle("INFO");
                                                alert.setContentText(ResponseMessage.SUCCESS_OPERATION);
                                                resetProductoForm();
                                                main_controller.reloadData();
                                            }else{
                                                alert = new Alert(AlertType.ERROR);
                                                alert.setTitle("ERROR");
                                                alert.setContentText(ResponseMessage.ERROR_UPLOADING_FILE);
                                            }
                                            
                                            alert.setHeaderText(null);
                                            alert.showAndWait();
                                        }
                                   });
                                }catch(Exception e){
                                    MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                                }
                            }else{
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        resetProductoForm();

                                        alert = new Alert(AlertType.INFORMATION);
                                        alert.setTitle("INFO");
                                        alert.setContentText(ResponseMessage.SUCCESS_OPERATION);
                                        alert.setHeaderText(null);
                                        alert.showAndWait();
                                    }
                               });
                            }
                        }else{
                            alert = new Alert(AlertType.ERROR);
                            alert.setTitle("ERROR");
                            alert.setContentText(response.getMessage());
                            alert.setHeaderText(null);
                            alert.showAndWait();
                        }
                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener deleteProductoClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });
                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener updateProductoClientListener(final File image){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;
 
                        if (response.isStatus() && !response.isError()){
                            if (image != null){
                                try{
                                    int server_port = Integer.parseInt(response.getMessage());
                                    final boolean is_send = sendProductoImage(image, server_port);

                                    Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (is_send){
                                                alert = new Alert(AlertType.INFORMATION);
                                                alert.setTitle("INFO");
                                                alert.setContentText(ResponseMessage.SUCCESS_OPERATION);
                                                resetProductoForm();
                                            }else{
                                                alert = new Alert(AlertType.ERROR);
                                                alert.setTitle("ERROR");
                                                alert.setContentText(ResponseMessage.ERROR_UPLOADING_FILE);
                                            }

                                            alert.setHeaderText(null);
                                            alert.showAndWait();
                                        }
                                   });
                                }catch(Exception e){
                                    MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                                }
                                
                                reloadProductosTable();
                            }else{
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        resetProductoForm();

                                        alert = new Alert(AlertType.INFORMATION);
                                        alert.setTitle("INFO");
                                        alert.setContentText(ResponseMessage.SUCCESS_OPERATION);
                                        alert.setHeaderText(null);
                                        alert.showAndWait();
                                    }
                               });
                            }

                         }else{
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    resetProductoForm();

                                    alert = new Alert(AlertType.ERROR);
                                    alert.setTitle("ERROR");
                                    alert.setContentText(response.getMessage());
                                    alert.setHeaderText(null);
                                    alert.showAndWait();
                                }
                           });
                        }

                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
}
