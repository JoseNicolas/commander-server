package com.commander.server.controller;

import com.commander.server.Request;
import com.commander.server.Response;
import com.commander.server.dao.DaoFactory;
import com.commander.server.model.Usuario;
import com.commander.server.client.Client;
import com.commander.server.utils.Validator;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

/*
 * 2019/04/29
 * @author JosÃƒÂ© Francisco NicolÃƒÂ¡s Bautista
 * @version 1.0
 */
public class UsuarioFormController extends Listener implements Initializable, EventHandler {

    @FXML private PasswordField pf_usuario_password;
    @FXML private TextField tf_usuario_nombre;
    @FXML private RadioButton rb_usuario_hombre;
    @FXML private Button bt_aceptar;
    @FXML private Button bt_cancelar;
    @FXML private Button bt_eliminar;
    @FXML private Button bt_editar;
    @FXML private Button bt_reload;
    @FXML private Button bt_reset_form;
    @FXML private RadioButton rb_usuario_mujer;
    @FXML private TextField tf_usuario_apellido2;
    @FXML private TextField tf_usuario_usuario;
    @FXML private PasswordField pf_usuario_password_confirm;
    @FXML private TextField tf_usuario_apellido1;
    @FXML private Text t_text_info;
    @FXML private Tab tab_nuevo_editar_usuario;
    @FXML private Tab tab_lista_usuarios;
    @FXML private TabPane tp_usuarios;
    @FXML private TableView table_usuarios;
    ToggleGroup tgroup;
    TableColumn<Usuario, Integer> tc_id;
    TableColumn<Usuario, String> tc_usuario;
    TableColumn<Usuario, String> tc_nombre;
    TableColumn<Usuario, String> tc_apellido1;
    TableColumn<Usuario, String> tc_apellido2;
    TableColumn<Usuario, String> tc_sexo;
    TableColumn<Usuario, Date> tc_created;
    TableColumn<Usuario, Integer> tc_creator;
    TableColumn<Usuario, Date> tc_updated;
    TableColumn<Usuario, Integer> tc_updator;
    
    private Usuario usuario_update;
    private List<Usuario> usuarios;
    private Client client_tcp;
    private Alert alert;
    private boolean open_on_edit;

    public UsuarioFormController(boolean open_on_edit) throws Exception{
        this.usuario_update = null;
        this.client_tcp = new Client();
        this.usuarios = DaoFactory.getUsuarioDao().queryAll();
        this.open_on_edit = open_on_edit;
    }
    
    public UsuarioFormController(Usuario usuario) throws Exception{
        this.usuario_update = usuario;
        this.usuarios = DaoFactory.getUsuarioDao().queryAll();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Form events
        bt_aceptar.setOnAction(this);
        bt_cancelar.setOnAction(this);
        bt_editar.setOnAction(this);
        bt_eliminar.setOnAction(this);
        bt_reload.setOnAction(this);
        bt_reset_form.setOnAction(this);
        tab_nuevo_editar_usuario.setOnSelectionChanged(this);
        tab_lista_usuarios.setOnSelectionChanged(this);
        
        // Radiobutton group
        tgroup = new ToggleGroup();
        rb_usuario_hombre.setToggleGroup(tgroup);
        rb_usuario_mujer.setToggleGroup(tgroup);
        
//        // Definimos los valores del usuario en el formulario
//        if (this.usuario_update != null){
//            setUsuarioToForm(this.usuario_update);
//        }
        
        tc_id = new TableColumn<>("ID");
        tc_usuario = new TableColumn<>("USUARIO");
        tc_nombre = new TableColumn<>("NOMBRE");
        tc_apellido1 = new TableColumn<>("APELIDO 1");
        tc_apellido2 = new TableColumn<>("APELLIDO 2");
        tc_sexo = new TableColumn<>("SEXO");
        tc_created = new TableColumn<>("CREATED");
        tc_creator = new TableColumn<>("CREATOR");
        tc_updated = new TableColumn<>("UPDATED");
        tc_updator = new TableColumn<>("UPDATER");
        
        tc_id.setCellValueFactory(new PropertyValueFactory<Usuario, Integer>("id"));
        tc_usuario.setCellValueFactory(new PropertyValueFactory<Usuario, String>("usuario"));
        tc_nombre.setCellValueFactory(new PropertyValueFactory<Usuario, String>("nombre"));
        tc_apellido1.setCellValueFactory(new PropertyValueFactory<Usuario, String>("apellido1"));
        tc_apellido2.setCellValueFactory(new PropertyValueFactory<Usuario, String>("apellido2"));
        tc_sexo.setCellValueFactory(new PropertyValueFactory<Usuario, String>("sexo"));
        tc_created.setCellValueFactory(new PropertyValueFactory<Usuario, Date>("created"));
        tc_creator.setCellValueFactory(new PropertyValueFactory<Usuario, Integer>("creator"));
        tc_updated.setCellValueFactory(new PropertyValueFactory<Usuario, Date>("updated"));
        tc_updator.setCellValueFactory(new PropertyValueFactory<Usuario, Integer>("updater"));
        tc_creator.setVisible(false);// Columna oculta por defecto
        tc_created.setVisible(false);// Columna oculta por defecto
        tc_updator.setVisible(false);// Columna oculta por defecto
        
        table_usuarios.getColumns().addAll(tc_id, tc_nombre, tc_apellido1, 
                tc_apellido2, tc_sexo, tc_usuario, tc_created,
                tc_creator, tc_updated, tc_updator);
//        table_usuarios.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table_usuarios.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        if (open_on_edit){
            tp_usuarios.getSelectionModel().select(tab_lista_usuarios); 
        }  
    }    
 
    /**
     * Implement handle event from Interface EventHandler
     * @param event 
     */
    @Override
    public void handle(Event event) {
        try{
            Class target = event.getTarget().getClass();
            
            // Botones Aceptar, Cancelar
            if (target.equals(Button.class)){
                Button button = (Button) event.getSource();
                if (bt_aceptar.equals(button)){
                    if (this.usuario_update == null){
                        createUsuario();   
                    }else{
                        updateUsuario(this.usuario_update);
                    }
                }
                
                if (bt_cancelar.equals(button)){
                    Stage stage = (Stage) button.getScene().getWindow();
                    stage.close();
                }
                
                if (bt_eliminar.equals(button)){
                    deleteUsuario();
                }
                
                if (bt_editar.equals(button)){
                    Usuario usuario = (Usuario) table_usuarios.getSelectionModel().getSelectedItem();
                    if (usuario != null){
                       this.usuario_update = usuario;
                       setUsuarioToForm(usuario);
                       tp_usuarios.getSelectionModel().select(tab_nuevo_editar_usuario);   
                    }
                }
                
                if (bt_reload.equals(button)){
                    reloadUsuariosTable();
                    ObservableList<Usuario> ol_usuarios = FXCollections.observableArrayList(usuarios);
                    table_usuarios.setItems(ol_usuarios);
                }
                
                if (bt_reset_form.equals(button)){
                    resetUsuarioForm();
                }
            }
            
            // PestaÃƒÂ±as (Tab)
            if (target.equals(Tab.class)){
                if (tab_nuevo_editar_usuario.isSelected()){
                }
               
                if (tab_lista_usuarios.isSelected()){
                    ObservableList<Usuario> ol_usuarios = FXCollections.observableArrayList(usuarios);
                    table_usuarios.setItems(ol_usuarios);
                }
            }  
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Valida los campos del formulario Usuarios
     */
    private boolean validarUsuariosForm(){
        boolean is_valid = true;
        
        String nombre = tf_usuario_nombre.getText();
        String apellido1 = tf_usuario_apellido1.getText();
        String apellido2 = tf_usuario_apellido2.getText();
        String usuario = tf_usuario_usuario.getText();
        String password = pf_usuario_password.getText();
        String password_confirm = pf_usuario_password_confirm.getText();

        if (Validator.isEmpty(nombre)){
            t_text_info.setText("Nombre* es un campo obligatorio");
            tf_usuario_nombre.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            tf_usuario_nombre.setStyle("-fx-border-color: green;");
        }
        
        if (Validator.isEmpty(usuario)){
            t_text_info.setText("Usuario* es un campo obligatorio");
            tf_usuario_usuario.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            tf_usuario_usuario.setStyle("-fx-border-color: green;");        
        }
               
        if (usuario_update != null){
            if (!Validator.isEmpty(password) || !Validator.isEmpty(password_confirm)){
                if (!password.equals(password_confirm)){
                   t_text_info.setText("ContraseÃ±a y Confirmar ContraseÃ±a deben coincidir");
                   pf_usuario_password.setStyle("-fx-border-color: red;");
                   pf_usuario_password_confirm.setStyle("-fx-border-color: red;");
                   is_valid = false;
                }else{
                   pf_usuario_password.setStyle("-fx-border-color: green;");
                   pf_usuario_password_confirm.setStyle("-fx-border-color: green;");   
                }
            }  
        }else{
            if (!password.equals(password_confirm)){
                   t_text_info.setText("ContraseÃ±a y Confirmar ContraseÃ±a deben coincidir");
                   pf_usuario_password.setStyle("-fx-border-color: red;");
                   pf_usuario_password_confirm.setStyle("-fx-border-color: red;");
                   is_valid = false;
                }else{
                   pf_usuario_password.setStyle("-fx-border-color: green;");
                   pf_usuario_password_confirm.setStyle("-fx-border-color: green;");   
                }
        }
        
        t_text_info.setFill(Color.RED);
        
        if (is_valid){
            t_text_info.setText("");
        }
        
        return is_valid;
    }
    
    /**
     * Inicializa los campos del formulario con los valores de un usuario
     * @param usuario 
     */
    private void setUsuarioToForm(Usuario usuario){
        try{
            tf_usuario_nombre.setText(usuario.getNombre());
            tf_usuario_apellido1.setText(usuario.getApellido1());
            tf_usuario_apellido2.setText(usuario.getApellido2());
            tf_usuario_usuario.setText(usuario.getUsuario());
            if (usuario.getSexo().equals("hombre")){
                rb_usuario_hombre.setSelected(true);
            }else{
                rb_usuario_mujer.setSelected(true);
            }
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Devuelve el usuario obtenido desde el formulario de usuarios
     * @return Usuario usuario
     */
    private Usuario getUsuarioFromForm(){
        Usuario usuario = new Usuario();
        
        try{
            RadioButton radio_selected = (RadioButton)tgroup.getSelectedToggle();
        
            usuario.setNombre(tf_usuario_nombre.getText());
            usuario.setApellido1(tf_usuario_apellido1.getText());
            usuario.setApellido2(tf_usuario_apellido2.getText());
            usuario.setUsuario(tf_usuario_usuario.getText());
            usuario.setPassword(pf_usuario_password.getText());
            usuario.setSexo(tf_usuario_nombre.getText());
            usuario.setSexo(radio_selected.getText().toLowerCase(Locale.getDefault()));
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }

        return usuario;
    }
    
    /**
     * Reinicializa el formulario de nuevo usuario
     */
    private void resetUsuarioForm(){
        tf_usuario_nombre.setText("");
        tf_usuario_apellido1.setText("");
        tf_usuario_apellido2.setText("");
        rb_usuario_hombre.setSelected(true);
        rb_usuario_mujer.setSelected(false);
        tf_usuario_usuario.setText("");
        pf_usuario_password.setText("");
        pf_usuario_password_confirm.setText("");
        t_text_info.setText("");
        
        tf_usuario_nombre.setStyle(null);
        tf_usuario_apellido1.setStyle(null);
        tf_usuario_apellido2.setStyle(null);
        rb_usuario_hombre.setStyle(null);
        rb_usuario_mujer.setStyle(null);
        tf_usuario_usuario.setStyle(null);
        pf_usuario_password.setStyle(null);
        pf_usuario_password_confirm.setStyle(null);
    }
    
    /**
     * Recarga el listado de usuarios en la tabla
     */
    private void reloadUsuariosTable(){
        try{
            this.usuarios = DaoFactory.getUsuarioDao().queryAll();    
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        } 
    }
    
    // ========================================================================
    //                              CRUD FUNCTIONS
    // ========================================================================
    /**
     * AÃ±adir usuario a BD
     */
    private void createUsuario(){
        Request request;
        Usuario usuario_sesion;
        String server_action = "create-usuario";
        
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
        
            if (validarUsuariosForm()){
                Usuario usuario = getUsuarioFromForm();
                usuario.setCreated(new Date());
                usuario.setCreator(usuario_sesion.getId());
                usuario.setUpdated(new Date());
                usuario.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(usuario);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(createUsuarioClientListener());
                this.client_tcp.sendTCP(request);
            }   
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Actualizar usuario de la BD
     */
    private void updateUsuario(Usuario usuario){
        Request request;
        Usuario usuario_sesion;
        Usuario usuario_updated;
        String server_action = "update-usuario";
        
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
            usuario_updated = getUsuarioFromForm();
            
            if (validarUsuariosForm()){
                usuario_updated.setId(usuario.getId());
//                usuario_updated.setCreated(usuario.getCreated());
//                usuario_updated.setCreator(usuario.getCreator());
                usuario_updated.setUpdated(new Date());
                usuario_updated.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(usuario_updated);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(updateUsuarioClientListener());
                this.client_tcp.sendTCP(request);
                
                this.usuario_update = null;
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Elimina uno o varios usuarios del sistema
     */
    private void deleteUsuario(){
        List<Usuario> usuarios = table_usuarios.getSelectionModel().getSelectedItems();
        Usuario usuario;
        String server_action = "delete-usuario";
        
        if (usuarios.size() == 1){
            usuario = usuarios.get(0);
            
            alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Eliminar usuario");
            alert.setHeaderText(null);
            alert.setContentText("Se va a proceder a eliminar el usuario con ID ("+usuario.getId()+"). Ã‚Â¿EstÃƒÂ¡ seguro?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                try{
                    Request request = new Request();
                    request.setAction(server_action);
                    request.setData(usuario);

                    this.client_tcp = new Client();
                    this.client_tcp.start();
                    if (!this.client_tcp.isConnected()){
                        this.client_tcp.connect();    
                    }
                    this.client_tcp.addListener(deleteUsuarioClientListener());
                    this.client_tcp.sendTCP(request);
                }catch(Exception e){
                    MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            alert = new Alert(AlertType.ERROR);
                            alert.setTitle("ERROR");
                            alert.setHeaderText(null);
                            alert.setContentText("Ups! Parace que algo no salÃƒÂ­o bien...");
                            alert.showAndWait();
                        }
                   });
                }
            }
        }
    }
    
    // ========================================================================
    //                              TCP CLIENT
    // ========================================================================

//    @Override
//    public void disconnected(Connection cnctn) {
//        super.disconnected(cnctn); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void connected(Connection cnctn) {
//        super.connected(cnctn); //To change body of generated methods, choose Tools | Templates.
//    }
 
    // ========================================================================
    //                           CLIENT LISTENERS
    // ========================================================================
    public Listener createUsuarioClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");

                                     resetUsuarioForm();
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });

                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener deleteUsuarioClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                reloadUsuariosTable();
                                alert.setHeaderText(null);
                                alert.setContentText(response.getMessage());
                                alert.showAndWait();
                             }
                        });

                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener updateUsuarioClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");

                                     resetUsuarioForm();
                                     reloadUsuariosTable();
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });

                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
}
