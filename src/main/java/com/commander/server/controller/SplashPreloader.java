package com.commander.server.controller;

import com.commander.server.utils.Config;
import javafx.application.Preloader;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * 2019/04/29
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class SplashPreloader extends Preloader{

    private Stage preloaderStage;
    private Scene scene;
    
    @Override
    public void init() throws Exception {
        Parent root1 = FXMLLoader.load(getClass().getResource("/fxml/FXMLSplashForm.fxml")); 
        scene = new Scene(root1); 
    } 
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.preloaderStage = primaryStage;
        // preloaderStage.initStyle(StageStyle.TRANSPARENT); 
        // Set preloader scene and show stage.
        preloaderStage.setScene(scene);  
        preloaderStage.initStyle(StageStyle.UNDECORATED);
        preloaderStage.getIcons().add(new Image(Config.getProperty("icon")));
        preloaderStage.show();
    }
    
        @Override
    public void handleApplicationNotification(Preloader.PreloaderNotification info) {
      
          if (info instanceof ProgressNotification) {
            SplashFormController.label.setText("Loading "+((ProgressNotification) info).getProgress()*100 + "%");
//            System.out.println("Value@ :" + ((ProgressNotification) info).getProgress());
            SplashFormController.statProgressBar.setProgress(((ProgressNotification) info).getProgress());
        }
    }

    @Override
    public void handleStateChangeNotification(Preloader.StateChangeNotification info) {
      
        StateChangeNotification.Type type = info.getType();
        switch (type) {
            
            case BEFORE_START:
                // Called after MyApplication#init and before MyApplication#start is called.
//                System.out.println("BEFORE_START");
                preloaderStage.hide();
                break;
        }
        
        
    }
}
