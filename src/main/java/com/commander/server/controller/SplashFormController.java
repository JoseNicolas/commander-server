/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commander.server.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Preloader;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/*
 * 2019/04/29
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class SplashFormController implements Initializable {
//
    @FXML private Label progress;
    @FXML private ProgressBar progressBar;
    
    public static ProgressBar statProgressBar;
    public static Label label;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        label = progress ;
        statProgressBar = progressBar;
    }    
}
