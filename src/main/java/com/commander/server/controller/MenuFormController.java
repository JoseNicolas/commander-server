package com.commander.server.controller;

import com.commander.server.Request;
import com.commander.server.Response;
import com.commander.server.client.Client;
import com.commander.server.dao.DaoFactory;
import com.commander.server.model.DetalleMenu;
import com.commander.server.model.Menu;
import com.commander.server.model.Producto;
import com.commander.server.model.Usuario;
import com.commander.server.utils.Validator;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.sun.javafx.scene.control.skin.LabeledText;
import com.sun.javafx.scene.control.skin.ListViewSkin;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.commons.lang3.exception.ExceptionUtils;

/*
 * 2019/05/14
 * @author JosÃƒÂ© Francisco NicolÃƒÂ¡s Bautista
 * @version 1.0
*/
public class MenuFormController implements Initializable, EventHandler {

    @FXML private Button bt_eliminar;
    @FXML private TextField tf_menu_nombre;
    @FXML private Text t_text_info;
    @FXML private Button bt_reset_form;
    @FXML private Button bt_reload;
    @FXML private Button bt_aceptar;
    @FXML private Button bt_add_producto;
    @FXML private Button bt_sub_producto;
    @FXML private Tab tab_nuevo_editar_menu;
    @FXML private TextArea ta_mesa_descripcion;
    @FXML private Button bt_cancelar;
    @FXML private TableView<Menu> table_menus;
    @FXML private TabPane tp_menu;
    @FXML private Tab tab_lista_menus;
    @FXML private ListView<String> lv_menu_platos;
    @FXML private ListView<Producto> lv_menu_productos;
    @FXML private Button bt_editar;
    TableColumn<Menu, Integer> tc_id;
    TableColumn<Menu, String> tc_nombre;
    TableColumn<Menu, String> tc_descripcion;
    TableColumn<Menu, Date> tc_created;
    TableColumn<Menu, Integer> tc_creator;
    TableColumn<Menu, Date> tc_updated;
    TableColumn<Menu, Integer> tc_updator;
    
    private Menu menu_update;
    private List<DetalleMenu> detalle_menu_update;
    private List<DetalleMenu> detalle_menu_new;
    private List<Menu> menus;
    private List<Producto> productos;
    private Client client_tcp;
    private Alert alert;
    private boolean open_on_edit;
    
    public MenuFormController(boolean open_on_edit) throws Exception{
        this.menu_update = null;
        this.detalle_menu_update = null;
        this.detalle_menu_new = null;
        this.menus = DaoFactory.getMenuDao().queryAll();
        this.productos = DaoFactory.getProductoDao().queryAll();
        this.open_on_edit = open_on_edit;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Form Events
        bt_aceptar.setOnAction(this);
        bt_cancelar.setOnAction(this);
        bt_editar.setOnAction(this);
        bt_eliminar.setOnAction(this);
        bt_reload.setOnAction(this);
        bt_reset_form.setOnAction(this);
        bt_add_producto.setOnAction(this);
        bt_sub_producto.setOnAction(this);
        tab_nuevo_editar_menu.setOnSelectionChanged(this);
        tab_lista_menus.setOnSelectionChanged(this);
        lv_menu_platos.setOnMouseClicked(this);
        
        tc_id = new TableColumn<>("ID");
        tc_nombre = new TableColumn<>("NOMBRE");
        tc_descripcion = new TableColumn<>("DESCRIPCION");
        tc_created = new TableColumn<>("CREATED");
        tc_creator = new TableColumn<>("CREATOR");
        tc_updated = new TableColumn<>("UPDATED");
        tc_updator = new TableColumn<>("UPDATER");
        
        tc_id.setCellValueFactory(new PropertyValueFactory<Menu, Integer>("id"));
        tc_nombre.setCellValueFactory(new PropertyValueFactory<Menu, String>("nombre"));
        tc_descripcion.setCellValueFactory(new PropertyValueFactory<Menu, String>("descripcion"));
        tc_created.setCellValueFactory(new PropertyValueFactory<Menu, Date>("created"));
        tc_creator.setCellValueFactory(new PropertyValueFactory<Menu, Integer>("creator"));
        tc_updated.setCellValueFactory(new PropertyValueFactory<Menu, Date>("updated"));
        tc_updator.setCellValueFactory(new PropertyValueFactory<Menu, Integer>("updater"));
        tc_creator.setVisible(false);// Columna oculta por defecto
        tc_created.setVisible(false);// Columna oculta por defecto
        tc_updator.setVisible(false);// Columna oculta por defecto
        
        table_menus.getColumns().addAll(tc_id, tc_nombre, tc_descripcion, tc_created,
                tc_creator, tc_updated, tc_updator);
        table_menus.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        String[] platos = {"Entrantes", "1er Plato", "2do Plato", "Postre", "Bebida"};
        ObservableList ob_platos = FXCollections.observableArrayList(platos);
        lv_menu_platos.setItems(ob_platos);
        
        if (open_on_edit){
            tp_menu.getSelectionModel().select(tab_lista_menus); 
        }  
    }    

    @Override
    public void handle(Event event) {
        try{
            Class target = event.getTarget().getClass();
            
            // Botones Aceptar, Cancelar
            if (target.equals(Button.class)){
                Button button = (Button) event.getSource();
                if (bt_aceptar.equals(button)){
                    if (this.menu_update == null){
                        createMenu();   
                    }else{
                        updateMenu(this.menu_update);
                    }
                }
                
                if (bt_cancelar.equals(button)){
                    Stage stage = (Stage) button.getScene().getWindow();
                    stage.close();
                }
                
                if (bt_eliminar.equals(button)){
                    deleteMenu();
                }
                
                if (bt_editar.equals(button)){
                    Menu menu = (Menu) table_menus.getSelectionModel().getSelectedItem();
                    List<DetalleMenu> detalle_menu = DaoFactory.getDetalleMenuDao().queryByMenu(menu);
                    if (menu != null){
                        if (detalle_menu != null){
                            this.menu_update = menu;
                            this.detalle_menu_update = detalle_menu;
                            this.detalle_menu_new = null;
                            setMenuToForm(menu, detalle_menu);   
                            tp_menu.getSelectionModel().select(tab_nuevo_editar_menu);
                        }
                    }
                }
                
                if (bt_reload.equals(button)){
                    reloadMenusTable();
                    ObservableList<Menu> ol_menus = FXCollections.observableArrayList(menus);
                    table_menus.setItems(ol_menus);
                }
                
                if (bt_reset_form.equals(button)){
                    resetMenuForm();
                }
                
                if (bt_add_producto.equals(button)){
                    
                    addDetalleMenu();
                }
                
                if (bt_sub_producto.equals(button)){
                    subDetalleMenu();
                }
            }
            
            // PestaÃƒÂ±as (Tab)
            if (target.equals(Tab.class)){
                if (tab_nuevo_editar_menu.isSelected()){
                }
               
                if (tab_lista_menus.isSelected()){
                    ObservableList<Menu> ol_menus = FXCollections.observableArrayList(menus);
                    table_menus.setItems(ol_menus);
                }
            }
            
            // ListView
            if (target.equals(ListViewSkin.class) || target.equals(LabeledText.class)){
                ListView listview = (ListView) event.getSource();
                if (lv_menu_platos.equals(listview)){
                    int selected = lv_menu_platos.getSelectionModel().getSelectedIndex();
                    selected = (selected-1); // Transformamos el indice en el tipo de plato
                    if (detalle_menu_update != null){
                        setDetalleMenuToForm(detalle_menu_update, selected);    
                    }else{
                        setDetalleMenuToForm(detalle_menu_new, selected);
                    }
                    
                }
            }
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Valida los campos del formulario Menus
     */
    private boolean validarMenusForm(){
        boolean is_valid = true;
        
        String nombre = tf_menu_nombre.getText();
        String descripcion = ta_mesa_descripcion.getText();
       
        if (Validator.isEmpty(nombre)){
            t_text_info.setText("Nombre* es un campo obligatorio");
            tf_menu_nombre.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            tf_menu_nombre.setStyle("-fx-border-color: green;");
        }
        
        if (this.menu_update != null){
            if (Validator.isNull(this.detalle_menu_update)){
                t_text_info.setText("Es necesario añadir productos al menu");
                lv_menu_productos.setStyle("-fx-border-color: red;");
                is_valid = false;
            }else{
                lv_menu_productos.setStyle("-fx-border-color: green;");        
            } 
        }else{
            if (Validator.isNull(this.detalle_menu_new)){
                t_text_info.setText("Es necesario añadir productos al menu");
                lv_menu_productos.setStyle("-fx-border-color: red;");
                is_valid = false;
            }else{
                lv_menu_productos.setStyle("-fx-border-color: green;");        
            } 
        }
        
        t_text_info.setFill(Color.RED);
        
        if (is_valid){
            t_text_info.setText("");
        }
        
        return is_valid;
    }
    
    /**
     * Inicializa los campos del formulario con los valores de un menu
     * @param menu 
     */
    private void setMenuToForm(Menu menu, List<DetalleMenu> detalle_menus){
        List<DetalleMenu> detalle_menu_entrantes;
        
        try{
            tf_menu_nombre.setText(menu.getNombre());
            ta_mesa_descripcion.setText(menu.getDescripcion());
            
            lv_menu_platos.getSelectionModel().select(0);
            setDetalleMenuToForm(detalle_menus, 0);
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Inicializa los campos del formulario con los valores de un detalle menu
     * @param detalle_menu
     * @param plato_a_mostrar 
     */
    private void setDetalleMenuToForm(List<DetalleMenu> detalle_menu, int plato_a_mostrar){
        List<DetalleMenu> detalle_menu_plato = new ArrayList();
        lv_menu_productos.getItems().clear();
        
        try{
           if (detalle_menu != null){
              if (detalle_menu.size() > 0){
                // Obtenemos lista de detalle menu del plato que queremos mostrar
                detalle_menu_plato = detalle_menu.stream()
                        .filter(dm -> dm.getPlato() == plato_a_mostrar)
                        .collect(Collectors.toList());

                if (detalle_menu_plato != null){
                    if (detalle_menu_plato.size() > 0){
                        List<Producto> productos_plato = new ArrayList();
                        for (int i=0; i<detalle_menu_plato.size(); i++){
                            DetalleMenu dm = detalle_menu_plato.get(i);

                            Producto p = DaoFactory.getProductoDao().load(dm.getProducto());
                            if (p != null){
                                productos_plato.add(p);
                            }
                        }

                        ObservableList ol_productos_entrantes = FXCollections.observableArrayList(productos_plato);
                        lv_menu_productos.setItems(ol_productos_entrantes);
                    }
                }
            }
        }
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Devuelve el menu obtenido desde el formulario de menus
     * @return Menu
     */
    private Menu getMenuFromForm(){
        Menu menu = new Menu();
        
        try{
            menu.setNombre(tf_menu_nombre.getText());
            menu.setDescripcion(ta_mesa_descripcion.getText());
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }

        return menu;
    }
    
    /**
     * Devuelve el detalle_menu obtenido desde el formulario de menus
     * @return DetalleMenu
     */
    private List<DetalleMenu> getDetallesMenuFromForm(){
        List<DetalleMenu> detalle_menu = null;
        
        if (this.detalle_menu_update != null){
            detalle_menu = this.detalle_menu_update;
        }
        if (this.detalle_menu_new != null){
            detalle_menu = this.detalle_menu_new;
        }
        
        return detalle_menu;
    }
    
    /**
     * Reinicializa el formulario de nuevo menu
     */
    private void resetMenuForm(){
        tf_menu_nombre.setText("");
        ta_mesa_descripcion.setText("");
        t_text_info.setText("");
        
        tf_menu_nombre.setStyle(null);
        ta_mesa_descripcion.setStyle(null);
        lv_menu_productos.setStyle(null);
        
        this.menu_update = null;
        this.detalle_menu_update = null;
        this.detalle_menu_new = null;
        
        
        lv_menu_productos.getItems().clear();
        lv_menu_productos.refresh();
        
    }
    
    /**
     * Recarga el listado de menus en la tabla
     */
    private void reloadMenusTable(){
        try{
            this.menus = DaoFactory.getMenuDao().queryAll();    
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        } 
    }
    
    /**
     * Añade un nuevo detalle de menu a un menu
     * @param producto
     * @param plato 
     */
    private void addDetalleMenu(){
        if (lv_menu_platos.getSelectionModel().getSelectedItem() != null){
            int plato = lv_menu_platos.getSelectionModel().getSelectedIndex();
            plato = (plato-1);
            if (this.detalle_menu_update == null){
                if (this.detalle_menu_new == null){
                    this.detalle_menu_new = new ArrayList();    
                }
                chooseProduct(this.detalle_menu_new, plato);
            }else{
                chooseProduct(this.detalle_menu_update, plato);
            }
        }
    }
    
    /**
     * Elimina un detalle de menu de un menu
     * @param producto
     * @param plato 
     */
    private void subDetalleMenu(){
        
        if (lv_menu_platos.getSelectionModel().getSelectedItem() != null){
            int plato = lv_menu_platos.getSelectionModel().getSelectedIndex();
            plato = (plato-1);
            
            List<DetalleMenu> detalle_menu = new ArrayList();
            
            if (this.detalle_menu_new != null){
                detalle_menu = this.detalle_menu_new;
            }
            if (this.detalle_menu_update != null){
                detalle_menu = this.detalle_menu_update;
            }
            
            Producto produco_selected = lv_menu_productos.getSelectionModel().getSelectedItem();
            if (produco_selected != null){
                lv_menu_productos.getItems().remove(produco_selected);
                for (int i=0; i<detalle_menu.size(); i++){
                    DetalleMenu dm = detalle_menu.get(i);
                    if (dm.getProducto() == produco_selected.getId()){
                        detalle_menu.remove(dm);
                    }
                }
            }
        }
    }
    
    /**
     * 
     */
    private void chooseProduct(List<DetalleMenu> detalle_menu, int plato){
        Stage modal;
        List<Producto> productos;
        
        
        try{
            modal = new Stage();
            productos = DaoFactory.getProductoDao().queryAll();
            
            final Label label = new Label("Producto: ");
            final Button bt_add = new Button("Añadir a menú");
            final ListView<Producto> listView = new ListView<Producto>();
            
            ObservableList<Producto> ol_productos =FXCollections.observableArrayList(productos);
            listView.setItems(ol_productos);

            listView.setOnMouseClicked(new EventHandler<MouseEvent>(){
                @Override
                public void handle(MouseEvent arg0) {

                    label.setText("Producto: " + listView.getSelectionModel().getSelectedItems());
                }
            });
            
            bt_add.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    Producto producto = listView.getSelectionModel().getSelectedItem();
                    if (detalle_menu != null) {
                        // Comprobamos que el producto no exista ya en la lista
                        boolean exist = false;
                        for (int i=0; i<detalle_menu.size(); i++){
                            DetalleMenu dm = detalle_menu.get(i);
                            if (dm.getProducto() == producto.getId()){
                                exist = true;
                            }
                        }
                        if (!exist){
                            DetalleMenu nuevo_dm = new DetalleMenu();
                            nuevo_dm.setProducto(producto.getId());
                            nuevo_dm.setPlato(plato);
                            detalle_menu.add(nuevo_dm);   
                            lv_menu_productos.getItems().add(producto);
                        }
                    }
                    modal.close();
                }
            });

            VBox vBox = new VBox();
            vBox.getChildren().addAll(label, listView, bt_add);

            StackPane root = new StackPane();
            root.getChildren().add(vBox);
            modal.setScene(new Scene(root, 300, 250));
            modal.show();

        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    // ========================================================================
    //                              CRUD FUNCTIONS
    // ========================================================================
    /**
     * AÃ±adir menu a BD
     */
    private void createMenu(){
        Request request;
        Usuario usuario_sesion;
        String server_action = "create-menu";
        
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
        
            if (validarMenusForm()){
                Menu menu = getMenuFromForm();
                menu.setCreated(new Date());
                menu.setCreator(usuario_sesion.getId());
                menu.setUpdated(new Date());
                menu.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(menu);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(createMenuClientListener());
                this.client_tcp.sendTCP(request);
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Actualizar menu de la BD
     */
    private void updateMenu(Menu menu){
        Request request;
        Usuario usuario_sesion;
        Menu menu_updated;
        String server_action = "update-menu";
        
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
            menu_updated = getMenuFromForm();
            
            if (validarMenusForm()){
                menu_updated.setId(menu.getId());
//                menu_updated.setCreated(new Date());
//                menu_updated.setCreator(menu.getCreator());
                menu_updated.setUpdated(new Date());
                menu_updated.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(menu_updated);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(updateMenuClientListener());
                this.client_tcp.sendTCP(request);
                
//                this.menu_update = null;
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Elimina un menu del sistema
     */
    private void deleteMenu(){
        List<Menu> menus = table_menus.getSelectionModel().getSelectedItems();
        Menu menu;
        String server_action = "delete-menu";
        
        if (menus.size() == 1){
            menu = menus.get(0);
            
            alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Eliminar menu");
            alert.setHeaderText(null);
            alert.setContentText("Se va a proceder a eliminar el menu con ID ("+menu.getId()+"). Ã‚Â¿EstÃƒÂ¡ seguro?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                try{
                    Request request = new Request();
                    request.setAction(server_action);
                    request.setData(menu);

                    this.client_tcp = new Client();
                    this.client_tcp.start();
                    if (!this.client_tcp.isConnected()){
                        this.client_tcp.connect();    
                    }
                    this.client_tcp.addListener(deleteMenuClientListener());
                    this.client_tcp.sendTCP(request);
                }catch(Exception e){
                    MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            alert = new Alert(AlertType.ERROR);
                            alert.setTitle("ERROR");
                            alert.setHeaderText(null);
                            alert.setContentText("Ups! Parace que algo no salÃƒÂ­o bien...");
                            alert.showAndWait();
                        }
                   });
                }
            }
        }
    }
    
    /**
     * AÃ±ade una lista de detalle de menus a BD
     */
    private void createDetalleMenus(List<DetalleMenu> detalle_menus, Menu menu){
        Request request;
        Usuario usuario_sesion;
        String server_action = "create-detalle_menu";
        
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
        
            if (validarMenusForm()){

                for (int i=0; i<detalle_menus.size(); i++){
                    DetalleMenu dm = detalle_menus.get(i);
                    dm.setCreated(new Date());
                    dm.setCreator(usuario_sesion.getId());
                    dm.setUpdated(new Date());
                    dm.setUpdater(usuario_sesion.getId());
                }

                request.setAction(server_action);
                request.setData(detalle_menus);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(createDetalleMenusClientListener(menu));
                this.client_tcp.sendTCP(request);
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    // ========================================================================
    //                           CLIENT LISTENERS
    // ========================================================================
    public Listener createMenuClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        if (response.isStatus() && !response.isError()){
                            Menu menu = (Menu) response.getData();
                            List<DetalleMenu> detalle_menus = getDetallesMenuFromForm();
                            
                            for (int i=0; i<detalle_menus.size(); i++){
                                DetalleMenu dm = detalle_menus.get(i);
                                dm.setMenu(menu.getId());
                            }
                            
                            createDetalleMenus(detalle_menus, menu);
                        }else{
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    alert = new Alert(AlertType.ERROR);
                                    alert.setTitle("ERROR");
                                    alert.setHeaderText(null);
                                    alert.setContentText(response.getMessage());
                                    alert.showAndWait();
                                }
                            });
                        }
                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener deleteMenuClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });

                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener updateMenuClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        if (response.isStatus() && !response.isError()){
                            Menu menu = (Menu) response.getData();
                            List<DetalleMenu> detalle_menus = getDetallesMenuFromForm();
                            
                            for (int i=0; i<detalle_menus.size(); i++){
                                DetalleMenu dm = detalle_menus.get(i);
                                dm.setMenu(menu.getId());
                            }
                            
                            DaoFactory.getDetalleMenuDao().deleteByIdMenu(menu.getId());
                            createDetalleMenus(detalle_menus, menu);
                        }else{
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    alert = new Alert(AlertType.ERROR);
                                    alert.setTitle("ERROR");
                                    alert.setHeaderText(null);
                                    alert.setContentText(response.getMessage());
                                    alert.showAndWait();
                                }
                            });
                        }
                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener createDetalleMenusClientListener(Menu menu){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                final Response response = (Response)object;

                                if (response.isStatus() && !response.isError()){
                                    detalle_menu_new = null;
                                    detalle_menu_update = null;
                                    menu_update = null;
                                    
                                    alert = new Alert(AlertType.INFORMATION);
                                    alert.setTitle("INFO");

                                    resetMenuForm();
                                }else{
                                    alert = new Alert(AlertType.ERROR);
                                    alert.setTitle("ERROR");
                                    try{
                                       DaoFactory.getComandaDao().delete(menu.getId());    
                                    }catch(Exception e){
                                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                                    }
                                } 

                                reloadMenusTable();
                                alert.setHeaderText(null);
                                alert.setContentText(response.getMessage());
                                alert.showAndWait();
                             }
                        });
                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
}
