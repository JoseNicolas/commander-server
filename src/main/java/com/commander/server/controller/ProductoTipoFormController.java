package com.commander.server.controller;

import com.commander.server.Request;
import com.commander.server.Response;
import com.commander.server.client.Client;
import com.commander.server.dao.DaoFactory;
import com.commander.server.model.ProductoTipo;
import com.commander.server.model.Usuario;
import com.commander.server.utils.Validator;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.commons.lang3.exception.ExceptionUtils;

/*
 * 2019/04/30
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class ProductoTipoFormController implements Initializable, EventHandler{

    @FXML private TableView<ProductoTipo> table_producto_tipos;
    @FXML private Button bt_eliminar;
    @FXML private TabPane tp_producto_tipos;
    @FXML private Tab tab_lista_producto_tipos;
    @FXML private Text t_text_info;
    @FXML private Button bt_reset_form;
    @FXML private Button bt_reload;
    @FXML private Button bt_aceptar;
    @FXML private ChoiceBox<ProductoTipo> cb_producto_tipo_padre;
    @FXML private TextField tf_producto_tipo_nombre;
    @FXML private Button bt_cancelar;
    @FXML private Tab tab_nuevo_editar_producto_tipo;
    @FXML private Button bt_editar;
    TableColumn<ProductoTipo, Integer> tc_id;
    TableColumn<ProductoTipo, String> tc_nombre;
    TableColumn<ProductoTipo, Integer> tc_padre;
    TableColumn<ProductoTipo, Date> tc_created;
    TableColumn<ProductoTipo, Integer> tc_creator;
    TableColumn<ProductoTipo, Date> tc_updated;
    TableColumn<ProductoTipo, Integer> tc_updator;
    
    private ProductoTipo producto_tipo_update;
    private List<ProductoTipo> producto_tipos;
    private Client client_tcp;
    private Alert alert;
    private boolean open_on_edit;
    
    public ProductoTipoFormController(boolean open_on_edit) throws Exception{
        this.producto_tipo_update = null;
        this.client_tcp = new Client(1024 * 1024, 1024 * 1024);
        this.producto_tipos = DaoFactory.getProductoTipoDao().queryAll();
        this.open_on_edit = open_on_edit;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bt_aceptar.setOnAction(this);
        bt_cancelar.setOnAction(this);
        bt_editar.setOnAction(this);
        bt_eliminar.setOnAction(this);
        bt_reload.setOnAction(this);
        bt_reset_form.setOnAction(this);
        tab_nuevo_editar_producto_tipo.setOnSelectionChanged(this);
        tab_lista_producto_tipos.setOnSelectionChanged(this);
        
        tc_id = new TableColumn<>("ID");
        tc_nombre = new TableColumn<>("NOMBRE");
        tc_padre = new TableColumn<>("PADRE");
        tc_created = new TableColumn<>("CREATED");
        tc_creator = new TableColumn<>("CREATOR");
        tc_updated = new TableColumn<>("UPDATED");
        tc_updator = new TableColumn<>("UPDATER");
        
        tc_id.setCellValueFactory(new PropertyValueFactory<ProductoTipo, Integer>("id"));
        tc_nombre.setCellValueFactory(new PropertyValueFactory<ProductoTipo, String>("nombre"));
        tc_padre.setCellValueFactory(new PropertyValueFactory<ProductoTipo, Integer>("padre"));
        tc_created.setCellValueFactory(new PropertyValueFactory<ProductoTipo, Date>("created"));
        tc_creator.setCellValueFactory(new PropertyValueFactory<ProductoTipo, Integer>("creator"));
        tc_updated.setCellValueFactory(new PropertyValueFactory<ProductoTipo, Date>("updated"));
        tc_updator.setCellValueFactory(new PropertyValueFactory<ProductoTipo, Integer>("updater"));
        tc_creator.setVisible(false);// Columna oculta por defecto
        tc_created.setVisible(false);// Columna oculta por defecto
        tc_updator.setVisible(false);// Columna oculta por defecto
        
        table_producto_tipos.getColumns().addAll(tc_id, tc_nombre, tc_padre, tc_created, 
                tc_creator, tc_updated, tc_updator);
        table_producto_tipos.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        ObservableList<ProductoTipo> ol_producto_tipos = FXCollections.observableArrayList(producto_tipos);
        cb_producto_tipo_padre.setItems(ol_producto_tipos);
        cb_producto_tipo_padre.getSelectionModel().select(0);
        
        if (open_on_edit){
            tp_producto_tipos.getSelectionModel().select(tab_lista_producto_tipos); 
        }  
    }    

    /**
     * Implement handle event from Interface EventHandler
     * @param event 
     */
    @Override
    public void handle(Event event) {
        try{
            Class target = event.getTarget().getClass();
            
            // Botones Aceptar, Cancelar
            if (target.equals(Button.class)){
                Button button = (Button) event.getSource();
                if (bt_aceptar.equals(button)){
                    if (this.producto_tipo_update == null){
                        createProductoTipo();
                    }else{
                        updateProductoTipo(this.producto_tipo_update);
                    }
                }
                
                if (bt_cancelar.equals(button)){
                    Stage stage = (Stage) button.getScene().getWindow();
                    stage.close();
                }
                
                if (bt_eliminar.equals(button)){
                    deleteProductoTipo();
                }
                
                if (bt_editar.equals(button)){
                    ProductoTipo producto_tipo = (ProductoTipo) table_producto_tipos.getSelectionModel().getSelectedItem();
                    if (producto_tipo != null){
                        this.producto_tipo_update = producto_tipo;
                        setProductoTipoToForm(producto_tipo);
                        tp_producto_tipos.getSelectionModel().select(tab_nuevo_editar_producto_tipo);   
                    }
                }
                
                if (bt_reload.equals(button)){
                    reloadProductoTiposTable();
                    ObservableList<ProductoTipo> ol_producto_tipos = FXCollections.observableArrayList(producto_tipos);
                    table_producto_tipos.setItems(ol_producto_tipos);
                }
                
                if (bt_reset_form.equals(button)){
                    resetProductoTipoForm();
                }
            }
            
            // PestaÃ±as (Tab)
            if (target.equals(Tab.class)){
                if (tab_nuevo_editar_producto_tipo.isSelected()){
                }
               
                if (tab_lista_producto_tipos.isSelected()){
                    ObservableList<ProductoTipo> ol_producto_tipos = FXCollections.observableArrayList(producto_tipos);
                    table_producto_tipos.setItems(ol_producto_tipos);
                }
            }
            
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Valida los campos del formulario Producto Tipos
     */
    private boolean validarProductoTiposForm(){
        boolean is_valid = true;
        
        String nombre = tf_producto_tipo_nombre.getText();
        ProductoTipo producto_tipo_padre = cb_producto_tipo_padre.getSelectionModel().getSelectedItem();
        
        if (Validator.isEmpty(nombre)){
            t_text_info.setText("Nombre* es un campo obligatorio");
            tf_producto_tipo_nombre.setStyle("-fx-border-color: red;");
            is_valid = false;
        }else{
            tf_producto_tipo_nombre.setStyle("-fx-border-color: green;");
        }
        
        t_text_info.setFill(Color.RED);
        
        if (is_valid){
            t_text_info.setText("");
        }
        
        return is_valid;
    }
    
    /**
     * Inicializa los campos del formulario con los valores de un Producto Tipo
     * @param producto_tipo
     */
    private void setProductoTipoToForm(ProductoTipo producto_tipo){
        ProductoTipo producto_tipo_padre = null;
        ObservableList<ProductoTipo> ol_producto_tipos;
        
        try{
            ol_producto_tipos = FXCollections.observableArrayList(producto_tipos);
            
            tf_producto_tipo_nombre.setText(producto_tipo.getNombre());
            cb_producto_tipo_padre.setItems(ol_producto_tipos);
            
            for(int i=0; i<producto_tipos.size(); i++){
                ProductoTipo p = producto_tipos.get(i);
                if (p.getId() == producto_tipo.getPadre()){
                    producto_tipo_padre = p;
                }
            }
            
            if (producto_tipo_padre != null){
                cb_producto_tipo_padre.getSelectionModel().select(producto_tipo_padre);
            }
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Devuelve el producto tipo obtenido desde el formulario de producto tipos
     * @return ProductoTipo producto_tipo
     */
    private ProductoTipo getProductoTipoFromForm(){
        ProductoTipo producto_tipo = new ProductoTipo();
        ProductoTipo producto_tipo_padre;
        
        try{
            producto_tipo.setNombre(tf_producto_tipo_nombre.getText());
            producto_tipo_padre = cb_producto_tipo_padre.getSelectionModel().getSelectedItem();
            if (producto_tipo_padre != null){
                producto_tipo.setPadre(producto_tipo_padre.getId());
            }else{
                producto_tipo.setPadre(0);
            }
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }

        return producto_tipo;
    }
    
    /**
     * Reinicializa el formulario de nuevo producto tipo
     */
    private void resetProductoTipoForm(){
        tf_producto_tipo_nombre.setText("");
        t_text_info.setText("");
        
        tf_producto_tipo_nombre.setStyle(null);
    }
    
    /**
     * Recarga el listado de producto tipos en la tabla
     */
    private void reloadProductoTiposTable(){
        try{
            this.producto_tipos = DaoFactory.getProductoTipoDao().queryAll();
            cb_producto_tipo_padre.setItems(FXCollections.observableArrayList(producto_tipos));
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        } 
    }
    
    // ========================================================================
    //                              CRUD FUNCTIONS
    // ========================================================================
    /**
     * AÃƒÆ’Ã‚Â±adir producto tipo a BD
     */
    private void createProductoTipo(){
        Request request;
        Usuario usuario_sesion;
        String server_action = "create-productoTipo";
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
        
            if (validarProductoTiposForm()){
                ProductoTipo producto_tipo = getProductoTipoFromForm();
                producto_tipo.setCreated(new Date());
                producto_tipo.setCreator(usuario_sesion.getId());
                producto_tipo.setUpdated(new Date());
                producto_tipo.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(producto_tipo);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(createProductoTipoClientListener());
                this.client_tcp.sendTCP(request);
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Actualizar producto tipo de la BD
     */
    private void updateProductoTipo(ProductoTipo producto_tipo){
        Request request;
        Usuario usuario_sesion;
        ProductoTipo producto_tipo_updated;
        String server_action = "update-productoTipo";
        try{
            request = new Request();
            usuario_sesion = MainFormController.getUserSesion();
            producto_tipo_updated = getProductoTipoFromForm();
            
            if (validarProductoTiposForm()){
                producto_tipo_updated.setId(producto_tipo.getId());
//                producto_tipo_updated.setCreated(producto_tipo.getCreated());
//                producto_tipo_updated.setCreator(producto_tipo.getCreator());
                producto_tipo_updated.setUpdated(new Date());
                producto_tipo_updated.setUpdater(usuario_sesion.getId());

                request.setAction(server_action);
                request.setData(producto_tipo_updated);
                
                this.client_tcp = new Client();
                this.client_tcp.start();
                this.client_tcp.connect();    
                this.client_tcp.addListener(updateProductoTipoClientListener());
                this.client_tcp.sendTCP(request);
                
                this.producto_tipo_update = null;
            }   
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
    
    /**
     * Elimina producto tipo del sistema
     */
    private void deleteProductoTipo(){
        List<ProductoTipo> producto_tipos = table_producto_tipos.getSelectionModel().getSelectedItems();
        ProductoTipo producto_tipo;
        String server_action = "delete-productoTipo";
        
        if (producto_tipos.size() == 1){
            producto_tipo = producto_tipos.get(0);
            
            alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Eliminar Tipo Producto");
            alert.setHeaderText(null);
            alert.setContentText("Se va a proceder a eliminar el tipo de producto con ID ("+producto_tipo.getId()+"). Â¿EstÃ¡ seguro?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                try{
                    Request request = new Request();
                    request.setAction(server_action);
                    request.setData(producto_tipo);

                    this.client_tcp = new Client();
                    this.client_tcp.start();
                    if (!this.client_tcp.isConnected()){
                        this.client_tcp.connect();    
                    }
                    this.client_tcp.addListener(deleteProductoTipoClientListener());
                    this.client_tcp.sendTCP(request);
                }catch(Exception e){
                    MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            alert = new Alert(AlertType.ERROR);
                            alert.setTitle("ERROR");
                            alert.setHeaderText(null);
                            alert.setContentText("Ups! Parace que algo no saliÃƒÂ³ bien...");
                            alert.showAndWait();
                        }
                   });
                }
            }
        }
    }
    
    // ========================================================================
    //                           CLIENT LISTENERS
    // ========================================================================
    public Listener createProductoTipoClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");

                                     resetProductoTipoForm();
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });

                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener deleteProductoTipoClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });
                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    public Listener updateProductoTipoClientListener(){
        return new Listener(){
            public void received (Connection connection, Object object) {
                if (object instanceof Response) {
                    try{
                        final Response response = (Response)object;

                        Platform.runLater(new Runnable() {
                             @Override
                             public void run() {
                                if (response.isStatus() && !response.isError()){
                                     alert = new Alert(AlertType.INFORMATION);
                                     alert.setTitle("INFO");

                                     resetProductoTipoForm();
                                 }else{
                                     alert = new Alert(AlertType.ERROR);
                                     alert.setTitle("ERROR");
                                 } 

                                 alert.setHeaderText(null);
                                 alert.setContentText(response.getMessage());
                                 alert.showAndWait();
                             }
                        });

                    }catch(Exception e) {
                        MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
                    }
                }
            }
        };
    }
    
}
