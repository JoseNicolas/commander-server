package com.commander.server.client;

import com.commander.server.Request;
import com.commander.server.Response;
import com.commander.server.controller.MainFormController;
import com.commander.server.model.*;
import com.esotericsoftware.kryo.Registration;
import java.io.IOException;
import java.util.ArrayList;
import com.commander.server.utils.Config;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.util.Date;
import java.util.logging.Level;
import org.apache.commons.lang3.exception.ExceptionUtils;

/*
 * 2019/04/29
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class Client extends com.esotericsoftware.kryonet.Client{
    
    public Client(){
        super();
        this.registerAll(this);
    }
    
    public Client(int writeBufferSize, int objectBufferSize){
        super(writeBufferSize, objectBufferSize);
        this.registerAll(this);
    }
    
    /**
     * Serializa y añade las clases para su posterior uso.
     * Es necesario añadir todas las clases antes de realizar la conexion
     * @param client 
     */
    public void registerAll(Client client){
        
        client.getKryo().register(Date.class);
        client.getKryo().register(Request.class);
        client.getKryo().register(Response.class);
        client.getKryo().register(ArrayList.class);
        client.getKryo().register(Usuario.class);
        client.getKryo().register(Mesa.class);
        client.getKryo().register(Rol.class);
        client.getKryo().register(ProductoTipo.class);
        client.getKryo().register(Producto.class);
        client.getKryo().register(Menu.class);
        client.getKryo().register(Factura.class);
        client.getKryo().register(DetalleMenu.class);
        client.getKryo().register(DetalleComanda.class);
        client.getKryo().register(Comanda.class);
    }

    /**
     * Conexion del cliente TCP parametrizado con los parametros del fichero de
     * configuracion (config.properties).
     */
    public void connect(){
        int client_timeout;
        String server_ip_addr;
        int server_tcp_port;
        
        try{
            if (!this.isConnected()){
                client_timeout = Integer.parseInt(Config.getProperty("client_timeout"));
                server_ip_addr = Config.getProperty("server_ip_addr");
                server_tcp_port = Integer.parseInt(Config.getProperty("server_tcp_port"));
                
                super.connect(client_timeout, server_ip_addr, server_tcp_port);
            }
        }catch(Exception e){
            MainFormController.Logger(this.getClass(), ExceptionUtils.getStackTrace(e), Level.SEVERE);
        }
    }
}
