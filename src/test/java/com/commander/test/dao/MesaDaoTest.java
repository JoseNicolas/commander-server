package com.commander.test.dao;

import com.commander.server.dao.DaoFactory;
import com.commander.server.model.Mesa;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * 2019/04/20
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MesaDaoTest {
    private Mesa aux_mesa;
    private static int id_inserted_mesa;
    
    public MesaDaoTest() {
        this.aux_mesa = new Mesa();
        this.aux_mesa.setNombre("Mesa 001");
        this.aux_mesa.setDescripcion("Mesa situada frente a maquina tragaperras");
        this.aux_mesa.setCreated(new Date());
        this.aux_mesa.setCreator(1);
        this.aux_mesa.setUpdated(new Date());
        this.aux_mesa.setUpdater(1);
    }

    @BeforeClass
    public static void setUpClass() {
        }
        
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        }
        
    @After
    public void tearDown() {
    }
    

    @Test
    public void testAInsert() {
        Mesa mesa_copy=null;
        try{
            mesa_copy = (Mesa) aux_mesa.clone();
            DaoFactory.getMesaDao().insert(aux_mesa);
            id_inserted_mesa = aux_mesa.getId();
        }catch(Exception ex){
            aux_mesa=null;
        }
        
        assertNotEquals(aux_mesa.getId(), mesa_copy.getId());
    }
    
    @Test
    public void testBLoad() {
        Mesa mesa_load=null;
        try{
            mesa_load = DaoFactory.getMesaDao().load(id_inserted_mesa);
        }catch(Exception ex){
            mesa_load=null;
        }
        
        assertNotNull(mesa_load);
    }
    
    @Test
    public void testCUpdate() {
        Mesa mesa_update=null;
        try{
            aux_mesa.setNombre("Mesa 002");
            aux_mesa.setDescripcion("Mesa ubicaa al comienzo de la terraza exterior");
            aux_mesa.setCreated(new Date());
            aux_mesa.setCreator(2);
            aux_mesa.setUpdated(new Date());
            aux_mesa.setUpdater(2);
            
            DaoFactory.getMesaDao().update(id_inserted_mesa, aux_mesa);
            
            mesa_update = DaoFactory.getMesaDao().load(id_inserted_mesa);
            
        }catch(Exception ex){
            aux_mesa=null;
        }
        
        assertEquals(aux_mesa.getCreated().toString(), mesa_update.getCreated().toString());
    }

    @Test
    public void testDDelete() {
        boolean deleted=false;
        try{
            deleted=DaoFactory.getMesaDao().delete(id_inserted_mesa);
        }catch(Exception ex){
            deleted=false;
        }
        
        assertTrue(deleted);
    }

    @Test
    public void testEQueryAll() {
        List<Mesa> mesas=null;
        try{
            mesas=DaoFactory.getMesaDao().queryAll();
        }catch(Exception ex){
            mesas=null;
        }
        
        assertNotNull(mesas);
    }
}
