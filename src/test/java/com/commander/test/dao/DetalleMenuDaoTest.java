package com.commander.test.dao;

import com.commander.server.dao.DaoFactory;
import com.commander.server.model.DetalleMenu;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * 2019/04/20
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DetalleMenuDaoTest {
    private DetalleMenu aux_detalle_menu;
    private static int id_inserted_detalle_menu;
    
    public DetalleMenuDaoTest() {
        this.aux_detalle_menu = new DetalleMenu();
        this.aux_detalle_menu.setMenu(1);
        this.aux_detalle_menu.setProducto(1);
        this.aux_detalle_menu.setCreated(new Date());
        this.aux_detalle_menu.setCreator(1);
        this.aux_detalle_menu.setUpdated(new Date());
        this.aux_detalle_menu.setUpdater(1);
    }

    @BeforeClass
    public static void setUpClass() {
        }
        
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        }
        
    @After
    public void tearDown() {
    }
    

    @Test
    public void testAInsert() {
        DetalleMenu detalle_menu_copy=null;
        try{
            detalle_menu_copy = (DetalleMenu) aux_detalle_menu.clone();
            DaoFactory.getDetalleMenuDao().insert(aux_detalle_menu);
            id_inserted_detalle_menu = aux_detalle_menu.getId();
        }catch(Exception ex){
            aux_detalle_menu=null;
        }
        
        assertNotEquals(aux_detalle_menu.getId(), detalle_menu_copy.getId());
    }
    
    @Test
    public void testBLoad() {
        DetalleMenu detalle_menu_load=null;
        try{
            detalle_menu_load = DaoFactory.getDetalleMenuDao().load(id_inserted_detalle_menu);
        }catch(Exception ex){
            detalle_menu_load=null;
        }
        
        assertNotNull(detalle_menu_load);
    }
    
    @Test
    public void testCUpdate() {
        DetalleMenu detalle_menu_update=null;
        try{
            aux_detalle_menu.setMenu(2);
            aux_detalle_menu.setProducto(2);
            aux_detalle_menu.setCreated(new Date());
            aux_detalle_menu.setCreator(2);
            aux_detalle_menu.setUpdated(new Date());
            aux_detalle_menu.setUpdater(2);
            
            DaoFactory.getDetalleMenuDao().update(id_inserted_detalle_menu, aux_detalle_menu);
            
            detalle_menu_update = DaoFactory.getDetalleMenuDao().load(id_inserted_detalle_menu);
            
        }catch(Exception ex){
            aux_detalle_menu=null;
        }
        
        assertEquals(aux_detalle_menu.getCreated().toString(), detalle_menu_update.getCreated().toString());
    }

    @Test
    public void testDDelete() {
        boolean deleted=false;
        try{
            deleted=DaoFactory.getDetalleMenuDao().delete(id_inserted_detalle_menu);
        }catch(Exception ex){
            deleted=false;
        }
        
        assertTrue(deleted);
    }

    @Test
    public void testEQueryAll() {
        List<DetalleMenu> detalle_comandas=null;
        try{
            detalle_comandas=DaoFactory.getDetalleMenuDao().queryAll();
        }catch(Exception ex){
            detalle_comandas=null;
        }
        
        assertNotNull(detalle_comandas);
    }
}
