package com.commander.test.dao;

import com.commander.server.dao.DaoFactory;
import com.commander.server.model.ProductoTipo;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * 2019/04/20
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductoTipoDaoTest {
    private ProductoTipo aux_producto_tipo;
    private static int id_inserted_producto_tipo;
    
    public ProductoTipoDaoTest() {
        this.aux_producto_tipo = new ProductoTipo();
        this.aux_producto_tipo.setNombre("Entrantes");
        this.aux_producto_tipo.setPadre(0);
        this.aux_producto_tipo.setCreated(new Date());
        this.aux_producto_tipo.setCreator(1);
        this.aux_producto_tipo.setUpdated(new Date());
        this.aux_producto_tipo.setUpdater(1);
    }

    @BeforeClass
    public static void setUpClass() {
        }
        
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        }
        
    @After
    public void tearDown() {
    }
    

    @Test
    public void testAInsert() {
        ProductoTipo producto_tipo_copy=null;
        try{
            producto_tipo_copy = (ProductoTipo) aux_producto_tipo.clone();
            DaoFactory.getProductoTipoDao().insert(aux_producto_tipo);
            id_inserted_producto_tipo = aux_producto_tipo.getId();
        }catch(Exception ex){
            aux_producto_tipo=null;
        }
        
        assertNotEquals(aux_producto_tipo.getId(), producto_tipo_copy.getId());
    }
    
    @Test
    public void testBLoad() {
        ProductoTipo producto_tipo_load=null;
        try{
            producto_tipo_load = DaoFactory.getProductoTipoDao().load(id_inserted_producto_tipo);
        }catch(Exception ex){
            producto_tipo_load=null;
        }
        
        assertNotNull(producto_tipo_load);
    }
    
    @Test
    public void testCUpdate() {
        ProductoTipo producto_tipo_update=null;
        try{
            aux_producto_tipo.setNombre("Vino blanco Manchego");
            aux_producto_tipo.setPadre(2);
            aux_producto_tipo.setCreated(new Date());
            aux_producto_tipo.setCreator(2);
            aux_producto_tipo.setUpdated(new Date());
            aux_producto_tipo.setUpdater(2);
            
            DaoFactory.getProductoTipoDao().update(id_inserted_producto_tipo, aux_producto_tipo);
            
            producto_tipo_update = DaoFactory.getProductoTipoDao().load(id_inserted_producto_tipo);
            
        }catch(Exception ex){
            aux_producto_tipo=null;
        }
        
        assertEquals(aux_producto_tipo.getCreated().toString(), producto_tipo_update.getCreated().toString());
    }

    @Test
    public void testDDelete() {
        boolean deleted=false;
        try{
            deleted=DaoFactory.getProductoTipoDao().delete(id_inserted_producto_tipo);
        }catch(Exception ex){
            deleted=false;
        }
        
        assertTrue(deleted);
    }

    @Test
    public void testEQueryAll() {
        List<ProductoTipo> producto_tipos=null;
        try{
            producto_tipos=DaoFactory.getProductoTipoDao().queryAll();
        }catch(Exception ex){
            producto_tipos=null;
        }
        
        assertNotNull(producto_tipos);
    }
}
