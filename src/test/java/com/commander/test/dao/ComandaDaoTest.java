package com.commander.test.dao;

import com.commander.server.dao.DaoFactory;
import com.commander.server.model.Comanda;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * 2019/04/20 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ComandaDaoTest {
    private Comanda aux_comanda;
    private static int id_inserted_comanda;
    
    public ComandaDaoTest() {
        this.aux_comanda = new Comanda();
        this.aux_comanda.setMesa(1);
        this.aux_comanda.setCamarero(1);
        this.aux_comanda.setFactura(1);
        this.aux_comanda.setCreated(new Date());
        this.aux_comanda.setCreator(1);
        this.aux_comanda.setUpdated(new Date());
        this.aux_comanda.setUpdater(1);
    }

    @BeforeClass
    public static void setUpClass() {
        }
        
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        }
        
    @After
    public void tearDown() {
    }
    
    
    @Test
    public void testAInsert() {
        Comanda comanda_copy=null;
        try{
            comanda_copy = (Comanda) aux_comanda.clone();
            DaoFactory.getComandaDao().insert(aux_comanda);
            id_inserted_comanda = aux_comanda.getId();
        }catch(Exception ex){
            aux_comanda=null;
        }
        
        assertNotEquals(aux_comanda.getId(), comanda_copy.getId());
    }
    
    @Test
    public void testBLoad() {
        Comanda comanda_load=null;
        try{
            comanda_load = DaoFactory.getComandaDao().load(id_inserted_comanda);
        }catch(Exception ex){
            comanda_load=null;
        }
        
        assertNotNull(comanda_load);
    }
    
    @Test
    public void testCUpdate() {
        Comanda comanda_update=null;
        try{
            aux_comanda.setMesa(2);
            aux_comanda.setCamarero(2);
            aux_comanda.setFactura(2);
            aux_comanda.setCreated(new Date());
            aux_comanda.setCreator(2);
            aux_comanda.setUpdated(new Date());
            aux_comanda.setUpdater(2);
            
            DaoFactory.getComandaDao().update(id_inserted_comanda, aux_comanda);
            
            comanda_update = DaoFactory.getComandaDao().load(id_inserted_comanda);
            
        }catch(Exception ex){
            aux_comanda=null;
        }
        
        assertEquals(aux_comanda.getCreated().toString(), comanda_update.getCreated().toString());
    }

    @Test
    public void testDDelete() {
        boolean deleted=false;
        try{
            deleted=DaoFactory.getComandaDao().delete(id_inserted_comanda);
        }catch(Exception ex){
            deleted=false;
        }
        
        assertTrue(deleted);
    }

    @Test
    public void testEQueryAll() {
        List<Comanda> comandas=null;
        try{
            comandas=DaoFactory.getComandaDao().queryAll();
        }catch(Exception ex){
            comandas=null;
        }
        
        assertNotNull(comandas);
    }
}
