package com.commander.test.dao;

import com.commander.server.dao.DaoFactory;
import com.commander.server.model.Usuario;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * 2019/04/27
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UsuarioDaoTest {
    private Usuario aux_usuario;
    private static int id_inserted_usuario;
    
    public UsuarioDaoTest() {
        this.aux_usuario = new Usuario();
        this.aux_usuario.setUsuario("userTest");
        this.aux_usuario.setPassword("userTestPassword");
        this.aux_usuario.setNombre("userNombre");
        this.aux_usuario.setApellido1("userApellido1");
        this.aux_usuario.setApellido2("userApellido2");
        this.aux_usuario.setSexo("hombre");
        this.aux_usuario.setCreated(new Date());
        this.aux_usuario.setCreator(1);
        this.aux_usuario.setUpdated(new Date());
        this.aux_usuario.setUpdater(1);
    }

    @BeforeClass
    public static void setUpClass() {
        }
        
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        }
        
    @After
    public void tearDown() {
    }
    

    @Test
    public void testAInsert() {
        Usuario usuario_copy=null;
        try{
            usuario_copy = (Usuario) aux_usuario.clone();
            DaoFactory.getUsuarioDao().insert(aux_usuario);
            id_inserted_usuario = aux_usuario.getId();
        }catch(Exception ex){
            aux_usuario=null;
        }
        
        assertNotEquals(aux_usuario.getId(), usuario_copy.getId());
    }
    
    @Test
    public void testBLoad() {
        Usuario usuario_load=null;
        try{
            usuario_load = DaoFactory.getUsuarioDao().load(id_inserted_usuario);
        }catch(Exception ex){
            usuario_load=null;
        }
        
        assertNotNull(usuario_load);
    }
    
    @Test
    public void testCUpdate() {
        Usuario usuario_update=null;
        try{
            aux_usuario.setUsuario("Vino blanco Manchego");
            aux_usuario.setPassword("Vino blanco Manchego");
            aux_usuario.setNombre("newuserTest");
            aux_usuario.setApellido1("newUserApellido1");
            aux_usuario.setApellido2("newUserApellido2");
            aux_usuario.setSexo("mujer");
            aux_usuario.setCreated(new Date());
            aux_usuario.setCreator(2);
            aux_usuario.setUpdated(new Date());
            aux_usuario.setUpdater(2);
            
            DaoFactory.getUsuarioDao().update(id_inserted_usuario, aux_usuario);
            
            usuario_update = DaoFactory.getUsuarioDao().load(id_inserted_usuario);
            
        }catch(Exception ex){
            aux_usuario=null;
        }
        
        assertEquals(aux_usuario.getCreated().toString(), usuario_update.getCreated().toString());
    }

    @Test
    public void testDDelete() {
        boolean deleted=false;
        try{
            deleted=DaoFactory.getProductoTipoDao().delete(id_inserted_usuario);
        }catch(Exception ex){
            deleted=false;
        }
        
        assertTrue(deleted);
    }

    @Test
    public void testEQueryAll() {
        List<Usuario> usuarios=null;
        try{
            usuarios=DaoFactory.getUsuarioDao().queryAll();
        }catch(Exception ex){
            usuarios=null;
        }
        
        assertNotNull(usuarios);
    }
}
