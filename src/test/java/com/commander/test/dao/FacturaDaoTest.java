package com.commander.test.dao;

import com.commander.server.dao.DaoFactory;
import com.commander.server.model.Factura;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * 2019/04/20
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FacturaDaoTest {
    private Factura aux_factura;
    private static int id_inserted_factura;
    
    public FacturaDaoTest() {
        this.aux_factura = new Factura();
        this.aux_factura.setCodigo("FAC001");
        this.aux_factura.setFecha(new Date());
        this.aux_factura.setComanda(1);
        this.aux_factura.setImporte(123.45f);
        this.aux_factura.setPagada(false);
        this.aux_factura.setObservaciones("Observaciones de factura");
        this.aux_factura.setCreated(new Date());
        this.aux_factura.setCreator(1);
        this.aux_factura.setUpdated(new Date());
        this.aux_factura.setUpdater(1);
    }

    @BeforeClass
    public static void setUpClass() {
        }
        
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        }
        
    @After
    public void tearDown() {
    }
    

    @Test
    public void testAInsert() {
        Factura factura_copy=null;
        try{
            factura_copy = (Factura) aux_factura.clone();
            DaoFactory.getFacturaDao().insert(aux_factura);
            id_inserted_factura = aux_factura.getId();
        }catch(Exception ex){
            aux_factura=null;
        }
        
        assertNotEquals(aux_factura.getId(), factura_copy.getId());
    }
    
    @Test
    public void testBLoad() {
        Factura factura_load=null;
        try{
            factura_load = DaoFactory.getFacturaDao().load(id_inserted_factura);
        }catch(Exception ex){
            factura_load=null;
        }
        
        assertNotNull(factura_load);
    }
    
    @Test
    public void testCUpdate() {
        Factura factura_update=null;
        try{
            aux_factura.setCodigo("FAC002");
            aux_factura.setFecha(new Date());
            aux_factura.setComanda(2);
            aux_factura.setImporte(2);
            aux_factura.setPagada(true);
            aux_factura.setObservaciones("LA FACTURA FUE PAGADA EN EFECTIVO");
            aux_factura.setCreated(new Date());
            aux_factura.setCreator(2);
            aux_factura.setUpdated(new Date());
            aux_factura.setUpdater(2);
            
            DaoFactory.getFacturaDao().update(id_inserted_factura, aux_factura);
            
            factura_update = DaoFactory.getFacturaDao().load(id_inserted_factura);
            
        }catch(Exception ex){
            aux_factura=null;
        }
        
        assertEquals(aux_factura.getCreated().toString(), factura_update.getCreated().toString());
    }

    @Test
    public void testDDelete() {
        boolean deleted=false;
        try{
            deleted=DaoFactory.getFacturaDao().delete(id_inserted_factura);
        }catch(Exception ex){
            deleted=false;
        }
        
        assertTrue(deleted);
    }

    @Test
    public void testEQueryAll() {
        List<Factura> facturas=null;
        try{
            facturas=DaoFactory.getFacturaDao().queryAll();
        }catch(Exception ex){
            facturas=null;
        }
        
        assertNotNull(facturas);
    }
}
