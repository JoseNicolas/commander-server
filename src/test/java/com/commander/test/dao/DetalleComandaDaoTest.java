package com.commander.test.dao;

import com.commander.server.dao.DaoFactory;
import com.commander.server.model.DetalleComanda;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * 2019/04/20
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DetalleComandaDaoTest {
    private DetalleComanda aux_detalle_comanda;
    private static int id_inserted_detalle_comanda;
    
    public DetalleComandaDaoTest() {
        this.aux_detalle_comanda = new DetalleComanda();
        this.aux_detalle_comanda.setComanda(1);
        this.aux_detalle_comanda.setProducto(1);
        this.aux_detalle_comanda.setCantidad(1.5f);
        this.aux_detalle_comanda.setCreated(new Date());
        this.aux_detalle_comanda.setCreator(1);
        this.aux_detalle_comanda.setUpdated(new Date());
        this.aux_detalle_comanda.setUpdater(1);
    }

    @BeforeClass
    public static void setUpClass() {
        }
        
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        }
        
    @After
    public void tearDown() {
    }
    

    @Test
    public void testAInsert() {
        DetalleComanda detalle_comanda_copy=null;
        try{
            detalle_comanda_copy = (DetalleComanda) aux_detalle_comanda.clone();
            DaoFactory.getDetalleComandaDao().insert(aux_detalle_comanda);
            id_inserted_detalle_comanda = aux_detalle_comanda.getId();
        }catch(Exception ex){
            aux_detalle_comanda=null;
        }
        
        assertNotEquals(aux_detalle_comanda.getId(), detalle_comanda_copy.getId());
    }
    
    @Test
    public void testBLoad() {
        DetalleComanda detalle_comanda_load=null;
        try{
            detalle_comanda_load = DaoFactory.getDetalleComandaDao().load(id_inserted_detalle_comanda);
        }catch(Exception ex){
            detalle_comanda_load=null;
        }
        
        assertNotNull(detalle_comanda_load);
    }
    
    @Test
    public void testCUpdate() {
        DetalleComanda detalle_comanda_update=null;
        try{
            aux_detalle_comanda.setComanda(2);
            aux_detalle_comanda.setProducto(2);
            aux_detalle_comanda.setCantidad(2.5f);
            aux_detalle_comanda.setCreated(new Date());
            aux_detalle_comanda.setCreator(2);
            aux_detalle_comanda.setUpdated(new Date());
            aux_detalle_comanda.setUpdater(2);
            
            DaoFactory.getDetalleComandaDao().update(id_inserted_detalle_comanda, aux_detalle_comanda);
            
            detalle_comanda_update = DaoFactory.getDetalleComandaDao().load(id_inserted_detalle_comanda);
            
        }catch(Exception ex){
            aux_detalle_comanda=null;
        }
        
        assertEquals(aux_detalle_comanda.getCreated().toString(), detalle_comanda_update.getCreated().toString());
    }

    @Test
    public void testDDelete() {
        boolean deleted=false;
        try{
            deleted=DaoFactory.getDetalleComandaDao().delete(id_inserted_detalle_comanda);
        }catch(Exception ex){
            deleted=false;
        }
        
        assertTrue(deleted);
    }

    @Test
    public void testEQueryAll() {
        List<DetalleComanda> detalle_comandas=null;
        try{
            detalle_comandas=DaoFactory.getDetalleComandaDao().queryAll();
        }catch(Exception ex){
            detalle_comandas=null;
        }
        
        assertNotNull(detalle_comandas);
    }
}
