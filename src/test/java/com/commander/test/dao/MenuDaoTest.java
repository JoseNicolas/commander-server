package com.commander.test.dao;

import com.commander.server.dao.DaoFactory;
import com.commander.server.model.Menu;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * 2019/04/20
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MenuDaoTest {
    private Menu aux_menu;
    private static int id_inserted_menu;
    
    public MenuDaoTest() {
        this.aux_menu = new Menu();
        this.aux_menu.setNombre("Menu Especialidades");
        this.aux_menu.setDescripcion("Selecto menu repleto de platos variados e interesantes");
        this.aux_menu.setCreated(new Date());
        this.aux_menu.setCreator(1);
        this.aux_menu.setUpdated(new Date());
        this.aux_menu.setUpdater(1);
    }

    @BeforeClass
    public static void setUpClass() {
        }
        
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        }
        
    @After
    public void tearDown() {
    }
    

    @Test
    public void testAInsert() {
        Menu menu_copy=null;
        try{
            menu_copy = (Menu) aux_menu.clone();
            DaoFactory.getMenuDao().insert(aux_menu);
            id_inserted_menu = aux_menu.getId();
        }catch(Exception ex){
            aux_menu=null;
        }
        
        assertNotEquals(aux_menu.getId(), menu_copy.getId());
    }
    
    @Test
    public void testBLoad() {
        Menu menu_load=null;
        try{
            menu_load = DaoFactory.getMenuDao().load(id_inserted_menu);
        }catch(Exception ex){
            menu_load=null;
        }
        
        assertNotNull(menu_load);
    }
    
    @Test
    public void testCUpdate() {
        Menu menu_update=null;
        try{
            aux_menu.setNombre("Menu del dia");
            aux_menu.setDescripcion("Menu básico de la casa");
            aux_menu.setCreated(new Date());
            aux_menu.setCreator(2);
            aux_menu.setUpdated(new Date());
            aux_menu.setUpdater(2);
            
            DaoFactory.getMenuDao().update(id_inserted_menu, aux_menu);
            
            menu_update = DaoFactory.getMenuDao().load(id_inserted_menu);
            
        }catch(Exception ex){
            aux_menu=null;
        }
        
        assertEquals(aux_menu.getCreated().toString(), menu_update.getCreated().toString());
    }

    @Test
    public void testDDelete() {
        boolean deleted=false;
        try{
            deleted=DaoFactory.getMenuDao().delete(id_inserted_menu);
        }catch(Exception ex){
            deleted=false;
        }
        
        assertTrue(deleted);
    }

    @Test
    public void testEQueryAll() {
        List<Menu> menus=null;
        try{
            menus=DaoFactory.getMenuDao().queryAll();
        }catch(Exception ex){
            menus=null;
        }
        
        assertNotNull(menus);
    }
}
