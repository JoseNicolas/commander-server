package com.commander.test.dao;

import com.commander.server.dao.DaoFactory;
import com.commander.server.model.Producto;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * 2019/04/20
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductoDaoTest {
    private Producto aux_producto;
    private static int id_inserted_producto;
    
    public ProductoDaoTest() {
        this.aux_producto = new Producto();
        this.aux_producto.setCodigo("PR001");
        this.aux_producto.setNombre("Hamburguesa ternera");
        this.aux_producto.setDescripcion("Deliciosa hamburguesa elaborada con la mejor ternera");
        this.aux_producto.setTipo(1);
        this.aux_producto.setPrecio_unit(7.45f);
        this.aux_producto.setCreated(new Date());
        this.aux_producto.setCreator(1);
        this.aux_producto.setUpdated(new Date());
        this.aux_producto.setUpdater(1);
    }

    @BeforeClass
    public static void setUpClass() {
        }
        
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        }
        
    @After
    public void tearDown() {
    }
    

    @Test
    public void testAInsert() {
        Producto producto_copy=null;
        try{
            producto_copy = (Producto) aux_producto.clone();
            DaoFactory.getProductoDao().insert(aux_producto);
            id_inserted_producto = aux_producto.getId();
        }catch(Exception ex){
            aux_producto=null;
        }
        
        assertNotEquals(aux_producto.getId(), producto_copy.getId());
    }
    
    @Test
    public void testBLoad() {
        Producto producto_load=null;
        try{
            producto_load = DaoFactory.getProductoDao().load(id_inserted_producto);
        }catch(Exception ex){
            producto_load=null;
        }
        
        assertNotNull(producto_load);
    }
    
    @Test
    public void testCUpdate() {
        Producto producto_update=null;
        try{
            aux_producto.setCodigo("PR0001");
            aux_producto.setNombre("Hamburguesa carne");
            aux_producto.setDescripcion("");
            aux_producto.setTipo(2);
            aux_producto.setPrecio_unit(8.34f);
            aux_producto.setCreated(new Date());
            aux_producto.setCreator(2);
            aux_producto.setUpdated(new Date());
            aux_producto.setUpdater(2);
            
            DaoFactory.getProductoDao().update(id_inserted_producto, aux_producto);
            
            producto_update = DaoFactory.getProductoDao().load(id_inserted_producto);
            
        }catch(Exception ex){
            aux_producto=null;
        }
        
        assertEquals(aux_producto.getCreated().toString(), producto_update.getCreated().toString());
    }

    @Test
    public void testDDelete() {
        boolean deleted=false;
        try{
            deleted=DaoFactory.getProductoDao().delete(id_inserted_producto);
        }catch(Exception ex){
            deleted=false;
        }
        
        assertTrue(deleted);
    }

    @Test
    public void testEQueryAll() {
        List<Producto> productos=null;
        try{
            productos=DaoFactory.getProductoDao().queryAll();
        }catch(Exception ex){
            productos=null;
        }
        
        assertNotNull(productos);
    }
}
