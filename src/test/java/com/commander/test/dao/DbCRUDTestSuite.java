package com.commander.test.dao;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/*
 * 2019/04/27
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({com.commander.test.dao.DetalleComandaDaoTest.class, com.commander.test.dao.UsuarioDaoTest.class, com.commander.test.dao.ComandaDaoTest.class, com.commander.test.dao.FacturaDaoTest.class, com.commander.test.dao.MenuDaoTest.class, com.commander.test.dao.MesaDaoTest.class, com.commander.test.dao.ProductoTipoDaoTest.class, com.commander.test.dao.DetalleMenuDaoTest.class, com.commander.test.dao.ProductoDaoTest.class})
public class DbCRUDTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
